<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentSlideTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('content_slidevariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->unsigned();
            $table->string('lang_code',6);

            $table->string('title', 191)->nullable();
            $table->text('description')->nullable();
            $table->string('button_text', 191)->nullable();
            $table->string('button_url', 191)->nullable();
            $table->string('image_url', 191)->nullable();

            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::table('content_slidevariable', function (Blueprint $table) {
            $table->foreign('content_id')->references('id')->on('content');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_slidevariable');
    }
}
