<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalenderTagCategoryTables extends Migration
{
    public function up()
    {
        Schema::create('calendar_has_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calendar_id')->unsigned()->nullable();
            $table->integer('tag_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('calendar_has_tag', function (Blueprint $table) {
            $table->foreign('calendar_id')->references('id')->on('calendar');
            $table->foreign('tag_id')->references('id')->on('tag');
        });

        Schema::create('calendar_has_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calendar_id')->unsigned()->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('calendar_has_category', function (Blueprint $table) {
            $table->foreign('calendar_id')->references('id')->on('calendar');
            $table->foreign('category_id')->references('id')->on('category');
        });
    }

    public function down()
    {
        Schema::dropIfExists('calendar_has_tag');
        Schema::dropIfExists('calendar_has_category');
    }
}
