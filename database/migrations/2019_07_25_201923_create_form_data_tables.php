<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormDataTables extends Migration
{
    
    public function up()
    {
        Schema::create('formdata', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('form_id')->unsigned();
            $table->string('source_type', 191);
            $table->integer('source_id');
            $table->string('lang_code',6);

            $table->longText('data')->nullable();
            $table->timestamps();
        });

    }

    public function down()
    {
        Schema::dropIfExists('formdata');
    }
}
