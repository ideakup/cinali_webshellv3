<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTables extends Migration
{
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('top_id')->unsigned()->nullable();
            $table->string('type', 16)->default('content')->comment('menuitem, content, list, link, photogallery, calendar');
            $table->mediumText('description')->nullable();
            
            $table->string('position', 16)->default('top')->comment('top, aside, all, none');
            $table->string('headertheme', 16)->default('general')->comment('general, home');
            $table->string('slidertype', 16)->default('no')->comment('image, slider, no');
            $table->string('listtype', 16)->default('normal')->nullable()->comment('normal, headed, pyramid, col-2x, col-3x, col-4x');
            $table->string('dropdowntype', 16)->default('normal')->comment('normal, mega01, mega02');
            $table->enum('breadcrumbvisible', ['yes', 'no'])->default('no');
            $table->enum('asidevisible', ['yes', 'no'])->default('no');

            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::table('menu', function (Blueprint $table) {
            $table->foreign('top_id')->references('id')->on('menu');
        });

        Schema::create('menuvariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            $table->string('lang_code',6);
            $table->string('name',191);
            $table->string('slug',191)->nullable();
            $table->string('menutitle',191)->nullable();
            $table->string('title',191)->nullable();
            $table->mediumText('stvalue')->nullable();
            $table->timestamps();
        });

        Schema::table('menuvariable', function (Blueprint $table) {
            $table->foreign('menu_id')->references('id')->on('menu');
        });
    }

    public function down()
    {   
        Schema::dropIfExists('menuvariable');
        Schema::dropIfExists('menu');
    }
}
