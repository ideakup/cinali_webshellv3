<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoGalleryTables extends Migration
{

    public function up()
    {
        Schema::create('content_photogalleryvariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->unsigned();
            $table->string('lang_code',6);
            
            $table->string('name', 191)->nullable();
            $table->string('description', 191)->nullable();
            $table->string('url', 191)->nullable();
            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::table('content_photogalleryvariable', function (Blueprint $table) {
            $table->foreign('content_id')->references('id')->on('content');
        });
    }

    public function down()
    {
        Schema::dropIfExists('content_photogalleryvariable');
    }
}
