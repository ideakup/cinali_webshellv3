<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagCategoryTables extends Migration
{
    public function up()
    {
        Schema::create('tag', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::create('tagvariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tag_id')->unsigned();
            $table->string('lang_code',6);
            $table->string('title', 191);
            $table->timestamps();
        });

        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::create('categoryvariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('lang_code',6);
            $table->string('title', 191);
            $table->timestamps();
        });

        Schema::create('content_has_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->unsigned()->nullable();
            $table->integer('tag_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('content_has_tag', function (Blueprint $table) {
            $table->foreign('content_id')->references('id')->on('content');
            $table->foreign('tag_id')->references('id')->on('tag');
        });

        Schema::create('content_has_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->unsigned()->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('content_has_category', function (Blueprint $table) {
            $table->foreign('content_id')->references('id')->on('content');
            $table->foreign('category_id')->references('id')->on('category');
        });

    }

    public function down()
    {
        Schema::dropIfExists('content_has_category');
        Schema::dropIfExists('categoryvariable');
        Schema::dropIfExists('category');

        Schema::dropIfExists('content_has_tag');
        Schema::dropIfExists('tagvariable');
        Schema::dropIfExists('tag');
    }
}
