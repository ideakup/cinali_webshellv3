<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarTables extends Migration
{

    public function up()
    {
        Schema::create('calendar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('form_id')->unsigned()->nullable();
            $table->string('owner');
            $table->enum('allDay', ['true', 'false'])->default('false');
            $table->dateTime('start');
            $table->dateTime('end')->nullable();
            $table->integer('capacity')->nullable(); 
            $table->decimal('price', 8, 2)->nullable();

            $table->integer('order');
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->enum('deleted', ['no', 'yes'])->default('no');
            $table->timestamps();
        });

        Schema::table('calendar', function (Blueprint $table) {
            $table->foreign('form_id')->references('id')->on('content');
        });

        Schema::create('calendarvariable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calendar_id')->unsigned();
            $table->string('lang_code',6);

            $table->string('title', 190);
            $table->longText('content')->nullable();
            $table->longText('address')->nullable();
            $table->longText('nocapmessage')->nullable();
            $table->string('image_name', 190)->nullable();
            $table->timestamps();
        });

        Schema::table('calendarvariable', function (Blueprint $table) {
            $table->foreign('calendar_id')->references('id')->on('calendar');
        });

    }

    public function down()
    {
        Schema::dropIfExists('calendarvariable');
        Schema::dropIfExists('calendar');
    }
    
}
