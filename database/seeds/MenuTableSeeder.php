<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('menu')->delete();
        DB::table('menuvariable')->delete();

        DB::table('menu')->insert([
			'id' => 1,
			'type' => 'content',
			'order' => '1000'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 1,
			'lang_code' => 'tr',
			'name' => 'ANA SAYFA',
			'slug' => 'ana-sayfa',
			'title' => 'ANA SAYFA'
        ]);

        DB::table('menu')->insert([
			'id' => 2,
			'type' => 'content',
			'order' => '10000'
        ]);

        DB::table('menuvariable')->insert([
			'menu_id' => 2,
			'lang_code' => 'tr',
			'name' => 'İLETİŞİM',
			'slug' => 'iletisim',
			'title' => 'İLETİŞİM'
        ]);

    }
}
