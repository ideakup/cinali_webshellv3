<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
    	$this->call(PermissionTableSeeder::class);
    	$this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);

        $this->call(LanguageTableSeeder::class);
        $this->call(SiteSettingsTableSeeder::class);
        $this->call(MenuTableSeeder::class);
    }
}
