<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PermissionTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('permissions')->delete();

        DB::table('permissions')->insert([
            'name' => 'ekle',
            'guard_name' => 'web',
            'description' => 'Ekleme yetkisi',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            'name' => 'duzenle',
            'guard_name' => 'web',
            'description' => 'Düzenleme yetkisi',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            'name' => 'goruntule',
            'guard_name' => 'web',
            'description' => 'Görüntüleme yetkisi',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            'name' => 'sil',
            'guard_name' => 'web',
            'description' => 'Silme yetkisi',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
