<?php

use Illuminate\Database\Seeder;

class SiteSettingsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('sitesettings')->delete();

        /* Search Engine Optimization - METAs */
        DB::table('sitesettings')->insert(['name' => 'SEO Başlık', 'slug' => 'seo-title', 'type' => 'text', 'value' => null, 'order' => 10]);
        DB::table('sitesettings')->insert(['name' => 'SEO Açıklama', 'slug' => 'seo-description', 'type' => 'text', 'value' => null, 'order' => 20]);
        DB::table('sitesettings')->insert(['name' => 'SEO Anahtar Kelimeler', 'slug' => 'seo-keyword', 'type' => 'text', 'value' => null, 'order' => 30]);

        /* Footer Bilgileri */
        DB::table('sitesettings')->insert(['name' => 'Footer Adres', 'slug' => 'footer-address', 'type' => 'text', 'value' => null, 'order' => 40]);
        DB::table('sitesettings')->insert(['name' => 'Footer Telefon', 'slug' => 'footer-phone', 'type' => 'phone', 'value' => null, 'order' => 50]);
        DB::table('sitesettings')->insert(['name' => 'Footer Faks', 'slug' => 'footer-fax', 'type' => 'text', 'value' => null, 'order' => 60]);
        DB::table('sitesettings')->insert(['name' => 'Footer E-Posta', 'slug' => 'footer-email', 'type' => 'email', 'value' => null, 'order' => 70]);

        /* Sosyal Medya Hesapları */
        DB::table('sitesettings')->insert(['name' => 'Facebook URL', 'slug' => 'social-facebook', 'type' => 'url', 'value' => null, 'order' => 80]);
        DB::table('sitesettings')->insert(['name' => 'Instagram URL', 'slug' => 'social-instagram', 'type' => 'url', 'value' => null, 'order' => 90]);
        DB::table('sitesettings')->insert(['name' => 'Linked-in URL', 'slug' => 'social-linkedin', 'type' => 'url', 'value' => null, 'order' => 100]);
        DB::table('sitesettings')->insert(['name' => 'Twitter URL', 'slug' => 'social-twitter', 'type' => 'url', 'value' => null, 'order' => 110]);
        DB::table('sitesettings')->insert(['name' => 'Youtube URL', 'slug' => 'social-youtube', 'type' => 'url', 'value' => null, 'order' => 120]);

        /* Copyright */
        DB::table('sitesettings')->insert(['name' => 'Copyright Metni', 'slug' => 'copyright', 'type' => 'text', 'value' => null, 'order' => 130]);

        /* Google Analytics  */
        DB::table('sitesettings')->insert(['name' => 'Google Analytics Code', 'slug' => 'google-analytics-code', 'type' => 'text', 'value' => null, 'order' => 140]);
    }
}
