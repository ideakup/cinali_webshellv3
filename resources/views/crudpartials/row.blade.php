@php

    $_row_visible = false;
    $_row_value = '';
    $_row_disabled = '';
    
    // $_row_visible
    if (Request::segment(4) == 'edit' && !is_null(Request::segment(6))){
    	if ($content->type == 'text'){
    		$_row_visible = true;
    	}
    }

    // $_row_value
    

    // $_row_disabled
    if (Request::segment(4) == 'delete'){
        $_row_disabled = ' disabled="disabled" ';
    }

@endphp

@if ($_row_visible)

	<hr>
    <div class="form-group m-form__group row">
        <div class="col-10 ml-auto">
            <h3 class="m-form__section">
                Fon Görseli
            </h3>
        </div>
    </div>

	<div class="form-group m-form__group row @if ($errors->has('row')) has-danger @endif">
        <label for="example-text-input" class="col-2 col-form-label">
            Satır Boyutu
        </label>
        <div class="col-7">
            @if ($errors->has('row'))
                <div id="row-error" class="form-control-feedback">{{ $errors->first('row') }}</div>
            @endif

            <select class="form-control m-select2" id="row" name="row" {!! $_row_disabled !!} >
                <option value="normal" @if ($content->variableLang(Request::segment(6))->row == old('row') || $content->variableLang(Request::segment(6))->row == 'normal') {{ 'selected' }} @endif> Normal </option>
                <option value="full" @if ($content->variableLang(Request::segment(6))->row == old('row') || $content->variableLang(Request::segment(6))->row == 'full') {{ 'selected' }} @endif> Full </option>
            </select>

        </div>
    </div>

@endif