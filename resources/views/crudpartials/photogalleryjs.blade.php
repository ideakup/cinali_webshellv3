@if (Request::segment(4) == 'galup' && is_null(Request::segment(6)))
    
    @if ($content->type == 'photogallery')
        
        <script type="text/javascript">

            function photoGalleryCrop(file, pid){

                console.log('transformFile');
                console.log(file);
                console.log(pid);
                

                var myDropZone = this;
                // Create the image editor overlay
                var editor = document.createElement('div');
                editor.style.position = 'fixed';
                editor.style.left = 0;
                editor.style.right = 0;
                editor.style.top = 0;
                editor.style.bottom = 0;
                editor.style.zIndex = 9999;
                editor.style.backgroundColor = '#000';
                document.body.appendChild(editor);
                // Create confirm button at the top left of the viewport
                var buttonConfirm = document.createElement('button');
                buttonConfirm.style.position = 'absolute';
                buttonConfirm.style.left = '10px';
                buttonConfirm.style.top = '10px';
                buttonConfirm.style.zIndex = 9999;
                buttonConfirm.textContent = 'Confirm';
                editor.appendChild(buttonConfirm);
                buttonConfirm.addEventListener('click', function() {
                    // Get the canvas with image data from Cropper.js
                    var canvas = cropper.getCroppedCanvas();
                    // Turn the canvas into a Blob (file object without a name)
                    canvas.toBlob(function(blob) {

                        //console.log(blob);
                        var formData = new FormData();
                        formData.append('uploadType', 'pgThumbnailCrop');
                        formData.append('file', blob);
                        formData.append('id', pid);

                        $.ajax('{{ url('/cropThumbnail') }}', {
                            method: "POST",
                            headers: {"X-CSRF-TOKEN" : $('#token').val()},
                            data: formData,
                            processData: false,
                            contentType: false,
                            success: function (xhr) {
                                var redata = jQuery.parseJSON(xhr);
                                console.log('Upload success');
                                //console.log($("#thumbnail-"+redata.id));
                                $("#thumbnail-"+redata.id).attr("src", "{{ url('upload/thumbnail/') }}/"+redata.location);
                            },
                            error: function () {
                                console.log('Upload error');
                            }
                        });

                    });
                        
                    // Remove the editor from the view
                    document.body.removeChild(editor);
                });


                var buttonTest = document.createElement('button');
                buttonTest.style.position = 'absolute';
                buttonTest.style.right = '10px';
                buttonTest.style.top = '10px';
                buttonTest.style.zIndex = 9999;
                buttonTest.textContent = 'Test';
                editor.appendChild(buttonTest);

                buttonTest.addEventListener('click', function() {
                    console.log(cropper.getImageData());
                    console.log(cropper.getCropBoxData());
                });

                // Create an image node for Cropper.js
                var image = new Image();
                image.src = file;
                editor.appendChild(image);

                // Create Cropper.js
                var cropper = new Cropper(image, { aspectRatio: {{config('webshell.upload.image.thumbnail.aspectRatio')}}, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, minCropBoxWidth: {{config('webshell.upload.image.thumbnail.width')}}, autoCropArea: 1});
            
            }

            Dropzone.options.dropzoneFileUpload = {
                headers: { 'X-CSRF-TOKEN': $('#token').val() },
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 5,
                maxFilesize: 5, // MB
                addRemoveLinks: true,
                accept: function(file, done) {
                    done();
                },
                init: function () {
                    //console.log('dropzone init');
                },
                sending: function(file, xhr, formData){
                    formData.append('uploadType', 'galleryimage');
                    formData.append('contentid', {{ $content->id }});
                    
                },
                success: function(file, xhr, event){
                    var data = jQuery.parseJSON(xhr);
                    $("#imgContainer").append('<div class="col-xl-4">'+
                    '    <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height" style="margin-bottom: 0;">'+
                    '        <div class="m-portlet__head m-portlet__head--fit">'+
                    '            <div class="m-portlet__head-tools">'+
                    '                <ul class="m-portlet__nav">'+
                    '                    <li class="m-portlet__nav-item" aria-expanded="true">'+
                    '                        <a href="{{ url('menu/content/'.Request::segment(3).'/edit/'.Request::segment(5).'/photogallery_edit/') }}/' + data.id + '" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">'+
                    '                           <i class="fa fa-cog"></i>'+
                    '                        </a>'+
                    '                    </li>'+
                    '                </ul>'+
                    '            </div>'+
                    '        </div>'+
                    '        <div class="m-portlet__body">'+
                    '            <div class="m-widget19">'+
                    '                <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">'+
                    '                    <img class="img-fluid" id="thumbnail-'+data.id+'" src="{{ url('upload/thumbnail/') }}/' + data.location + '" />'+
                    '                    <p class="m-widget19__title m--font-light" style="padding-bottom: 0;">'+
                    '                    </p>'+
                    '                    <div class="m-widget19__shadow" style="background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 1) 60%);"></div>'+
                    '               </div>'+
                    '           </div>'+
                    '        </div>'+
                    '    </div>'+
                    '</div>');
                    this.removeFile(file);
                }
            };

        </script>
        
    @endif

@endif