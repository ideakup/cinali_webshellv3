@php

    $_dropzonefile_visible = false;
    $_dropzonefile_type = '';
    $_dropzonefile_value = '';
    $_dropzonefile_disabled = '';
    
    // $_dropzonefile_visible
    if (Request::segment(4) == 'edit' && !is_null(Request::segment(6))){
        if ($content->type == 'file') {
            $_dropzonefile_visible = true;
            $_dropzonefile_type = 'file';
        }
    }
    
    // $_dropzonefile_value
    if (Request::segment(4) != 'add' && empty(old('order'))) {
        $_dropzonefile_value =  $content->order;
    } else {
        $_dropzonefile_value = old('order');
    }

    // $_dropzonefile_disabled
    if (Request::segment(4) == 'delete'){
        $_dropzonefile_disabled = ' disabled="disabled" ';
    }

@endphp

@if ($_dropzonefile_visible)

    <div class="m-portlet m-portlet--mobile">

        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">

                    @if ($_dropzonefile_type == 'file')
                        <h3 class="m-portlet__head-text"> Dosya Yükle </h3>
                    @endif

                </div>
            </div>

            <div class="m-portlet__head-tools" id="fileContainer">
                @php $isFileExist = false; @endphp
                @if (!is_null($content->variableLang(Request::segment(6))) && !empty($content->variableLang(Request::segment(6))->content))
                    @php $isFileExist = true; @endphp
                @endif
                
                @if($isFileExist)
                    <a href="{{ url('upload/files/'.$content->variableLang(Request::segment(6))->content) }}" target="_blank">{{ url('upload/files/'.$content->variableLang(Request::segment(6))->content) }}</a>
                @endif
                
            </div>
        </div>

        <div class="m-portlet__body">

            <div class="form-group m-form__group row">

                <div class="col-12">
                    <div class="m-dropzone dropzone" action="{{ url('/upload2File') }}" id="dropzonefileFileUpload">
                        <div class="m-dropzone__msg dz-message needsclick">
                            <h3 class="m-dropzone__msg-title">
                                Dosya yüklemek için dosyayı buraya sürükleyin yada bu alan tıklayın...
                            </h3>
                            <span class="m-dropzone__msg-desc">
                                Bu alan <strong>doc, docx, xls, xlsx</strong> ve <strong>pdf</strong> formatlarını yüklemenize izin verir.
                            </span>
                        </div>
                    </div>
                </div>

            </div>
            
        </div>

    </div>

@endif