@php

    $_tag_visible = false;
    $_tag_value = '';
    $_tag_disabled = '';
    
    // $_tag_visible
    if (Request::segment(4) == 'edit' && is_null(Request::segment(6))){
        if ($content->type == 'group' || $content->type == 'photogallery'){
            $_tag_visible = true;
        }
    }

    // $_tag_value


    // $_tag_disabled
    if (Request::segment(4) == 'delete'){
        $_tag_disabled = ' disabled="disabled" ';
    }

@endphp

@if ($_tag_visible)
    <div class="form-group m-form__group row @if ($errors->has('tag')) has-danger @endif">
        <label for="example-text-input" class="col-2 col-form-label">
            Etiket (Ne)
        </label>
        <div class="col-7">
            @if ($errors->has('tag'))
                <div id="tag-error" class="form-control-feedback">{{ $errors->first('tag') }}</div>
            @endif
            <select class="form-control m-select2" id="tag" name="tag[]" multiple="multiple" {!! $_tag_disabled !!} >
                <option value="null">Seçiniz...</option>

                @foreach ($tags as $tag)
                    <option value="{{ $tag->id }}" @if ($content->tags()->where('tag_id', $tag->id)->count()) {{ 'selected' }} @endif> {{ $tag->variableLang($langs->first()->code)->title }} </option>
                @endforeach
                
            </select>
        </div>
    </div>
@endif