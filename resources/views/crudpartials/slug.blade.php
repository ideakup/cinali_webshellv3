@php

    $_slug_visible = false;
    $_slug_value = '';
    $_slug_disabled = '';
    
    // $_slug_visible
    if (!is_null($menu) && $menu->type != 'content' /* && is_null($content->top_content)*/){
        $_slug_visible = true;
    }

    // $_slug_value
    if (Request::segment(4) != 'add' && empty(old('slug'))) {
        if(is_null($content->variableLang(Request::segment(6)))){
            $_slug_value =  $content->variableLang($langs->first()->code)->slug;
        } else {
            $_slug_value = $content->variableLang(Request::segment(6))->slug;
        }
    } else {
        $_slug_value = old('slug');
    }

    // $_slug_disabled
    if (Request::segment(4) == 'add' || Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($content->variableLang(Request::segment(6))))){
        $_slug_disabled = ' disabled="disabled" ';
    }

@endphp

@if ($_slug_visible)
    <div class="form-group m-form__group row @if ($errors->has('slug')) has-danger @endif">
        <label for="example-text-input" class="col-2 col-form-label">
            Slug
        </label>
        <div class="col-7">
            <input class="form-control m-input" type="text" id="slug" name="slug" value="{{ $_slug_value }}" {!! $_slug_disabled !!} required>
            
            @if ($errors->has('slug'))
                <div id="slug-error" class="form-control-feedback">{{ $errors->first('slug') }}</div>
            @endif
        </div>
    </div>
@endif