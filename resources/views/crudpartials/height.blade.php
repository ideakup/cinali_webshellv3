@php

    $_height_visible = false;
    $_height_value = '';
    $_height_disabled = '';
    
    // $_height_visible
    if (Request::segment(4) == 'edit' && !is_null(Request::segment(6))){
    	if ($content->type == 'text'){
    		$_height_visible = true;
    	}
    }

    // $_height_value
    if (Request::segment(4) != 'add' && empty(old('height'))) {
		if (is_null($content->variableLang(Request::segment(6)))) {
			$_height_value = $content->variableLang($langs->first()->code)->height;
		} else {
			$_height_value = $content->variableLang(Request::segment(6))->height;
		}
	} else {
		$_height_value = old('height');
	}
    
    // $_height_disabled
    if (Request::segment(4) == 'delete'){
        $_height_disabled = ' disabled="disabled" ';
    }

@endphp

@if ($_height_visible)

	<div class="form-group m-form__group row @if ($errors->has('height')) has-danger @endif">
        <label for="example-text-input" class="col-2 col-form-label">
            Satır Yüksekliği (px)
        </label>
        <div class="col-7">
            <input class="form-control m-input" type="number" min="1" max="1000" id="height" name="height" value="{{ $_height_value }}" {!! $_height_disabled !!} >

            @if ($errors->has('height'))
                <div id="height-error" class="form-control-feedback">{{ $errors->first('height') }}</div>
            @endif
        </div>
    </div>

@endif