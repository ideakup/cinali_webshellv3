@php

    $_form_visible = false;
    $_form_value = '';
    $_form_disabled = '';
    
    // $_form_visible
    if (Request::segment(4) == 'edit' && !is_null(Request::segment(6))){
        if ($content->type == 'form'){
            $_form_visible = true;
        }
    }
    
    // $_form_value
    

    // $_form_disabled


@endphp

@if ($_form_visible)
 
    <div class="form-group m-form__group row">
        <div class="col-12">
            <div id="fb-editor"></div>
            <style type="text/css">
                #fb-editor {
                    background: #f5f5f5;
                    padding: 5px;
                    border-radius: 5px;
                    -webkit-box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);
                    -moz-box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);
                    box-shadow: 1px 1px 5px 0px rgba(0,0,0,0.75);
                }
                #fb-editor [class^="icon-"] {
                    display: inherit;
                }
                .form-builder-dialog code {
                    border: 0;
                    box-shadow: none;
                }
                #fb-editor .form-elements>.className-wrap,
                #fb-editor .form-elements>.placeholder-wrap,
                #fb-editor .form-elements>.access-wrap {
                    /*display: none !important;*/
                }
            </style>
        </div>
    </div>

@endif