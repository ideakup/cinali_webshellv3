@php

    $_category_visible = false;
    $_category_value = '';
    $_category_disabled = '';
    
    // $_category_visible
    if (Request::segment(4) == 'edit' && is_null(Request::segment(6))){
        if ($content->type == 'group' || $content->type == 'photogallery'){
            $_category_visible = true;
        }
    }

    // $_category_value
    

    // $_category_disabled
    if (Request::segment(4) == 'delete'){
        $_category_disabled = ' disabled="disabled" ';
    }

@endphp

@if ($_category_visible)
    <div class="form-group m-form__group row @if ($errors->has('category')) has-danger @endif">
        <label for="example-text-input" class="col-2 col-form-label">
            Kategori (Nerede)
        </label>
        <div class="col-7">
            @if ($errors->has('category'))
                <div id="category-error" class="form-control-feedback">{{ $errors->first('category') }}</div>
            @endif
            <select class="form-control m-select2" id="category" name="category[]" multiple="multiple" {!! $_category_disabled !!} >
                <option value="null">Seçiniz...</option>

                @foreach ($categories as $category)
                    <option value="{{ $category->id }}" @if ($content->categories()->where('category_id', $category->id)->count()) {{ 'selected' }} @endif> {{ $category->variableLang($langs->first()->code)->title }} </option>
                @endforeach
                
            </select>
        </div>
    </div>
@endif