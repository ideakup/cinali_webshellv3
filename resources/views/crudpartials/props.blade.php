@if (Request::segment(4) == 'edit' && is_null(Request::segment(6)))

    @if ($content->type == 'photo')
        <hr>

        <div class="form-group m-form__group row @if ($errors->has('props')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Gösterim Türü
            </label>
            <div class="col-7">
                @if ($errors->has('props'))
                    <div id="props-error" class="form-control-feedback">{{ $errors->first('props') }}</div>
                @endif
                <select class="form-control m-select2" id="props" name="props"
                    @if (Request::segment(4) == 'delete')
                        disabled="disabled" 
                    @endif
                >
                    
                    @foreach (config('webshell.content.photo.props') as $key => $conf)
                        <option value="{{ config('webshell.content.photo.props.'.$key.'.value') }}" @if (!empty($content) && $content->variableLang($langs->first()->code)->props == config('webshell.content.photo.props.'.$key.'.value')) {{ 'selected' }} @endif> {{config('webshell.content.photo.props.'.$key.'.label')}} </option>
                    @endforeach

                </select>
            </div>
        </div>
    @endif

    @if ($content->type == 'photogallery')
        <hr>
        <div class="form-group m-form__group row @if ($errors->has('props')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Kolon Sayısı
            </label>
            <div class="col-7">
                @if ($errors->has('props'))
                    <div id="props-error" class="form-control-feedback">{{ $errors->first('props') }}</div>
                @endif
                <select class="form-control m-select2" id="props" name="props"
                    @if (Request::segment(4) == 'delete')
                        disabled="disabled" 
                    @endif
                >
                    <option value="col-2x" @if (!empty($content) && $content->variableLang($langs->first()->code)->props == 'col-2x') {{ 'selected' }} @endif> 2 Kolon </option>
                    <option value="col-3x" @if (!empty($content) && $content->variableLang($langs->first()->code)->props == 'col-3x') {{ 'selected' }} @endif> 3 Kolon </option>
                    <option value="col-4x" @if (!empty($content) && $content->variableLang($langs->first()->code)->props == 'col-4x') {{ 'selected' }} @endif> 4 Kolon </option>
                </select>
            </div>
        </div>
    @endif

    @if ($content->type == 'seperator')
        <hr>

        <div class="form-group m-form__group row @if ($errors->has('props')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Fon Rengi
            </label>
            <div class="col-7">
                @if ($errors->has('props'))
                    <div id="props-error" class="form-control-feedback">{{ $errors->first('props') }}</div>
                @endif
                <select class="form-control m-select2" id="props" name="props"
                    @if (Request::segment(4) == 'delete')
                        disabled="disabled" 
                    @endif
                >
                    @foreach (config('webshell.seperator') as $key => $sep)

                        <option value="{{$sep['color']}}" @if (!empty($content) && $content->variableLang($langs->first()->code)->props == $sep['color']) {{ 'selected' }} @endif> {{$sep['name']}} </option>

                    @endforeach

                </select>
            </div>
        </div>
    @endif

@endif

@if (Request::segment(4) == 'edit' && !is_null(Request::segment(6)))
    
    @if ($content->type == 'link')
        
        <div class="m-form__group form-group row">
            <label for="example-text-input" class="col-2 col-form-label">
                Dahili / Dış Link
            </label>
            <div class="col-3">
                <span class="m-switch">
                    <label>
                        <input type="checkbox" @if ($content->variableLang(Request::segment(6))->props == 'external' || Request::segment(4) == 'add') {{ 'checked="checked"' }} @endif id="props" name="props" value="external" 
                            @if (Request::segment(4) == 'delete')
                                disabled="disabled" 
                            @endif
                        />
                        <span></span>
                    </label>
                </span>
            </div>
        </div>

    @endif

    @if ($content->type == 'form')

        <hr>
        <div class="form-group m-form__group row @if ($errors->has('props_eposta')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Bilgilendirme E-Postası
            </label>
            @php 
                $props_eposta_val = ''; 
            @endphp
            <div class="col-7">
                <input class="form-control m-input" type="text" id="props_eposta" name="props_eposta" 
                    @if (Request::segment(4) != 'add' && empty(old('props_eposta')))
                        @if(is_null($content->variableLang(Request::segment(6))))
                            @if(!empty(json_decode($content->variableLang($langs->first()->code)->props)->props_eposta))
                                @php 
                                    $props_eposta_val = json_decode($content->variableLang($langs->first()->code)->props)->props_eposta; 
                                @endphp
                            @endif
                        @else
                            @if(!empty(json_decode($content->variableLang(Request::segment(6))->props)->props_eposta))
                                @php 
                                    $props_eposta_val = json_decode($content->variableLang(Request::segment(6))->props)->props_eposta;
                                @endphp
                            @endif
                        @endif
                        value="{{ $props_eposta_val }}"
                    @else 
                        value="{{ old('props') }}" 
                    @endif
                    
                    @if (Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($content->variableLang(Request::segment(6)))))
                        disabled="disabled" 
                    @endif
                >
                @if ($errors->has('props_eposta'))
                    <div id="props_eposta-error" class="form-control-feedback">{{ $errors->first('props_eposta') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group m-form__group row @if ($errors->has('props_buttonname')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Buton Adı
            </label>
            @php 
                $props_buttonname_val = ''; 
            @endphp
            <div class="col-7">
                <input class="form-control m-input" type="text" id="props_buttonname" name="props_buttonname" 
                    @if (Request::segment(4) != 'add' && empty(old('props_buttonname')))
                        @if(is_null($content->variableLang(Request::segment(6))))
                            @if(!empty(json_decode($content->variableLang($langs->first()->code)->props)->props_buttonname))
                                @php 
                                    $props_buttonname_val = json_decode($content->variableLang($langs->first()->code)->props)->props_buttonname; 
                                @endphp
                            @endif
                        @else
                            @if(!empty(json_decode($content->variableLang(Request::segment(6))->props)->props_buttonname))
                                @php 
                                    $props_buttonname_val = json_decode($content->variableLang(Request::segment(6))->props)->props_buttonname;
                                @endphp
                            @endif
                        @endif
                        value="{{ $props_buttonname_val }}"
                    @else 
                        value="{{ old('props_buttonname') }}" 
                    @endif
                    
                    @if (Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($content->variableLang(Request::segment(6)))))
                        disabled="disabled" 
                    @endif
                >
                @if ($errors->has('props_buttonname'))
                    <div id="props_buttonname-error" class="form-control-feedback">{{ $errors->first('props_buttonname') }}</div>
                @endif
            </div>
        </div>
                            
    @endif

@endif