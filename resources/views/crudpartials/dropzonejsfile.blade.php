@php

	$_dropzonejsfile_visible = false;
    $_dropzonejsfile_type = '';
    $_dropzonejsfile_maxFiles = 0;

    // $_dropzonejsfile_visible
    if (Request::segment(4) == 'edit' && !is_null(Request::segment(6))){

        if ($content->type == 'file') {

            $_dropzonejsfile_visible = true;

        }

    }

@endphp

@if ($_dropzonejsfile_visible)
	
	<script type="text/javascript">

        Dropzone.options.dropzonefileFileUpload = {
            headers: { 'X-CSRF-TOKEN': $('#token').val() },
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 1,
            maxFilesize: 5, // MB
            addRemoveLinks: true,
            accept: function(file, done) {
                done();
            },
            init: function () {
                console.log('dropzone init');
            },
            sending: function(file, xhr, formData){
                formData.append('uploadType', 'file');
                formData.append('contentid', {{ $content->id }});
                formData.append('lang_code', '{{ Request::segment(6) }}');
            },
            success: function(file, xhr, event){

            	@if ($content->type == 'file')
		
					var data = jQuery.parseJSON(xhr);
	                $("#fileContainer").html('eklendi');
	                this.removeFile(file);

				@endif
                
            }
        };

    </script>

@endif