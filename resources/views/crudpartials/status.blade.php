@php

    $_status_visible = false;
    $_status_checked = '';
    $_status_disabled = '';
    
    // $_status_visible
    if (Request::segment(4) == 'add' || (Request::segment(4) == 'edit' && is_null(Request::segment(6)))){
        $_status_visible = true;
    }

    // $_status_checked
    if (Request::segment(4) == 'add' || $content->status == 'active') {
        $_status_checked =  'checked="checked"';
    }

    // $_status_disabled
    if (Request::segment(4) == 'delete'){
        $_status_disabled = ' disabled="disabled" ';
    }

@endphp

@if ($_status_visible)
    <div class="m-form__group form-group row">
        <label for="example-text-input" class="col-2 col-form-label">
            Pasif / Aktif
        </label>
        <div class="col-3">
            <span class="m-switch">
                <label>
                    <input type="checkbox" id="status" name="status" value="active" {!! $_status_checked !!} {!! $_status_disabled !!} />
                    <span></span>
                </label>
            </span>
        </div>
    </div>
@endif