@if ((Request::segment(4) == 'edit' || Request::segment(4) == 'galup') && !is_null(Request::segment(6)))

    @if ($content->type == 'photo')
        
        <script type="text/javascript">
        
            



            Dropzone.options.dropzoneFileUpload = {
                headers: { 'X-CSRF-TOKEN': $('#token').val() },
                paramName: "file", // The name that will be used to transfer the file
                maxFiles: 1,
                maxFilesize: 5, // MB
                addRemoveLinks: true,
                accept: function(file, done) {
                    done();
                },
                init: function () {
                    //console.log('dropzone init');
                },
                sending: function(file, xhr, formData){
                    formData.append('uploadType', 'contentimage');
                    formData.append('contentid', {{ $content->id }});
                    formData.append('lang_code', '{{ Request::segment(6) }}');
                },
                success: function(file, xhr, event){
                    var data = jQuery.parseJSON(xhr);
                    $("#imgContainer").html('<img class="img-fluid" src="{{ url('') }}/upload/xlarge/' + data.location + '" />');
                    this.removeFile(file);
                },
                transformFile: function(file, done) { 
                    console.log('transformFile');
                    console.log(file);
                    var myDropZone = this;
                    // Create the image editor overlay
                    var editor = document.createElement('div');
                    editor.style.position = 'fixed';
                    editor.style.left = 0;
                    editor.style.right = 0;
                    editor.style.top = 0;
                    editor.style.bottom = 0;
                    editor.style.zIndex = 9999;
                    editor.style.backgroundColor = '#000';
                    document.body.appendChild(editor);
                    // Create confirm button at the top left of the viewport
                    var buttonConfirm = document.createElement('button');
                    buttonConfirm.style.position = 'absolute';
                    buttonConfirm.style.left = '10px';
                    buttonConfirm.style.top = '10px';
                    buttonConfirm.style.zIndex = 9999;
                    buttonConfirm.textContent = 'Confirm';
                    editor.appendChild(buttonConfirm);
                    buttonConfirm.addEventListener('click', function() {
                        // Get the canvas with image data from Cropper.js
                        var canvas = cropper.getCroppedCanvas();
                        // Turn the canvas into a Blob (file object without a name)
                        canvas.toBlob(function(blob) {
                            // Create a new Dropzone file thumbnail

                            myDropZone.createThumbnail(
                                blob,
                                myDropZone.options.thumbnailWidth,
                                myDropZone.options.thumbnailHeight,
                                myDropZone.options.thumbnailMethod,
                                false, 
                                function(dataURL) {
                                    // Update the Dropzone file thumbnail
                                    myDropZone.emit('thumbnail', file, dataURL);
                                    // Return the file to Dropzone
                                    done(blob);

                                });
                            });
                        // Remove the editor from the view
                        document.body.removeChild(editor);
                    });


                    var buttonTest = document.createElement('button');
                    buttonTest.style.position = 'absolute';
                    buttonTest.style.left = '200px';
                    buttonTest.style.top = '10px';
                    buttonTest.style.zIndex = 9999;
                    buttonTest.textContent = 'Test';
                    editor.appendChild(buttonTest);

                    buttonTest.addEventListener('click', function() {
                        console.log(cropper.getImageData());
                        console.log(cropper.getCropBoxData());
                    });

                    // Create an image node for Cropper.js
                    var image = new Image();
                    image.src = URL.createObjectURL(file);
                    editor.appendChild(image);

                    // Create Cropper.js
                    var cropper = new Cropper(image, { aspectRatio: 2.5, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, minCropBoxWidth: 1920, minCropBoxHeight: 768, autoCropArea: 1, });
                    
                }
            };

        </script>

    @endif
    
@endif

