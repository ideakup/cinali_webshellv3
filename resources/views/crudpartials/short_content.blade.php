@php

    $_short_content_visible = false;
    $_short_content_value = '';
    $_short_content_disabled = '';
    

    // $_short_content_visible
    if (Request::segment(4) == 'edit' && !is_null(Request::segment(6))){
        if (true || $menu->type != 'content'){
            if ($content->type == 'group' || $content->type == 'text' || $content->type == 'photogallery' || $content->type == 'seperator'){
                $_short_content_visible = true;
            }
        }
    }

    // $_short_content_value
    if (Request::segment(4) != 'add' && empty(old('short_content'))) {
        if(is_null($content->variableLang(Request::segment(6)))){
            $_short_content_value = $content->variableLang($langs->first()->code)->short_content;
        } else {
            $_short_content_value = $content->variableLang(Request::segment(6))->short_content;
        }
    } else {
        $_short_content_value = old('short_content');
    }

    // $_short_content_disabled
    if (Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($content->variableLang(Request::segment(6))))){
        $_short_content_disabled = ' disabled="disabled" ';
    }

@endphp

@if ($_short_content_visible)
    <hr>
    <div class="form-group m-form__group row @if ($errors->has('short_content')) has-danger @endif">
        <label for="example-text-input" class="col-2 col-form-label">
            Kısa İçerik
        </label>
        <div class="col-7">
            <input class="form-control m-input" type="text" id="short_content" name="short_content" value="{{ $_short_content_value }}" {!! $_short_content_disabled !!} >
            @if ($errors->has('short_content'))
                <div id="short_content-error" class="form-control-feedback">{{ $errors->first('short_content') }}</div>
            @endif
        </div>
    </div>
@endif