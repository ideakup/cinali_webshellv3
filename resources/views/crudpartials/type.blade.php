@php

    $_type_visible = false;
    $_type_value = '';
    $_type_disabled = '';
    
    // $_type_visible
    if (Request::segment(4) == 'add'){
        $_type_visible = true;
    }

    // $_type_value
    

    // $_type_disabled
    if (Request::segment(4) == 'delete'){
        $_type_disabled = ' disabled="disabled" ';
    }else if(is_null($menu) && Request::segment(3) == 0) {
        $_type_disabled = ' disabled="disabled" ';
    }

@endphp

@if ($_type_visible)

    @if(is_null($menu) && Request::segment(3) == 0 && Request::segment(1) == 'form')
        
        <input type="hidden" name="type" value="{{ Request::segment(1) }}">

    @else

        <div class="form-group m-form__group row @if ($errors->has('type')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Tip
            </label>
            <div class="col-7">
                @if ($errors->has('type'))
                    <div id="type-error" class="form-control-feedback">{{ $errors->first('type') }}</div>
                @endif

                <select class="form-control m-select2" id="type" name="type" {!! $_type_disabled !!} >

                    <option value="null">Seçiniz...</option>

                    @if (is_null($content) && $menu->type == 'list')
                        <option value="group"> İçerik Grubu </option>
                    @else
                        <option value="text"> Metin & HTML </option>
                        <option value="photo"> Fotoğraf </option>
                        <option value="photogallery"> Foto Galeri </option>
                        <option value="link"> Button & Link </option>
                        <option value="slide"> Slide </option>
                        <option value="seperator"> Seperatör </option>
                        <option value="form" @if(is_null($menu) && Request::segment(3) == 0) selected @endif > Form </option>
                        <option value="file"> Dosya </option>
                    @endif
                    <!--
                        <option value="youtube"> Video (Youtube) </option>
                        <option value="vimeo"> Video (Vimeo) </option>
                        <option value="file"> Dosya </option>
                        <option value="audio"> Ses </option>
                        <option value="code"> Kod </option>
                    -->
                </select>
                
            </div>
        </div>

    @endif
    
@endif