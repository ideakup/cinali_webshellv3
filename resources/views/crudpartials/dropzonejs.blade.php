@php

	$_dropzonejs_visible = false;
    $_dropzonejs_type = '';
    $_dropzonejs_maxFiles = 0;

    // $_dropzonejs_visible
    if (Request::segment(4) == 'edit' && !is_null(Request::segment(6))){

        if ($content->type == 'photo') {

            $_dropzonejs_visible = true;
            $uploadConfig = config('webshell.upload.imageType.'.$content->type);
            $uploadThumbnailConfig = config('webshell.upload.imageThumbnail.thumbnail');

        }elseif($content->type == 'photogallery'){

            $_dropzonejs_visible = true;
            $uploadConfig = config('webshell.upload.imageType.'.$content->type);
            $uploadThumbnailConfig = config('webshell.upload.imageThumbnail.thumbnail');

        }elseif($content->type == 'slide'){

            $_dropzonejs_visible = true;
            $uploadConfig = config('webshell.upload.imageType.'.$content->type);
            $uploadThumbnailConfig = config('webshell.upload.imageThumbnail.thumbnail');

        }

    }

@endphp

@if ($_dropzonejs_visible)
	
	<script type="text/javascript">

        function photoGalleryThumbnailCrop(file, pid, type){

            console.log('transformFile');
            console.log(file);
            console.log(pid);
            
            // Create the image editor overlay
            var editor = document.createElement('div');
            editor.style.position = 'fixed';
            editor.style.left = 0;
            editor.style.right = 0;
            editor.style.top = 0;
            editor.style.bottom = 0;
            editor.style.zIndex = 9999;
            editor.style.backgroundColor = '#000';
            document.body.appendChild(editor);
            // Create confirm button at the top left of the viewport
            var buttonConfirm = document.createElement('button');
            buttonConfirm.style.position = 'absolute';
            buttonConfirm.style.left = '10px';
            buttonConfirm.style.top = '10px';
            buttonConfirm.style.zIndex = 9999;
            buttonConfirm.textContent = 'Onay';
            editor.appendChild(buttonConfirm);

            buttonConfirm.addEventListener('click', function() {
                // Get the canvas with image data from Cropper.js
                var canvas = cropper.getCroppedCanvas();
                // Turn the canvas into a Blob (file object without a name)
                canvas.toBlob(function(blob) {

                    var formData = new FormData();
                    formData.append('uploadType', type);
                    formData.append('file', blob);
                    formData.append('id', pid);
                    formData.append('lang_code', '{{ Request::segment(6) }}');

                    $.ajax('{{ url('/cropThumbnail') }}', {
                        method: "POST",
                        headers: {"X-CSRF-TOKEN" : $('#token').val()},
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (xhr) {
                            var redata = jQuery.parseJSON(xhr);
                            console.log('Upload success');
                            //console.log($("#thumbnail-"+redata.id));
                            $("#thumbnail-"+redata.id).attr("src", "{{ url('upload/thumbnail/') }}/"+redata.location);
                        },
                        error: function () {
                            console.log('Upload error');
                        }
                    });

                });
                    
                // Remove the editor from the view
                document.body.removeChild(editor);
            });

            var buttonCancel = document.createElement('button');
            buttonCancel.style.position = 'absolute';
            buttonCancel.style.right = '10px';
            buttonCancel.style.top = '10px';
            buttonCancel.style.zIndex = 9999;
            buttonCancel.textContent = 'İptal';
            editor.appendChild(buttonCancel);

            buttonCancel.addEventListener('click', function() {
                editor.remove();
            });

            // Create an image node for Cropper.js
            var image = new Image();
            image.src = file;
            editor.appendChild(image);

            // Create Cropper.js
            var cropper = new Cropper(image, {aspectRatio: {{ config('webshell.upload.imageThumbnail.thumbnail.aspectRatio') }}, dragMode: 'move', zoomable: true, movable: true, zoomOnTouch:false, zoomOnWheel:true, wheelZoomRatio:false, minCropBoxWidth: {{ config('webshell.upload.imageThumbnail.thumbnail.width') }}, autoCropArea: 1});
        
        }

        Dropzone.options.dropzoneFileUpload = {
            headers: { 'X-CSRF-TOKEN': $('#token').val() },
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: {{ $uploadConfig['maxFiles'] }},
            maxFilesize: 5, // MB
            addRemoveLinks: true,
            accept: function(file, done) {
                done();
            },
            init: function () {
                //console.log('dropzone init');
            },
            sending: function(file, xhr, formData){
                formData.append('uploadType', '{{ $uploadConfig['uploadType'] }}');
                formData.append('contentid', {{ $content->id }});
                formData.append('lang_code', '{{ Request::segment(6) }}');
            },
            success: function(file, xhr, event){

            	@if ($content->type == 'photo')
		
					var data = jQuery.parseJSON(xhr);
	                $("#imgContainer").html('<img class="img-fluid" src="{{ url('/upload/xlarge') }}/' + data.location + '" />');
                    $("#cropPhotoButton").attr("onclick", "photoGalleryThumbnailCrop('{{ url('upload/xlarge') }}/" + data.location + "', " + data.id + ", '{{ $content->type }}')");
	                this.removeFile(file);

				@elseif($content->type == 'photogallery')
				   
					var data = jQuery.parseJSON(xhr);
                    $("#imgContainer").append('<div class="col-4">'+
                    '    <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height" style="margin-bottom: 0;">'+
                    '        <div class="m-portlet__head m-portlet__head--fit" style="padding: 0 15px;">'+
                    '            <div class="m-portlet__head-tools">'+
                    '                <ul class="m-portlet__nav">'+
                    '                    <li class="m-portlet__nav-item" aria-expanded="true">'+
                    '                       <a href="{{ url('menu/content/'.Request::segment(3).'/edit/'.Request::segment(5).'/photogallery_edit/') }}/' + data.id + '/{{ Request::segment(6) }}" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">'+
                    '                           <i class="fa fa-cog"></i>'+
                    '                       </a>'+
                    '                       <a href="#" onclick="photoGalleryThumbnailCrop(\'{!! url('upload/org/') !!}/' + data.location + '\', \'' + data.id + '\', \'{{ $content->type }}\'); return false;"  class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">'+
                    '                           <i class="fa fa-crop"></i>'+
                    '                       </a>'+
                    '                    </li>'+
                    '                </ul>'+
                    '            </div>'+
                    '        </div>'+
                    '        <div class="m-portlet__body">'+
                    '            <div class="m-widget19">'+
                    '                <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">'+
                    '                    <img class="img-fluid" id="thumbnail-'+data.id+'" src="{{ url('upload/thumbnail/') }}/' + data.location + '" />'+
                    '                    <p class="m-widget19__title m--font-light" style="padding-bottom: 0;">'+
                    '                    </p>'+
                    '                    <div class="m-widget19__shadow" style="background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 1) 60%);"></div>'+
                    '               </div>'+
                    '           </div>'+
                    '        </div>'+
                    '    </div>'+
                    '</div>');
                    this.removeFile(file);

			    @elseif($content->type == 'slide')
			        
			        var data = jQuery.parseJSON(xhr);
                    $("#imgContainer").append('<div class="col-xl-4">'+
                    '    <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height" style="margin-bottom: 0;">'+
                    '        <div class="m-portlet__head m-portlet__head--fit">'+
                    '            <div class="m-portlet__head-tools">'+
                    '                <ul class="m-portlet__nav">'+
                    '                    <li class="m-portlet__nav-item" aria-expanded="true">'+
                    '                        <a href="{{ url('menu/content/'.Request::segment(3).'/edit/'.Request::segment(5).'/slide_edit/') }}/' + data.id + '" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">'+
                    '                           <i class="fa fa-cog"></i>'+
                    '                        </a>'+
                    '                    </li>'+
                    '                </ul>'+
                    '            </div>'+
                    '        </div>'+
                    '        <div class="m-portlet__body">'+
                    '            <div class="m-widget19">'+
                    '                <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">'+
                    '                    <img class="img-fluid" src="{{ url('upload/thumbnail/') }}/' + data.location + '" />'+
                    '                    <p class="m-widget19__title m--font-light" style="padding-bottom: 0;">'+
                    '                    </p>'+
                    '                    <div class="m-widget19__shadow" style="background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 1) 60%);"></div>'+
                    '               </div>'+
                    '           </div>'+
                    '        </div>'+
                    '    </div>'+
                    '</div>');
                    this.removeFile(file);

			    @endif
                
            },
            transformFile: function(file, done) { 
                console.log('transformFile');
                console.log(file);
                var myDropZone = this;
                // Create the image editor overlay
                var editor = document.createElement('div');
                editor.style.position = 'fixed';
                editor.style.left = 0;
                editor.style.right = 0;
                editor.style.top = 0;
                editor.style.bottom = 0;
                editor.style.zIndex = 9999;
                editor.style.backgroundColor = '#000';
                document.body.appendChild(editor);

                // Create confirm button at the top left of the viewport
                var buttonConfirm = document.createElement('button');
                buttonConfirm.style.position = 'absolute';
                buttonConfirm.style.left = '10px';
                buttonConfirm.style.top = '10px';
                buttonConfirm.style.zIndex = 9999;
                buttonConfirm.textContent = 'Onayla';
                editor.appendChild(buttonConfirm);
                buttonConfirm.addEventListener('click', function() {
                    // Get the canvas with image data from Cropper.js
                    var canvas = cropper.getCroppedCanvas();
                    // Turn the canvas into a Blob (file object without a name)
                    canvas.toBlob(function(blob) {
                        // Create a new Dropzone file thumbnail

                        myDropZone.createThumbnail(
                            blob,
                            myDropZone.options.thumbnailWidth,
                            myDropZone.options.thumbnailHeight,
                            myDropZone.options.thumbnailMethod,
                            false, 
                            function(dataURL) {
                                // Update the Dropzone file thumbnail
                                myDropZone.emit('thumbnail', file, dataURL);
                                // Return the file to Dropzone
                                done(blob);

                            });
                        });
                    // Remove the editor from the view
                    document.body.removeChild(editor);
                });

                var buttonCancel = document.createElement('button');
                buttonCancel.style.position = 'absolute';
                buttonCancel.style.right = '10px';
                buttonCancel.style.top = '10px';
                buttonCancel.style.zIndex = 9999;
                buttonCancel.textContent = 'İptal';
                editor.appendChild(buttonCancel);

                buttonCancel.addEventListener('click', function() {
                    editor.remove();
                });

                var selectSize = document.createElement('select');
                selectSize.setAttribute("id", "selectSize");
                selectSize.style.position = 'absolute';
                selectSize.style.left = '40%';
                selectSize.style.top = '10px';
                selectSize.style.width = '20%';
                selectSize.style.zIndex = 9999;

                @foreach ($uploadConfig['type'] as $key => $value)
                    var opt = document.createElement('option');
                    opt.setAttribute("label", "{{ $value['title'] }}");
                    opt.setAttribute("value", {{ $value['aspectRatio'] }});
                    selectSize.appendChild(opt);
                @endforeach

                editor.appendChild(selectSize);

                selectSize.addEventListener('change', function() {
                    //console.log(this.value);
                    cropper.setAspectRatio(this.value);
                });

                // Create an image node for Cropper.js
                var image = new Image();
                image.src = URL.createObjectURL(file);
                editor.appendChild(image);

                // Create Cropper.js
                var cropper = new Cropper(image, { aspectRatio: {{ $uploadConfig['type']['default']['aspectRatio'] }}, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 1, });
                
            }
        };

    </script>

@endif