@if ((Request::segment(4) == 'edit' || Request::segment(4) == 'galup') && !is_null(Request::segment(6)))
    @if ($content->type == 'text')
        
        <script type="text/javascript">
        
            tinymce.init({
                selector: 'textarea#content',

                @if (Request::segment(4) == 'delete')
                    readonly : 1,
                @endif
                
                //plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
                //Color Picker, Context Menu, MoxieManager, noneditable, pageembed,  Text Color documentation
                //, linkchecker TEKRAR BAK
                //codesample, directionality, fullpage, help, pagebreak, paste, print, template, textpattern, toc, visualchars

                plugins: 'advlist lists anchor autolink autoresize charmap code fullscreen hr image imagetools insertdatetime link media nonbreaking preview searchreplace table visualblocks wordcount importcss paste',
                
                // table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol
                // formatselect styleselect fontselect fontsizeselect
                toolbar: 'forMoreCustomButton | undo redo | formatselect | bold italic underline strikethrough subscript superscript | removeformat | forecolor backcolor | alignleft aligncenter alignright alignjustify alignnone | numlist bullist outdent indent | link image media | anchor charmap hr insertdatetime nonbreaking | searchreplace | visualblocks code fullscreen',
                imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",

                branding: false,
                min_height: 400,

                relative_urls: false,
                convert_urls: false,

                /* without images_upload_url set, Upload tab won't show up*/
                images_upload_url: '{{ url('/uploadFile') }}',
                images_upload_base_path: '{{ url('/') }}',
                images_upload_credentials: true,
                images_upload_handler: function (blobInfo, success, failure) {
                   var xhr, formData;
                   xhr = new XMLHttpRequest();
                   xhr.withCredentials = false;
                   xhr.open('POST', '{{ url('/uploadFile') }}');
                   var token = '{{ csrf_token() }}';
                   xhr.setRequestHeader("X-CSRF-Token", token);
                   xhr.onload = function() {
                       var json;
                       if (xhr.status != 200) {
                           failure('HTTP Error: ' + xhr.status);
                           return;
                       }
                       json = JSON.parse(xhr.responseText);

                       if (!json || typeof json.location != 'string') {
                           failure('Invalid JSON: ' + xhr.responseText);
                           return;
                       }
                       success(json.location);
                   };
                   formData = new FormData();
                   formData.append('file', blobInfo.blob(), blobInfo.filename());
                   formData.append('uploadType', 'editor');
                   xhr.send(formData);
                },
                extended_valid_elements: "formore",
                custom_elements: "formore",
                setup: function (editor) {
                    editor.ui.registry.addButton('forMoreCustomButton', {
                        icon: 'flip-vertically',
                        onAction: function (_) {
                            editor.insertContent('<formore></formore>');
                        }
                    });
                },
                content_css: "/css/tinymce_inline.css",
            });

            /*
                Dropzone.options.dropzoneBgFileUpload = {
                    headers: { 'X-CSRF-TOKEN': $('#token').val() },
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        formData.append('uploadType', 'contentbgimage');
                        formData.append('contentid', {{ $content->id }});
                        formData.append('lang_code', '{{ Request::segment(6) }}');
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#bgImgContainer").html('<img class="img-fluid" src="{{ url('') }}/upload/xlarge/' + data.location + '" />');
                        this.removeFile(file);
                    }
                };
            */

        </script>

    @endif
@endif

