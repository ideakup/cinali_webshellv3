@php

    $_content_visible = false;
    $_content_value = '';
    $_content_disabled = '';
    
    // $_content_visible
    if (Request::segment(4) == 'edit' && !is_null(Request::segment(6))){
    	if ($content->type == 'text'){
    		$_content_visible = true;
    	}else if ($content->type == 'link'){
    		$_content_visible = true;
    	}
    }
    
    // $_content_value
    if (Request::segment(4) != 'add' && empty(old('content'))) {
		if (is_null($content->variableLang(Request::segment(6)))) {
			$_content_value = $content->variableLang($langs->first()->code)->content;
		} else {
			$_content_value = $content->variableLang(Request::segment(6))->content;
		}
	} else {
		$_content_value = old('content');
	}

    // $_content_disabled
    if (Request::segment(4) == 'delete'){
        $_content_disabled = ' disabled="disabled" ';
    }

@endphp

@if ($_content_visible)

	@if ($content->type == 'text')

		<div class="form-group m-form__group" style="padding-bottom: 0;">
		    <label>İçerik</label>
		</div>

		<div class="form-group m-form__group @if ($errors->has('content')) has-danger @endif">
		    <textarea class="form-control m-input" id="content" name="content" rows="3">{!! $_content_value !!}</textarea>

		    @if ($errors->has('content'))
		        <div id="content-error" class="form-control-feedback">{{ $errors->first('content') }}</div>
		    @endif
		</div>

	@endif

	@if ($content->type == 'link')
		
		<hr>
        <div class="form-group m-form__group row @if ($errors->has('content')) has-danger @endif">
            <label for="example-text-input" class="col-2 col-form-label">
                Link
            </label>
            <div class="col-7">
                <input class="form-control m-input" type="text" id="content" name="content" value="{{ $_content_value }}" {!! $_content_disabled !!} required>
                
                @if ($errors->has('content'))
                    <div id="content-error" class="form-control-feedback">{{ $errors->first('content') }}</div>
                @endif
            </div>
        </div>

	@endif

@endif