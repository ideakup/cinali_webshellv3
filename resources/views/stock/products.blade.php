@extends('layouts.app') @php use Carbon\Carbon; @endphp

@section('content')
<div class="page-content-wrapper">
    <div class="page-content"> 
        <!-- SAYFA BAŞLIĞI -->   
        <div class="page-head">
            <div class="page-title">
                <h1>Ürünler
                    <small>Ürün barkodu yazdırma bölümü</small>
                </h1>
            </div>
            <div class="page-action">
                <a href="{{url('settings/barcode')}}" class="btn btn-sm btn-default"><i class="fa fa-text-width" aria-hidden="true"></i> Barkod Ölçüleri</a>
            </div>
        </div>

        <!-- !!! HER SAYFADA OLACAK !!! -->
        <!-- BREADCRUMBS START -->
        @include('partials.breadcrumbs')  
        <!-- BREADCRUMBS END --> 
    
        <!-- İÇERİK -->
        <div class="row">
            <div class="col-md-8">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-body">
                        <div class="table-scrollable table-scrollable-borderless">
                            <table class="table table-hover table-light">
                                <thead>
                                    <tr class="uppercase">
                                        <th> # </th>
                                        <th> Ürün İsmi </th>
                                        <th> Barkod No </th>
                                        <th> Stok Sayısı </th>
                                        <th> İşlem </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($pro_list as $pro)
                                        <tr class="{{($pro['relation'] == 0) ? 'warning' : ''}}">
                                            <td> {{$i}} </td>
                                            <td> {{$pro['name']}} </td>
                                            <td>
                                                @if (is_null($pro['barcode']))
                                                    <span class="badge badge-warning">Yok</span>
                                                @else
                                                    {{$pro['barcode']}}
                                                @endif
                                            </td>
                                            <td> {{intval($pro['stock'])}} </td>
                                            <td> 
                                                @if ($pro['relation'] == 0)
                                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
                                                    Önce bu ürünü <a href="{{url('parasut/products')}}">ilişkilendirin</a>.
                                                @elseif($pro['relation'] == 1)
                                                    @if (is_null($pro['barcode']))
                                                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
                                                        Önce bu ürüne bir barkod numarası <a href="{{url('https://uygulama.parasut.com/201695/hizmet-ve-urunler/'.$pro['id'].'/duzenle')}}" target="_blank">ekleyin</a>.
                                                    @else
                                                        <a data-toggle="modal" href="#modal-single-{{$pro['id']}}" class="btn btn-icon-only grey" title="Ürün Barkodu"><i class="fa fa-tag" aria-hidden="true"></i></a>
                                                    @endif
                                                @endif                                                
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="modal-single-{{$pro['id']}}" tabindex="-1" role="modal-{{$pro['id']}}" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title">
                                                            Tek Ürün Barkodu | 
                                                            {{$pro['name']}}
                                                            <span class="badge badge-default">{{$pro['code']}}</span>
                                                        </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div id="canvas1-{{$pro['id']}}" class="row">
                                                            <div class="col-md-12">
                                                                <div class="barcode">
                                                                    {!! $pro['img'] !!}
                                                                    <h3 class="margin-0">{{$pro['barcode']}}</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-default" onclick="printBarcode(1, {{$pro['id']}})">
                                                            <i class="fa fa-print" aria-hidden="true"></i> Yazdır
                                                        </button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        @php
                                            $i++;
                                        @endphp  
                                    @endforeach                            
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">    
    var printBarcode = function(type, d){
        $('#canvas' + type + '-' + d).print({
            mediaPrint: true,
        }); 
    }
	$(document).ready(function(){
		
	});
</script>
@endsection
