@extends('layouts.webshell') 
@php use Carbon\Carbon; @endphp

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü 
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            Depo Barkod İşlemleri
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Depo Barkod İşlemleri
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <!-- <a href="{{url('settings/barcode')}}" class="btn btn-sm btn-default"><i class="fa fa-text-width" aria-hidden="true"></i> Barkod Ölçüleri</a> -->
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="m-section__content">
                    <table class="table m-table">
                        
                        <thead>
                            <tr>
                                <th> P. ID </th>
                                <th> Ürün Adı </th>
                                <th> Barkod No</th>
                                <th> Barkod <br> Stok Sayısı </th>
                                <th> Paraşüt <br> Stok Sayısı </th>
                                <th> Birim </th>
                                <th> Barcode <br> Görüntüle </th>
                                <th> Barcode <br> Oluştur </th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($parasut_products as $product)
                                <form class="form-horizontal" role="form" method="POST" action="{{url('stock/make_barcodes')}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="product_id" value="{{ $product['id'] }}">

                                    <tr class="
                                        @if ($product['status'] == 'error')
                                            {{ 'm-table__row--danger' }}
                                        @elseif ($product['status'] == 'relationship_success')
                                            {{-- 'info' --}}
                                        @elseif ($product['status'] == 'relationship_null')
                                            {{ 'm-table__row--warning' }}
                                        @endif
                                    ">
                                        <td> {{ $product['id'] }} </td>
                                        <td> {{ $product['attributes']['name'] }} </td>
                                        <td> 
                                            @if (empty($product['attributes']['barcode']))
                                                <i class="fa fa-warning" aria-hidden="true"></i>
                                            @else
                                                {{ $product['attributes']['barcode'] }}
                                            @endif
                                        </td>
                                            @php
                                                $sum = 0;
                                            @endphp
                                            @if (isset($product['barcodes']))
                                                @foreach ($product['barcodes'] as $bbb)
                                                    @php
                                                        $sum += $bbb['group_count'];
                                                    @endphp
                                                @endforeach
                                            @else
                                                @php
                                                    $sum = 'Yok';
                                                @endphp
                                            @endif
                                        <td 
                                        @if ($sum != 'Yok')
                                            @if ($sum < $product['attributes']['stock_count'])
                                                style="background-color: #FDF3F3;"
                                            @elseif($sum > $product['attributes']['stock_count'])
                                                style="background-color: #F7F3F9;"
                                            @endif
                                        @endif
                                        >
                                            {{ $sum }}
                                        </td>
                                        <td 
                                        @if ($sum != 'Yok')
                                            @if ($sum < $product['attributes']['stock_count'])
                                                style="background-color: #FDF3F3;"
                                            @elseif($sum > $product['attributes']['stock_count'])
                                                style="background-color: #F7F3F9;"
                                            @endif
                                        @endif
                                        > {{ number_format($product['attributes']['stock_count'],0,',','') }}
                                        <td> {{ $product['attributes']['unit'] }} </td>
                                        <td style="width: 100px;">
                                            @if ($product['status'] == 'relationship_success')
                                                <a data-toggle="modal" href="#modal-single-{{$product['id']}}" class="btn btn-icon-only grey" title="Ürün Barkodu"><i class="fa fa-tag" aria-hidden="true"></i></a>
                                                <a data-toggle="modal" href="#modal-multi-{{$product['id']}}" class="btn btn-icon-only grey-cascade" title="Kutu Barkodları"><i class="fa fa-tags" aria-hidden="true"></i></a>
                                            @endif
                                        </td>
                                        <td style="width: 180px;">
                                            @if ($product['status'] == 'relationship_success')
                                                <div class="form-group">
                                                    <input type="number" id="pctc-{{$product['id']}}" class="form-control" name="pctc" min="0" max="999" placeholder="Kutu Sayısı" required />
                                                    <input type="number" id="pct-{{$product['id']}}" class="form-control" name="pct" min="0" max="999" placeholder="Kutudaki Ürün Sayısı" required />
                                                    <button type="submit" class="btn btn-primary form-control" title="Barkod Üret"><i class="fa fa-barcode" aria-hidden="true"></i> Barcode Üret </button>
                                                </div>
                                            @else
                                                {{ $product['status'] }} - {{ $product['status_text'] }}
                                            @endif
                                        </td>
                                    </tr>
                                    @php
                                        //dump($product);
                                    @endphp
                                    @if (/*$product['status'] == 'relationship_success' && !is_null($product['barcodes'])*/ true)
                                        <div class="modal fade" id="modal-single-{{$product['id']}}" tabindex="-1" role="modal-{{$product['id']}}" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title">
                                                            Tek Ürün Barkodu | 
                                                            {{$product['attributes']['name']}}
                                                            <span class="badge badge-default">{{$product['attributes']['barcode']}}</span>
                                                        </h4>
                                                    </div>
                                                    <div class="modal-body">

                                                        <div id="canvas1-{{$product['id']}}" class="row">
                                                            <div class="col-md-12">
                                                                @if (!is_null($product['barcodeimg']))
                                                                    <div class="barcode">
                                                                        {!! $product['barcodeimg'] !!}
                                                                        <h4 class="margin-0">{{$product['barcode']}}</h4>
                                                                        <p class="margin-0" style="font-size: 12px;">{{$product['attributes']['name']}}</p>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-default" onclick="printBarcode(1, {{$product['id']}})">
                                                            <i class="fa fa-print" aria-hidden="true"></i> Yazdır
                                                        </button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                                <!-- /.modal-dialog -->
                                        </div>
                                        <div class="modal fade" id="modal-multi-{{$product['id']}}" tabindex="-1" role="modal-{{$product['id']}}" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title">
                                                            Ürün Kutu Barkodu | 
                                                            {{$product['attributes']['name']}}
                                                            <span class="badge badge-default">{{$product['attributes']['barcode']}}</span>
                                                        </h4>
                                                    </div>
                                                    <div class="modal-body">

                                                        <div class="row" id="canvas2-{{$product['id']}}">
                                                            <div class="col-md-12">
                                                                @if (!is_null($product['barcodes']))
                                                                    @foreach ($product['barcodes'] as $barcode_view)
                                                                        @if ($barcode_view['group_count'] > 1)
                                                                            <div class="barcode">
                                                                                {!! $barcode_view['img'] !!}
                                                                                <h4 class="margin-0">{{$barcode_view['barcode']}}</h4>
                                                                                <p class="margin-0" style="font-size: 12px;">{{$product['attributes']['name']}}</p>
                                                                            </div>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-default" onclick="printBarcode(2, {{$product['id']}})">
                                                            <i class="fa fa-print" aria-hidden="true"></i> Yazdır
                                                        </button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                    @endif
                                </form>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    var printBarcode = function(type, d){
        $('#canvas' + type + '-' + d).print({
            mediaPrint: true,
        }); 
    }
	$(document).ready(function(){
        $('.checkbox-group').change(function(){
            obj = $('#pct-' + $(this).attr('data-control'));
            if($(this).is(':checked') == true){
                obj.attr('required', true).attr('disabled', false);
            }else{
                obj.attr('required', false).attr('disabled', true);
            }
        });
	});
</script>
@endsection
