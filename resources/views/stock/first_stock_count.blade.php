@extends('layouts.app') @php use Carbon\Carbon; @endphp

@section('content')
<div class="page-content-wrapper">
    <div class="page-content"> 
        <!-- SAYFA BAŞLIĞI -->   
        <div class="page-head">
            <div class="page-title">
                <h1>Stok - Barkod
                    <small>Stok Girişi Barkod Oluşturma</small>
                </h1>
            </div>
            <div class="page-action">
                <a href="{{url('settings/barcode')}}" class="btn btn-sm btn-default"><i class="fa fa-text-width" aria-hidden="true"></i> Barkod Ölçüleri</a>
            </div>
        </div>

        <!-- !!! HER SAYFADA OLACAK !!! -->
        <!-- BREADCRUMBS START -->
        @include('partials.breadcrumbs')  
        <!-- BREADCRUMBS END --> 

        <div class="row">
            <div class="ol-lg-9">
                 <div class="portlet light portlet-fit bordered">
                    <div class="portlet-body">
                        <div class="table-scrollable table-scrollable-borderless">
                            <table class="table table-hover table-light">
                                <thead>
                                    <tr class="uppercase">
                                        <th> ID </th>
                                        <th> Ürün Adı </th>
                                        <th> Kod </th>
                                        <th> Barkod </th>
                                        <th> Birim </th>
                                        <th data-sortable="true"> Stok Sayısı </th>
                                        <th> İşlem </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($parasut_products as $product)
                                        
                                        @php
                                            //dump($product);
                                        @endphp


                                        <form class="form-horizontal" role="form" method="POST" action="{{url('stock/make_first_barcode')}}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="product_id" value="{{ $product['id'] }}">

                                            <tr class="
                                            @if ($product['status'] == 'error')
                                                {{ 'danger' }}
                                            @elseif ($product['status'] == 'relationship_success')
                                                {{ 'info' }}
                                            @elseif ($product['status'] == 'relationship_null')
                                                {{ 'warning' }}
                                            @endif
                                            ">
                                                <td> {{ $product['id'] }} </td>
                                                <td> {{ $product['attributes']['name'] }} </td>
                                                <td> 
                                                    @if (empty($product['attributes']['code']))
                                                        <i class="fa fa-warning" aria-hidden="true"></i>
                                                    @else
                                                        {{ $product['attributes']['code'] }}
                                                    @endif 
                                                </td>
                                                <td> 
                                                    @if (empty($product['attributes']['barcode']))
                                                        <i class="fa fa-warning" aria-hidden="true"></i>
                                                    @else
                                                        {{ $product['attributes']['barcode'] }}
                                                    @endif
                                                </td>
                                                <td> {{ $product['attributes']['unit'] }} </td>
                                                <td> {{ $product['attributes']['stock_count'] }} </td>
                                                <td style="width: 200px;">
                                                    
                                                    @if (/*$product['status'] == 'relationship_success'*/ true)
                                                        @php
                                                            //dump($product['relationship']);
                                                        @endphp
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <input type="checkbox" class="checkbox-group" data-control="{{$product['id']}}" checked>
                                                                <span></span>
                                                            </span>
                                                            <input type="number" id="pct-{{$product['id']}}" class="form-control" name="pct" min="2" max="999" placeholder="Kutu mik." required />
                                                            <span class="input-group-btn">
                                                                <button type="submit" class="btn btn-default" title="Barkod oluştur"><i class="fa fa-barcode" aria-hidden="true"></i></button>
                                                            </span>
                                                        </div>
                                                        <a data-toggle="modal" href="#modal-single-{{$product['id']}}" class="btn btn-icon-only grey" title="Ürün Barkodu"><i class="fa fa-tag" aria-hidden="true"></i></a>
                                                        <a data-toggle="modal" href="#modal-multi-{{$product['id']}}" class="btn btn-icon-only grey-cascade" title="Kutu Barkodları"><i class="fa fa-tags" aria-hidden="true"></i></a>
                                                    
                                                    
                                                    @else
                                                        {{ $product['status'] }} - {{ $product['status_text'] }}
                                                    @endif
                                                </td>
                                            </tr>
                                            @php
                                                //dump($product);
                                            @endphp
                                            @if (/*$product['status'] == 'relationship_success' && !is_null($product['barcodes'])*/ true)
                                                <div class="modal fade" id="modal-single-{{$product['id']}}" tabindex="-1" role="modal-{{$product['id']}}" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                <h4 class="modal-title">
                                                                    Tek Ürün Barkodu | 
                                                                    {{$product['attributes']['name']}}
                                                                    <span class="badge badge-default">{{$product['attributes']['code']}}</span>
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                
                                                                <div id="canvas1-{{$product['id']}}" class="row">
                                                                    <div class="col-md-12">
                                                                        @if (!is_null($product['barcodes']))
                                                                            @foreach ($product['barcodes'] as $barcode_view)
                                                                                @if ($barcode_view['group_count'] == 1)
                                                                                    <div class="barcode">
                                                                                        {!! $barcode_view['img'] !!}
                                                                                        <h3 class="margin-0">{{$barcode_view['barcode']}}</h3>
                                                                                    </div>
                                                                                    @break
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-default" onclick="printBarcode(1, {{$product['id']}})">
                                                                    <i class="fa fa-print" aria-hidden="true"></i> Yazdır
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                        <!-- /.modal-dialog -->
                                                </div>
                                                <div class="modal fade" id="modal-multi-{{$product['id']}}" tabindex="-1" role="modal-{{$product['id']}}" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                <h4 class="modal-title">
                                                                    Ürün Kutu Barkodu | 
                                                                    {{$product['attributes']['name']}}
                                                                    <span class="badge badge-default">{{$product['attributes']['code']}}</span>
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                
                                                                <div id="canvas2-{{$product['id']}}" class="row">
                                                                    <div class="col-md-12">
                                                                        @if (!is_null($product['barcodes']))
                                                                            @foreach ($product['barcodes'] as $barcode_view)
                                                                                @if ($barcode_view['group_count'] > 1)
                                                                                    <div class="barcode">
                                                                                        {!! $barcode_view['img'] !!}
                                                                                        <h3 class="margin-0">{{$barcode_view['barcode']}}</h3>
                                                                                    </div>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-default" onclick="printBarcode(2, {{$product['id']}})">
                                                                    <i class="fa fa-print" aria-hidden="true"></i> Yazdır
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                            @endif
                                        </form>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var printBarcode = function(type, d){
        $('#canvas' + type + '-' + d).print({
            mediaPrint: true,
        }); 
    }
	$(document).ready(function(){
        $('.checkbox-group').change(function(){
            obj = $('#pct-' + $(this).attr('data-control'));
            if($(this).is(':checked') == true){
                obj.attr('required', true).attr('disabled', false);
            }else{
                obj.attr('required', false).attr('disabled', true);
            }
        });
	});
</script>
@endsection
