@extends('layouts.nomenu') @php use Carbon\Carbon; @endphp

@section('content')

<div class="page-content"> 
    <!-- SAYFA BAŞLIĞI -->   
    <div class="page-head">
        <div class="page-title">
            <h1>Yeni Stok Sayımı
                <small></small>
            </h1>
        </div>
    </div>

    <!-- !!! HER SAYFADA OLACAK !!! -->
    <!-- BREADCRUMBS START -->
    @include('partials.breadcrumbs')  
    <!-- BREADCRUMBS END --> 

    <!-- İÇERİK -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <h4>Stok Sayımı</h4>
                </div>
                <div class="portlet-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{url('stock/taking_save')}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="idd" value="{{ (isset($resumeStockTracking)) ? $resumeStockTracking->id : '' }}">

                        <div class="form-group">
                            <div class="col-md-3">
                                <label> Sayımı Gerçekleştiren</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="staff" value="{{(isset($resumeStockTracking->staff)) ? $resumeStockTracking->staff : ''}}" required>
                            </div> 
                        </div> 
                        <div class="form-group">
                            <div class="col-md-3">
                                <label> Yetkili Sayım Eşlikçisi</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="controller_staff" value="{{(isset($resumeStockTracking->controller_staff)) ? $resumeStockTracking->controller_staff : ''}}" required>
                            </div> 
                        </div>  
                        <div class="form-group">
                            <div class="col-md-3">
                                <label> Sayım Notu</label>
                            </div>
                            <div class="col-md-4">
                                <textarea name="notes" class="form-control" rows="2">{{(isset($resumeStockTracking->notes)) ? $resumeStockTracking->notes : ''}}</textarea>
                            </div> 
                        </div>
                
                        <hr/>
                        <h4>Barkod Okutun</h4>
                        <div id="add-line">

                            @php
                                $inpt_num_last = 1;
                            @endphp
                            @if ($resumeStockTracking != null)
                                @php
                                    $reportArr = json_decode($resumeStockTracking->report);
                                    //dump($resumeStockTracking);
                                    //dump($reportArr->readBarcode);
                                @endphp
                                @foreach ($reportArr->readBarcode as $bbarcode)
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control barcode-input" name="take_{{$inpt_num_last}}" id="take_{{$inpt_num_last}}" data-num="{{$inpt_num_last}}" value="{{$bbarcode->take}}" readonly="readonly">
                                            <i id="fa-spin-{{$inpt_num_last}}" class="fa fa-spinner fa-spin fa-fw" style="display: none;"></i>
                                        </div> 
                                        <div class="col-md-2">
                                            <input type="number" placeholder="Tek ürün adedi..." min="0" class="form-control single-num" id="single_num_{{$inpt_num_last}}" name="num_{{$inpt_num_last}}" readonly="readonly" 
                                                @if (isset($bbarcode->num))
                                                     value="{{$bbarcode->num}}"
                                                @else
                                                    style="display: none;"
                                                @endif
                                            >
                                        </div>
                                        <div class="col-md-4" id="title_{{$inpt_num_last}}">
                                            @foreach ($parasut_products as $pproduct)
                                                @if ($pproduct['attributes']['code'] == $bbarcode->code)
                                                    {{ $pproduct['attributes']['name'] }}
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>   
                                    @php $inpt_num_last++; @endphp
                                @endforeach
                            @endif
                            
                            <div class="form-group">
                                <div class="col-md-4">
                                    <input type="text" class="form-control barcode-input" name="take_{{$inpt_num_last}}" id="take_{{$inpt_num_last}}" data-num="{{$inpt_num_last}}">
                                    <i id="fa-spin-{{$inpt_num_last}}" class="fa fa-spinner fa-spin fa-fw" style="display: none;"></i>
                                </div> 
                                <div class="col-md-2">
                                    <input type="number" placeholder="Tek ürün adedi..." min="0" class="form-control single-num" id="single_num_{{$inpt_num_last}}" name="num_{{$inpt_num_last}}" style="display: none;">
                                </div>
                                <div class="col-md-4" id="title_{{$inpt_num_last}}"></div>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn green pull-right">Sayımı Tamamla</button>
                            </div>
                        </div>

                    </form> 
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">

    function checkBarcode(inp){
        console.log("checkBarcode");
        inpt = inp;
        box = 0;

        inpt_num = parseInt(inpt.attr('data-num'));
        inpt_num_last = parseInt($('.barcode-input').last().attr('data-num')) + 1;
        inpt.attr('disabled', true);
        $("#fa-spin-" + inpt_num).show();
        
        if(inpt.val().split('G').length == 2){
            if(inpt.val().split('G')[1].split('N').length == 2){
                box = 1;
            }                
        }
        if(box == 0){
            $('#single_num_' + inpt_num).show();
            $('#single_num_' + inpt_num).focus();
            $('#single_num_' + inpt_num).attr('required', true);
        }else{
            $('#single_num_' + inpt_num).hide();
            $('#single_num_' + inpt_num).attr('required', false);
        }
        
        $.ajax({
            type: 'GET',
            url: "/stock/check_listen",
            dataType: 'json',
            data: {
                barcode: inpt.val(),
            },
            success: function (a) {
                inpt.removeClass('border-fail');
                inpt.addClass('border-success');
                $("#fa-spin-" + inpt_num).hide();
                inpt.attr('disabled', false);
                
                if(a['status'] == 'OK'){
                    $('#add-line').append(
                    '<div class="form-group">' +
                    '<div class="col-md-4">' +
                    '<input type="text" class="form-control barcode-input" name="take_' + inpt_num_last + '" id="take_' + inpt_num_last + '" data-num="' + inpt_num_last + '">' +
                    '<i id="fa-spin-' + inpt_num_last + '" class="fa fa-spinner fa-spin fa-fw" style="display: none;"></i>' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<input type="number" placeholder="Tek ürün Adedi" min="0" class="form-control single-num" id="single_num_' + inpt_num_last + '" name="num_' + inpt_num_last + '" style="display: none;">' +
                    '</div>' +
                    '<div class="col-md-4" id="title_' + inpt_num_last + '"></div>'+
                    '</div>'
                    );
                    $('#title_' + inpt_num).html(a['title']);
                    if(box == 1){
                        $('#take_' + inpt_num_last).focus();
                    }
                }
                
                             
            },
            error: function (a) {
                console.log(a.responseJSON);
                
                if(a.responseJSON == 'ERROR 01'){
                    inpt.removeClass('border-success');
                    inpt.addClass('border-fail');
                    $('#title_' + inpt_num).html("<strong style='color:#82030B'>Bu Kutu Barkodu Daha Önceden Okutuldu.</strong>");
                }else if(a.responseJSON == 'ERROR 02'){
                    inpt.removeClass('border-success');
                    inpt.addClass('border-fail');
                    $('#title_' + inpt_num).html("<strong style='color:#82030B'>Böyle Bir Barkod Yok.</strong>");
                }else if(a.responseJSON == 'ERROR 03'){                        
                    inpt.removeClass('border-success');
                    inpt.removeClass('border-fail');
                    $('#title_' + inpt_num).html("<strong style='color:#82030B'>Boş Bırakılamaz.</strong>");
                }

                inpt.attr('disabled', false);
                inpt.focus();
                inpt.select();
                $("#fa-spin-" + inpt_num).hide();
            },
            complete: function(a){
                //console.log(a.responseText)
            }
        }); 
        
    }


	$(document).ready(function(){
        var inpt_num; 
        var timer;

        $(document).on('keypress', 'input.barcode-input', function(){
            console.log("triger > keypress event");
            inp = $(this);
            clearTimeout(timer);
            
            timer = setTimeout(
                function(){ 
                    //checkBarcode(inp);
                    inp.blur();
                }
            , 250);
        });

        $(document).on('change', 'input.barcode-input', function(){
            console.log("triger > change event");
            checkBarcode($(this));
        });

	});
</script>
@endsection
