@extends('layouts.app') @php use Carbon\Carbon; @endphp

@section('content')
<div class="page-content-wrapper">
    <div class="page-content"> 
        <!-- SAYFA BAŞLIĞI -->   
        <div class="page-head">
            <div class="page-title">
                <h1>Stok Sayımı
                    <small></small>
                </h1>
            </div>
            <div class="page-action">
                <a href="{{ url('stock/newstocktaking') }}" class="btn btn-primary" >Yeni Sayım</a>
            </div>
        </div>

        <!-- !!! HER SAYFADA OLACAK !!! -->
        <!-- BREADCRUMBS START -->
        @include('partials.breadcrumbs')  
        <!-- BREADCRUMBS END --> 
    
        <!-- İÇERİK -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-body">
                        <div class="table-scrollable table-scrollable-borderless">
                            <table class="table table-hover table-light">
                                <thead>
                                    <tr class="uppercase">
                                        <th> # </th>
                                        <th style="width: 90px;"> Tarih </th>
                                        <th> Sayım Yapan <br> Eşlik Eden </th>
                                        <th> Not </th>
                                        <th style="width: 110px;"> Durum </th>
                                        <th style="width: 110px;"> İşlem </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($datas as $data)
                                        {{-- expr --}}
                                        <tr>
                                            <td> {{$i}} </td>
                                            <td> {{Carbon::parse($data->date)->format('d.m.Y H:m')}} </td>
                                            <td> {{$data->staff}} <br> {{$data->controller_staff}} </td>
                                            <td> {{$data->notes}}  </td>
                                            <td>
                                                @if ($data->cheked == 0 && Carbon::parse($data->date)->addHours(6) > Carbon::now())
                                                    <span class="badge badge-info"> Bekleniyor </span>
                                                @elseif($data->cheked == 1)
                                                    <span class="badge badge-success"> Onaylanmış </span>
                                                @else
                                                    <span class="badge badge-warning"> Zaman Aşımı </span>
                                                @endif
                                            </td>
                                            <td> 
                                                @if ($data->cheked == 0 && Carbon::parse($data->date)->addHours(6) > Carbon::now())
                                                    <a href="{{ url('stock/stocktakingdetail/'.$data->id) }}" class="btn green" title="Onayla"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    <a href="{{ url('stock/newstocktaking/'.$data->id) }}" class="btn btn-primary" title="Devam Et"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                                @elseif($data->cheked == 1)
                                                    <a href="{{ url('stock/stocktakingdetail/'.$data->id) }}" class="btn green" title="İncele"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                @else
                                                    <a href="{{ url('stock/stocktakingdetail/'.$data->id) }}" class="btn btn-warning" title="İptal Et"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        @php
                                            $i++;
                                        @endphp  
                                    @endforeach                            
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

