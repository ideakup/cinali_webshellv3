@extends('layouts.app') @php use Carbon\Carbon; @endphp

@section('content')
<div class="page-content-wrapper">
    <div class="page-content"> 
        <!-- SAYFA BAŞLIĞI -->   
        <div class="page-head">
            <div class="page-title">
                <h1>{{Carbon::parse($data->date)->format('d.m.Y H:m')}} 
                    <small>Tarihli Sayım Raporu</small>
                </h1>
            </div>
            <div class="page-action">
                <!--<a href="{{ url('stock/newstocktaking') }}" class="btn btn-success" >Yeni Sayım</a>-->
            </div>
        </div>

        <!-- !!! HER SAYFADA OLACAK !!! -->
        <!-- BREADCRUMBS START -->
        @include('partials.breadcrumbs')  
        <!-- BREADCRUMBS END --> 
    
        <!-- İÇERİK -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-body">
                        <div class="well">
                            <div class="row">
                                <div class="col-xs-12">
                                    <b>Sayım Gerçekleştiren:</b>
                                    {{$data->staff}}
                                </div>
                                <div class="col-xs-12">
                                    <b>Yetkili Sayım Eşlikçisi:</b>
                                    {{$data->controller_staff}}
                                </div>
                                <div class="col-xs-12">
                                    <b>Sayım Notu:</b>
                                    {{$data->notes}}
                                </div>
                            </div>
                        </div>
                        <div class="table-scrollable">
                            
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr class="uppercase">
                                        <th> Ürün Adı </th>
                                        <th style="width: 70px;"> Sayılan </th>
                                        <th style="width: 70px;"> Barkod </th>
                                        <th style="width: 70px;"> Paraşüt </th>
                                        <th> Açıklama </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $report = json_decode($data->report, true);
                                        $nowC = 0;
                                        $localC = 0;
                                        $parasutC = 0;
                                        $onayControl = true;
                                    @endphp
                                    @foreach ($report as $key => $values)
                                        @php 
                                            //dump($key); 
                                        @endphp
                                        @if ($key != 'readBarcode')
                                            <tr>
                                                <td> 
                                                    @foreach ($parasut_products as $pproduct)
                                                        @if ($pproduct['attributes']['code'] == $key)
                                                            {{ $pproduct['attributes']['name'] }} ({{ $key }})
                                                            @break
                                                        @endif
                                                    @endforeach
                                                </td>

                                                @foreach ($values as $keyvalue => $value)

                                                    <td> {{$value}} </td>

                                                    @if ($keyvalue == "nowCount")
                                                        @php $nowC = $value; @endphp
                                                    @elseif ($keyvalue == "localCount")
                                                        @php $localC = $value; @endphp
                                                    @elseif ($keyvalue == "parasutCount")
                                                        @php $parasutC = $value; @endphp
                                                    @endif
                                                
                                                @endforeach
                                                
                                                <td>
                                                    @if ($nowC == $parasutC)
                                                        <p class="text-success bold" style="margin-top: 5px; margin-bottom: 5px;">Sayım Geçerli.</p>
                                                        @if ($data->cheked == 0)
                                                            @if ($nowC == $localC)
                                                                <p class="text-success bold" style="margin-top: 5px; margin-bottom: 5px;">Paraşüt stoğuyla eşit sayıda sayıldı.</p>
                                                            @elseif ($nowC < $localC)
                                                                <p class="text-danger bold" style="margin-top: 5px; margin-bottom: 5px;">Barkod kayıtları düzeltilmeli...</p>
                                                            @elseif ($nowC > $localC)
                                                                <p class="text-danger bold" style="margin-top: 5px; margin-bottom: 5px;">Barkod kayıtlarında hata var...</p>
                                                            @endif
                                                        @endif
                                                    @elseif ($nowC < $parasutC)
                                                        @php
                                                            $onayControl = false;
                                                        @endphp
                                                        <p class="text-danger bold" style="margin-top: 5px; margin-bottom: 5px;">Sayım Hatalı. Paraşüt stoğundan daha az ürün sayıldı.</p>
                                                    @elseif ($nowC > $parasutC)
                                                        @php
                                                            $onayControl = false;
                                                        @endphp
                                                        <p class="font-yellow-gold bold" style="margin-top: 5px; margin-bottom: 5px;">Sayım Hatalı. Paraşüt stoğundan daha fazla ürün sayıldı.</p>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                        
                                    @endforeach                            
                                </tbody>
                            </table>

                        </div>


                        @if ($data->cheked == 0 && Carbon::parse($data->date)->addHours(6) > Carbon::now())

                            @if ($onayControl)

                                <form class="form-horizontal" role="form" method="POST" action="{{url('stock/taking_ok')}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="idd" value="{{ (isset($data)) ? $data->id : '' }}">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="alert alert-success">
                                                <p>
                                                    <button type="submit" class="btn green">Sayımı Onayla</button>
                                                    <strong>Sayım Onaylanmayı Bekliyor.</strong>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            @else
                                
                                <form class="form-horizontal" role="form" method="POST" action="{{url('stock/taking_cancel')}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="idd" value="{{ (isset($data)) ? $data->id : '' }}">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger">
                                                <p>
                                                    <button type="submit" class="btn btn-danger">İptal Et</button>
                                                    <strong>Sayımda Hata Olduğu İçin Onaylanamaz.</strong> Lütfen Hataları Kontrol Edip Yeniden Sayım Yapınız!
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            @endif
                            
                        @elseif($data->cheked == 1)

                            <div class="alert alert-success">
                                <p>
                                    <strong>Sayım Başarılı Şekilde Tamamlanmış ve Onaylanmıştır.</strong>
                                </p>
                            </div>
                        
                        @else
                        
                            <form class="form-horizontal" role="form" method="POST" action="{{url('stock/taking_cancel')}}">
                                {{ csrf_field() }}
                                <input type="hidden" name="idd" value="{{ (isset($data)) ? $data->id : '' }}">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="alert alert-danger">
                                            <p>
                                                <button type="submit" class="btn btn-danger">İptal Et</button>
                                                <strong>Sayım Yapıldıktan Sonra 6 Saat İçinde Onaylanması Gerekir.</strong>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

