@extends('layouts.app') @php use Carbon\Carbon; @endphp

@section('content')
<div class="page-content-wrapper">
    <div class="page-content"> 
        <!-- SAYFA BAŞLIĞI -->   
        <div class="page-head">
            <div class="page-title">
                <h1>Stok - Barkod
                    <small>Stok Girişi Barkod Oluşturma</small>
                </h1>
            </div>
            <div class="page-action">
                <a href="{{url('settings/barcode')}}" class="btn btn-sm btn-default"><i class="fa fa-text-width" aria-hidden="true"></i> Barkod Ölçüleri</a>
            </div>
        </div>

        <!-- !!! HER SAYFADA OLACAK !!! -->
        <!-- BREADCRUMBS START -->
        @include('partials.breadcrumbs')  
        <!-- BREADCRUMBS END --> 

        <div class="row">
            <div class="ol-lg-9">
                 <div class="portlet light portlet-fit bordered">
                    <div class="portlet-body">
                        <div class="table-scrollable table-scrollable-borderless">
                            <table class="table table-hover table-light">
                                <thead>
                                    <tr class="uppercase">
                                        <th> Barkod </th>
                                        <th> Ürün Adı </th>
                                        <th data-sortable="true"> Stok Sayısı </th>
                                        <th> Birim </th>
                                        <th> İşlem </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($parasut_products as $product)
                                        <!-- <form class="form-horizontal" role="form" method="POST" action="{{url('stock/make_first_barcode')}}"> 
                                            {{-- csrf_field() --}}
                                            <input type="hidden" name="product_id" value="{{ $product['id'] }}"> -->
    
                                            <tr class="
                                            @if ($product['status'] == 'error')
                                                {{-- 'danger' --}}
                                            @elseif ($product['status'] == 'relationship_success')
                                                {{-- 'info' --}}
                                            @elseif ($product['status'] == 'relationship_null')
                                                {{-- 'warning' --}}
                                            @endif
                                            ">
                                                <td> 
                                                    @if (empty($product['attributes']['barcode']))
                                                        <i class="fa fa-warning" aria-hidden="true"></i>
                                                    @else
                                                        {{ $product['attributes']['barcode'] }}
                                                    @endif
                                                </td>
                                                <td> {{ $product['attributes']['name'] }} </td>
                                                <td> {{ $product['attributes']['stock_count'] }} </td>
                                                <td> {{ $product['attributes']['unit'] }} </td>
                                                
                                                <td style="/*width: 200px;*/">

                                                    @if (!is_null($product['barcode_stock_count']))
                                                        {{ $product['barcode_stock_count'] }}
                                                    @endif

                                                    @if (!is_null($product['barcodes']))
                                                        @foreach ($product['barcodes'] as $barcode_view)
                                                            
                                                            {{ $barcode_view['barcode'] }} - {{ $barcode_view['group_count'] }} - {{ $barcode_view['disband'] }} <br>

                                                           
                                                        @endforeach
                                                    @endif

                                                </td>
                                            </tr>
                                            
                                        <!-- </form> -->
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var printBarcode = function(type, d){
        $('#canvas' + type + '-' + d).print({
            mediaPrint: true,
        }); 
    }
	$(document).ready(function(){
        $('.checkbox-group').change(function(){
            obj = $('#pct-' + $(this).attr('data-control'));
            if($(this).is(':checked') == true){
                obj.attr('required', true).attr('disabled', false);
            }else{
                obj.attr('required', false).attr('disabled', true);
            }
        });
	});
</script>
@endsection
