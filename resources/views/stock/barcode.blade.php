@extends('layouts.app') @php use Carbon\Carbon; @endphp

@section('content')
<div class="page-content-wrapper">
    <div class="page-content"> 
        <!-- SAYFA BAŞLIĞI -->   
        <div class="page-head">
            <div class="page-title">
                <h1>Stok - Barkod
                    <small>Stok Girişi Barkod Oluşturma</small>
                </h1>
            </div>
            <div class="page-action">
                <a href="{{url('settings/barcode')}}" class="btn btn-sm btn-default"><i class="fa fa-text-width" aria-hidden="true"></i> Barkod Ölçüleri</a>
            </div>
        </div>

        <!-- !!! HER SAYFADA OLACAK !!! -->
        <!-- BREADCRUMBS START -->
        @include('partials.breadcrumbs')  
        <!-- BREADCRUMBS END --> 
    
        <!-- İÇERİK -->
        <div class="row">
            <div class="ol-lg-9">
                 <div class="portlet light portlet-fit bordered">
                    <div class="portlet-body">
                        <div class="table-scrollable table-scrollable-borderless">
                            <table class="table table-hover table-light">
                                <thead>
                                    <tr class="uppercase">
                                        <th> Stok Giriş Tarihi </th>
                                        <th> Ürün Adı </th>
                                        <th> Barkod No </th>
                                        <th> Stok Giriş Sayısı </th>
                                        <th> Toplam Stok Sayısı </th>
                                        <th> Durum </th>
                                        <th> İşlem </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($stock_activities as $activity)
                                        @php
                                            $have = 0;
                                            $activity_id = $activity['id'];
                                            $relation = $activity['relation'];
                                            $date = $activity['date'];
                                            $datas = $activity['datas'];
                                            $products = $activity['products'];
                                            if(!is_null($activity['stock_tracking'])){
                                                $stock_tracking = $activity['stock_tracking'];
                                            }                                               
                                        @endphp
                                        @for ($k = 0; $k < count($products) ; $k++)
                                            @php
                                                $product = $products[$k];
                                                if(!is_null($activity['stock_tracking']) && !is_null($stock_tracking[$k])){
                                                    $have = 1;
                                                }
                                            @endphp
                                            @if ($have == 0)
                                            <form class="form-horizontal" role="form" method="POST" action="{{url('stock/make_barcode')}}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="activity_id" value="{{$activity_id}}">
                                                <input type="hidden" name="id" value="{{$datas[$k]['id']}}">
                                            @endif      
                                                <tr class="{{($relation[$k] == 0) ? 'warning' : ''}}">
                                                    <td> {{Carbon::parse($date)->format('d.m.Y H:m')}} </td>
                                                    <td> {{$product['attributes']['name']}} </td>
                                                    <td>  
                                                        @if (is_null($product['attributes']['barcode']))
                                                            <span class="badge badge-warning">Yok</span>
                                                        @else
                                                            {{$product['attributes']['barcode']}}
                                                        @endif
                                                    </td>
                                                    <td> {{intval($datas[$k]['attributes']['quantity'])}} </td>
                                                    <td> {{intval($product['attributes']['stock_count'])}} </td>
                                                    <td>
                                                        @if ($have == 0)
                                                            <span class="label label-sm label-default"> Hazır Değil </span>
                                                        @else
                                                            <span class="label label-sm label-success"> Hazırlandı </span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($relation[$k] == 0)
                                                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
                                                            Önce bu ürünü <a href="{{url('parasut/products')}}">ilişkilendirin</a>.
                                                        @elseif($relation[$k] == 1)
                                                            @if (is_null($product['attributes']['barcode']))
                                                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
                                                                Önce bu ürüne bir barkod numarası <a href="{{url('https://uygulama.parasut.com/201695/hizmet-ve-urunler/'.$product['id'].'/duzenle')}}" target="_blank">ekleyin</a>.
                                                            @else
                                                                @if ($have == 0) 
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <input type="checkbox" class="checkbox-group" data-control="{{$datas[$k]['id']}}" checked>
                                                                            <span></span>
                                                                        </span>
                                                                        <input type="number" id="pct-{{$datas[$k]['id']}}" class="form-control" name="pct" min="2" max="999" placeholder="Kutu mik." required />
                                                                        <span class="input-group-btn">
                                                                            <button type="submit" class="btn btn-default" title="Barkod oluştur"><i class="fa fa-barcode" aria-hidden="true"></i></button>
                                                                        </span>
                                                                    </div>
                                                                @else
                                                                    <a data-toggle="modal" href="#modal-single-{{$datas[$k]['id']}}" class="btn btn-icon-only grey" title="Ürün Barkodu"><i class="fa fa-tag" aria-hidden="true"></i></a>
                                                                    <a data-toggle="modal" href="#modal-multi-{{$datas[$k]['id']}}" class="btn btn-icon-only grey-cascade" title="Kutu Barkodları"><i class="fa fa-tags" aria-hidden="true"></i></a>
                                                                @endif 
                                                            @endif
                                                        @endif  
                                                    </td>
                                                </tr>
                                                <div class="modal fade" id="modal-single-{{$datas[$k]['id']}}" tabindex="-1" role="modal-{{$datas[$k]['id']}}" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                <h4 class="modal-title">
                                                                    Tek Ürün Barkodu | 
                                                                    {{$product['attributes']['name']}}
                                                                    <span class="badge badge-default">{{$product['attributes']['code']}}</span>
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                @if ($have == 1)
                                                                <div id="canvas1-{{$datas[$k]['id']}}" class="row">
                                                                    <div class="col-md-12">
                                                                        @foreach ($stock_tracking[$k] as $barcode_view)
                                                                            @if ($barcode_view['group_count'] == 1)
                                                                                <div class="barcode">
                                                                                    {!! $barcode_view['img'] !!}
                                                                                    <h3 class="margin-0">{{$barcode_view['barcode']}}</h3>
                                                                                </div>
                                                                                @break
                                                                            @endif
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="modal-footer">
                                                                @if ($have == 1)
                                                                    <button class="btn btn-default" onclick="printBarcode(1, {{$datas[$k]['id']}})">
                                                                        <i class="fa fa-print" aria-hidden="true"></i> Yazdır
                                                                    </button>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <div class="modal fade" id="modal-multi-{{$datas[$k]['id']}}" tabindex="-1" role="modal-{{$datas[$k]['id']}}" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                <h4 class="modal-title">
                                                                    Ürün Kutu Barkodu | 
                                                                    {{$product['attributes']['name']}}
                                                                    <span class="badge badge-default">{{$product['attributes']['code']}}</span>
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                @if ($have == 1)
                                                                <div id="canvas2-{{$datas[$k]['id']}}" class="row">
                                                                    <div class="col-md-12">
                                                                        @foreach ($stock_tracking[$k] as $barcode_view)
                                                                            @if ($barcode_view['group_count'] > 1)
                                                                                <div class="barcode">
                                                                                    {!! $barcode_view['img'] !!}
                                                                                    <h3 class="margin-0">{{$barcode_view['barcode']}}</h3>
                                                                                </div>
                                                                            @endif
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="modal-footer">
                                                                @if ($have == 1)
                                                                    <button class="btn btn-default" onclick="printBarcode(2, {{$datas[$k]['id']}})">
                                                                        <i class="fa fa-print" aria-hidden="true"></i> Yazdır
                                                                    </button>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                            @if ($have == 0)
                                            </form>
                                            @endif
                                        @endfor
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var printBarcode = function(type, d){
        $('#canvas' + type + '-' + d).print({
            mediaPrint: true,
        }); 
    }
	$(document).ready(function(){
        $('.checkbox-group').change(function(){
            obj = $('#pct-' + $(this).attr('data-control'));
            if($(this).is(':checked') == true){
                obj.attr('required', true).attr('disabled', false);
            }else{
                obj.attr('required', false).attr('disabled', true);
            }
        });
	});
</script>
@endsection
