@extends('layouts.webshell') 
@php use Carbon\Carbon; @endphp

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü 
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            Kafe Barkod İşlemleri
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Kafe Barkod İşlemleri
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <!-- <a href="{{url('settings/barcode')}}" class="btn btn-sm btn-default"><i class="fa fa-text-width" aria-hidden="true"></i> Barkod Ölçüleri</a> -->
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="m-section__content">
                    <table class="table m-table">
                        
                        <thead>
                            <tr>
                                <th> P. ID </th>
                                <th> Ürün Adı </th>
                                <th> Barkod No</th>
                                <th> Satış Fiyatı </th>
                                <th> Barcode <br> Görüntüle </th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($kafe_product as $product)
                                <form class="form-horizontal" role="form" method="POST" action="{{url('stock/make_barcodes')}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">

                                    <tr>
                                        <td> {{ $product->id }} </td>
                                        <td> {{ $product->name }} </td>
                                        <td> 
                                            @if (empty($product->barcode))
                                                <i class="fa fa-warning" aria-hidden="true"></i>
                                            @else
                                                {{ $product->barcode }}
                                            @endif
                                        </td>
                                        <td> {{ number_format($product->price,0,',','') }} ₺ </td>
                                        <td style="width: 100px;">
                                            <!--
                                                <a data-toggle="modal" href="#modal-single-{{ $product->id }}" class="btn btn-primary btn-sm m-btn  m-btn m-btn--icon m-btn--pill" title="Ürün Barkodu">
                                                    <span>
                                                        <i class="fa fa-barcode"></i>
                                                        <span>Barcode Göster</span>
                                                    </span>
                                                </a>
                                            -->

                                            {!! '<img src="data:image/png;base64,'.DNS1D::getBarcodePNG($product->barcode, "C128", 3.6, 100).'" alt="barcode" />' !!}
    
                                        </td>
                                    </tr>
                                    
                                    <!--
                                        <div class="modal fade" id="modal-single-{{ $product->id }}" tabindex="-1" role="modal-{{ $product->id }}" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">
                                                            {{ $product->name }}
                                                        </h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div id="canvas1-{{ $product->id }}" class="row">
                                                            <div class="col-md-12">
                                                                {{-- '<img src="data:image/png;base64,'.DNS1D::getBarcodePNG($product->barcode, "C128", 3.6, 50).'" alt="barcode" />' --}}
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-default" onclick="printBarcode(1, {{ $product->id }})">
                                                            <i class="fa fa-print" aria-hidden="true"></i> Yazdır
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    -->
                                    
                                </form>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection
