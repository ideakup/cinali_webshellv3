@extends('layouts.webshell')

@section('content')

	<div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü 
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('sitesettings') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Site Ayarlar
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Site Ayarları
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
				</div>
			</div>

			<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('sitesettings_save') }}" id="settingsForm">
                {{ csrf_field() }}
                <input type="hidden" name="crud" value="{{ Request::segment(2) }}">

                <div class="m-portlet__body">
                    @foreach ($sitesettings as $setting)
                        <div class="form-group m-form__group row @if ($errors->has($setting->slug)) has-danger @endif">
                            <label for="{{ $setting->slug }}" class="col-2 col-form-label">
                                {{ $setting->name }}
                            </label>
                            <div class="col-7">
                                <input class="form-control m-input" type="text" id="{{ $setting->slug }}" name="{{ $setting->slug }}" 
                                    @if (empty(old($setting->slug))) 
                                        value="{{ $setting->value }}"
                                    @else 
                                        value="{{ old($setting->slug) }}" 
                                    @endif>
                                @if ($errors->has($setting->slug))
                                    <div id="{{ $setting->slug }}-error" class="form-control-feedback">{{ $errors->first($setting->slug) }}</div>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                    Kaydet
                                </button>
                                &nbsp;&nbsp;
                                <a href="{{ url('menu/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                    Vazgeç
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            
		</div>
	</div>
    
@endsection

@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#postBtn').click(function(e) {
                e.preventDefault();
                var btn = $(this);
                var form = $(this).closest('form');

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                form.submit();
            });
        });
    </script>
@endsection
