@extends('layouts.webshell')

@section('content')

	<div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu/list') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Menü Listesi
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                İçerik Listesi ({{ $menu->variableLang($activeLang->first()->code)->name }})
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            @if (Request::segment(4) == 'add')
                                İçerik Ekle
                            @elseif (Request::segment(4) == 'edit')
                                @if (is_null(Request::segment(6)))
                                    İçerik Detayları
                                @else
                                    İçerik Düzenle
                                @endif
                            @elseif (Request::segment(4) == 'delete')
                                İçerik Sil
                            @elseif (Request::segment(4) == 'galup')
                                Fotoğraf Yükle
                            @elseif (Request::segment(4) == 'addexist')
                                Mevcut İçerik Ekle
                            @endif
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">

                <div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							@if (Request::segment(4) == 'add')
                                İçerik Ekle
                            @elseif (Request::segment(4) == 'edit')
                                @if (is_null(Request::segment(6)))
                                    İçerik Detayları
                                @else
                                    İçerik Düzenle
                                @endif
                            @elseif (Request::segment(4) == 'delete')
                                İçerik Sil ({{ $menu->variableLang($activeLang->first()->code)->name }})
                            @elseif (Request::segment(4) == 'addexist')
                                Mevcut İçerik Ekle ({{ $menu->variableLang($activeLang->first()->code)->name }})
                            @endif
						</h3>
					</div>
				</div>
                
				<div class="m-portlet__head-tools">
                    @if (Request::segment(4) == 'edit')
                        <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}/delete/{{Request::segment(5)}}" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" title="İçerik Sil">
                            <i class="fa fa-trash"></i>
                        </a>
                    @endif
				</div>

			</div>

			<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('menu/content/saveexist') }}" id="existContentForm">
                {{ csrf_field() }}
                <input type="hidden" name="crud" value="{{ Request::segment(4) }}">
                <input type="hidden" name="menu_id" value="{{ Request::segment(3) }}">
                <input type="hidden" name="content_id" value="{{ Request::segment(5) }}">
                <input type="hidden" name="lang" value="{{ Request::segment(6) }}">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="m-portlet__body">
                    
                    <div class="form-group m-form__group row">
                        <div class="col-10 ml-auto">
                            <h3 class="m-form__section">
                                Mevcut İçerik Seç 
                                (
                                @if ($menu->type == 'photogallery')
                                    Foto Galeri
                                @endif
                                )
                            </h3>
                        </div>
                    </div>

                    <div class="form-group m-form__group row @if ($errors->has('existcontent')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            İçerik
                        </label>
                        <div class="col-7">
                            @if ($errors->has('existcontent'))
                                <div id="existcontent-error" class="form-control-feedback">{{ $errors->first('existcontent') }}</div>
                            @endif
                            <select class="form-control m-select2" id="existcontent" name="existcontent"
                                @if (Request::segment(4) == 'delete')
                                    disabled="disabled" 
                                @endif
                            >
                                <option value="null">Seçiniz...</option>

                                @php
                                    $typeText = '';
                                    $typeSlug = '';
                                @endphp

                                @foreach ($content as $con)

                                    @if ($con->type == 'text')
                                        @php
                                            $typeText = 'Metin & HTML';
                                        @endphp
                                    @elseif ($con->type == 'photo')
                                        @php
                                            $typeText = 'Fotoğraf';
                                        @endphp
                                    @elseif ($con->type == 'photogallery')
                                        @php
                                            $typeText = 'Foto Galeri';
                                        @endphp
                                    @elseif ($con->type == 'link')
                                        @php
                                            $typeText = 'Button & Link';
                                        @endphp
                                    @elseif ($con->type == 'form')
                                        @php
                                            $typeText = 'Form';
                                        @endphp
                                    @endif

                                    @if ($typeSlug != $con->type)
                                        @if (!$loop->first)
                                            </optgroup>
                                        @endif
                                        <optgroup label="{{ $typeText }}">
                                        @php $typeSlug = $con->type; @endphp
                                    @endif
                                    
                                    <option value="{{ $con->id }}"> {{ $con->variableLang($activeLang->first()->code)->title }} </option>
                                    
                                    @if ($loop->last)
                                        </optgroup>
                                    @endif
                                @endforeach

                            </select>
                        </div>
                    </div>

                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                @if (Request::segment(4) == 'addexist')
                                    <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Ekle
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </form>
		</div>
	</div>
@endsection

@section('inline-scripts')
<script type="text/javascript">

    $(document).ready(function(){

        $('#existcontent').select2({
            placeholder: "Seçiniz..."
        });

        $('#postBtn').click(function(e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            /*
                form.validate({
                    rules: {
                        existcontent: {
                            required: true
                        }
                    }
                });

                if (!form.valid()) {
                    return;
                }
            */
            
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            form.submit();
            
        });
    });
</script>
@endsection
