@extends('layouts.webshell')

@section('content')

	<div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu/list') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Menü Listesi
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                İçerik Listesi ({{ $menu->variableLang($langs->first()->code)->name }})
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Alt İçerikler ({{ $content->variableLang($langs->first()->code)->title }})
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            @if (Request::segment(6) == 'add')
                                Alt İçerik Ekle
                            @elseif (Request::segment(6) == 'edit')
                                @if (is_null(Request::segment(8)))
                                    Alt İçerik Detayları
                                @else
                                    Alt İçerik Düzenle
                                @endif
                            @elseif (Request::segment(6) == 'delete')
                                Alt İçerik Sil
                            @elseif (Request::segment(6) == 'galup')
                                Fotoğraf Yükle
                            @endif
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			
            <div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
                            @if (Request::segment(6) == 'add')
                                Alt İçerik Ekle
                            @elseif (Request::segment(6) == 'edit')
                                @if (is_null(Request::segment(8)))
                                    Alt İçerik Detayları
                                @else
                                    Alt İçerik Düzenle
                                @endif
                            @elseif (Request::segment(6) == 'delete')
                                Alt İçerik Sil ({{ $menu->variableLang($langs->first()->code)->name }})
                            @elseif (Request::segment(6) == 'galup')
                                Fotoğraf Yükle
                            @endif
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
                    @if (Request::segment(6) == 'edit' && is_null(Request::segment(8)))
                        <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}/addons/{{Request::segment(5)}}/delete/{{Request::segment(7)}}" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" title="Alt İçerik Sil">
                            <i class="fa fa-trash"></i>
                        </a>
                    @endif
				</div>
            </div>

			<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('menu/content/'.Request::segment(3).'/addons/'.Request::segment(5).'/save') }}" id="addonForm">
                {{ csrf_field() }}
                <input type="hidden" name="crud" value="{{ Request::segment(6) }}">
                <input type="hidden" name="menu_id" value="{{ Request::segment(3) }}">
                <input type="hidden" name="content_id" value="{{ Request::segment(5) }}">
                <input type="hidden" name="addon_id" value="{{ Request::segment(7) }}">
                <input type="hidden" name="lang" value="{{ Request::segment(8) }}">
                <input type="hidden" id="formdata" name="formdata">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                <div class="m-portlet__body">

                    <div class="form-group m-form__group row">
                        <div class="col-10 ml-auto">
                            <h3 class="m-form__section">
                                @if (!is_null(Request::segment(8))) 
                                    [{{ Request::segment(8) }}] 
                                @endif 
                                
                                @if (Request::segment(6) == 'add')
                                    Alt İçerik Ekle
                                @elseif (Request::segment(6) == 'edit')
                                    @if (is_null(Request::segment(8)))
                                        Alt İçerik Detayları
                                    @else
                                        Alt İçerik Düzenle
                                    @endif
                                @elseif (Request::segment(6) == 'delete')
                                    Alt İçerik Sil
                                @elseif (Request::segment(6) == 'galup')
                                    Fotoğraf Yükle
                                @endif

                                @if (Request::segment(6) != 'add')
                                    <small> (
                                        @if ($addon->type == 'group')
                                            İçerik Grubu
                                        @elseif ($addon->type == 'text')
                                            Metin & HTML
                                        @elseif ($addon->type == 'photo')
                                            Fotoğraf
                                        @elseif ($addon->type == 'photogallery')
                                            Foto Galeri
                                        @elseif ($addon->type == 'link')
                                            Button & Link
                                        @elseif ($addon->type == 'slide')
                                            Slide
                                        @elseif ($addon->type == 'form')
                                            Form
                                        @endif
                                    ) </small>
                                @endif
                            </h3>
                        </div>
                    </div>

                    <div class="form-group m-form__group row @if ($errors->has('title')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Başlık
                        </label>
                        <div class="col-7">
                            <input class="form-control m-input" type="text" id="title" name="title" 
                                @if (Request::segment(6) != 'add' && empty(old('title'))) 
                                    value="@if(is_null($addon->variableLang(Request::segment(8)))){{ $addon->variableLang($langs->first()->code)->title }}@else{{ $addon->variableLang(Request::segment(8))->title }}@endif"
                                @else
                                    value="{{ old('title') }}" 
                                @endif

                                @if (Request::segment(6) == 'delete' || (Request::segment(6) != 'add' && is_null($addon->variableLang(Request::segment(8)))))
                                    disabled="disabled" 
                                @endif
                            required autofocus>
                            @if ($errors->has('title'))
                                <div id="title-error" class="form-control-feedback">{{ $errors->first('title') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group m-form__group row @if ($errors->has('slug')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Slug
                        </label>
                        <div class="col-7">
                            <input class="form-control m-input" type="text" id="slug" name="slug" 
                                @if (Request::segment(6) != 'add' && empty(old('slug'))) 
                                    value="@if(is_null($addon->variableLang(Request::segment(8)))){{ $addon->variableLang($langs->first()->code)->slug }}@else{{ $addon->variableLang(Request::segment(8))->slug }}@endif"
                                @else 
                                    value="{{ old('slug') }}" 
                                @endif

                                @if (Request::segment(6) == 'delete' ||  is_null(Request::segment(8)))
                                    disabled="disabled" 
                                @endif
                            required>
                            @if ($errors->has('slug'))
                                <div id="slug-error" class="form-control-feedback">{{ $errors->first('slug') }}</div>
                            @endif
                        </div>
                    </div>
                    
                    @if (Request::segment(6) == 'add')
                        <div class="form-group m-form__group row @if ($errors->has('type')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Tip
                            </label>
                            <div class="col-7">
                                @if ($errors->has('type'))
                                    <div id="type-error" class="form-control-feedback">{{ $errors->first('type') }}</div>
                                @endif
                                <select class="form-control m-select2" id="type" name="type"
                                    @if (Request::segment(6) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                >
                                    <option value="null">Seçiniz...</option>

                                    <option value="text"> Metin & HTML </option>
                                    <option value="photo"> Fotoğraf </option>
                                    <option value="photogallery"> Foto Galeri </option>
                                    <option value="link"> Button & Link </option>
                                    
                                    <!-- <option value="slide"> Slide </option> -->
                                    <!-- <option value="form" @if(is_null($menu) && Request::segment(3) == 0) selected @endif > Form </option> -->
                                    <!--
                                        <option value="youtube"> Video (Youtube) </option>
                                        <option value="vimeo"> Video (Vimeo) </option>
                                        <option value="file"> Dosya </option>
                                        <option value="audio"> Ses </option>
                                        <option value="code"> Kod </option>
                                    -->
                                </select>
                            </div>
                        </div>
                    @endif

                    @if (Request::segment(6) == 'edit' && is_null(Request::segment(8)) && $addon->type != 'photo')
                        
                        <div class="form-group m-form__group row @if ($errors->has('tag')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Etiket (Ne)
                            </label>
                            <div class="col-7">
                                @if ($errors->has('tag'))
                                    <div id="tag-error" class="form-control-feedback">{{ $errors->first('tag') }}</div>
                                @endif
                                <select class="form-control m-select2" id="tag" name="tag[]" multiple="multiple"
                                    @if (Request::segment(6) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                >
                                    <option value="null">Seçiniz...</option>

                                    @foreach ($tags as $tag)
                                        <option value="{{ $tag->id }}" @if ($addon->tags()->where('tag_id', $tag->id)->count()) {{ 'selected' }} @endif> {{ $tag->variableLang($langs->first()->code)->title }} </option>
                                    @endforeach
                                    
                                </select>
                            </div>
                        </div>

                        <div class="form-group m-form__group row @if ($errors->has('category')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Kategori (Nerede)
                            </label>
                            <div class="col-7">
                                @if ($errors->has('category'))
                                    <div id="category-error" class="form-control-feedback">{{ $errors->first('category') }}</div>
                                @endif
                                <select class="form-control m-select2" id="category" name="category[]" multiple="multiple"
                                    @if (Request::segment(6) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                >
                                    <option value="null">Seçiniz...</option>

                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" @if ($addon->categories()->where('category_id', $category->id)->count()) {{ 'selected' }} @endif> {{ $category->variableLang($langs->first()->code)->title }} </option>
                                    @endforeach
                                    
                                </select>
                            </div>
                        </div>

                    @endif

                    @if (Request::segment(6) == 'add' || (Request::segment(6) == 'edit' && is_null(Request::segment(8))))
                        
                        <div class="form-group m-form__group row @if ($errors->has('order')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Sıralama
                            </label>
                            <div class="col-7">
                                <input class="form-control m-input" type="number" min="1" max="100000" id="order" name="order" 
                                    @if (Request::segment(6) != 'add' && empty(old('order'))) 
                                        value="{{ $addon->order }}"
                                    @else 
                                        value="{{ old('order') }}" 
                                    @endif

                                    @if (Request::segment(6) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                required autofocus>
                                @if ($errors->has('order'))
                                    <div id="order-error" class="form-control-feedback">{{ $errors->first('order') }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="m-form__group form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Pasif / Aktif
                            </label>
                            <div class="col-3">
                                <span class="m-switch">
                                    <label>
                                        <input type="checkbox" @if ($addon->status == 'active' || Request::segment(6) == 'add') {{ 'checked="checked"' }} @endif  id="status" name="status" value="active" 
                                            @if (Request::segment(6) == 'delete')
                                                disabled="disabled" 
                                            @endif
                                        />
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>

                    @endif
                    
                    @if (Request::segment(6) != 'add')

                        @if ($addon->type == 'photogallery' && (Request::segment(6) == 'edit' && is_null(Request::segment(8))))
                            
                            <hr>
                            <div class="form-group m-form__group row @if ($errors->has('props')) has-danger @endif">
                                <label for="example-text-input" class="col-2 col-form-label">
                                    Kolon Sayısı
                                </label>
                                <div class="col-7">
                                    @if ($errors->has('props'))
                                        <div id="props-error" class="form-control-feedback">{{ $errors->first('props') }}</div>
                                    @endif
                                    <select class="form-control m-select2" id="props" name="props"
                                        @if (Request::segment(6) == 'delete')
                                            disabled="disabled" 
                                        @endif
                                    >
                                        <option value="col-2x" @if (!empty($addon) && $addon->variableLang($langs->first()->code)->props == 'col-2x') {{ 'selected' }} @endif> 2 Kolon </option>
                                        <option value="col-3x" @if (!empty($addon) && $addon->variableLang($langs->first()->code)->props == 'col-3x') {{ 'selected' }} @endif> 3 Kolon </option>
                                        <option value="col-4x" @if (!empty($addon) && $addon->variableLang($langs->first()->code)->props == 'col-4x') {{ 'selected' }} @endif> 4 Kolon </option>
                                    </select>
                                </div>
                            </div>

                        @endif

                    @endif

                    @if (Request::segment(6) == 'edit' && !is_null(Request::segment(8)))
                        
                        @if ($addon->type == 'photo')

                            <hr>
                            <div class="form-group m-form__group row">
                                <div class="col-6">
                                    <div class="m-dropzone dropzone" action="{{ url('/uploadFile') }}" id="dropzone-file-upload">
                                        <div class="m-dropzone__msg dz-message needsclick">
                                            <h3 class="m-dropzone__msg-title">
                                                Dosya yüklemek için dosyayı buraya sürükleyin yada bu alan tıklayın.
                                            </h3>
                                            <span class="m-dropzone__msg-desc">
                                                Bu alan <strong>jpg</strong> ve <strong>png</strong> formatlarını yüklemenize izin verir.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="col-12" id="imgContainer">
                                        @if (empty($addon->variableLang(Request::segment(8))->content))
                                            <div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show" role="alert">
                                                <strong>Uyarı!</strong> Yüklenmiş Görsel Bulunmuyor...
                                            </div>
                                        @else
                                            <img class="img-fluid" src="{{ url('/upload/xlarge/'.$addon->variableLang(Request::segment(8))->content) }}" />
                                        @endif
                                    </div>
                                </div>
                                
                            </div>

                        @endif
                        
                    @endif

                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                @if (Request::segment(6) == 'add' || Request::segment(6) == 'edit')
                                    <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydet
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}/addons/{{ Request::segment(5) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                @elseif (Request::segment(6) == 'delete')
                                    <div class="alert alert-danger" role="alert">
                                        <strong> Siliyorsunuz... </strong>
                                        Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.
                                    </div>
                                    <button class="btn btn-danger m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydı Sil
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}/addons/{{ Request::segment(5) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </form>
		</div>
    
        @if (Request::segment(6) == 'galup' && $addon->type == 'photogallery')
            <div class="m-portlet m-portlet--mobile">

                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Fotoğraf Yükle
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__body">
                    <div class="form-group m-form__group row" id="imgContainer">
                        <div class="col-12">
                            <div class="m-dropzone dropzone" action="{{ url('/uploadFile') }}" id="dropzone-file-upload">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 class="m-dropzone__msg-title">
                                        Dosya yüklemek için dosyayı buraya sürükleyin yada bu alan tıklayın.
                                    </h3>
                                    <span class="m-dropzone__msg-desc">
                                        Bu alan <strong>jpg</strong> ve <strong>png</strong> formatlarını yüklemenize izin verir.
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="col-12">
                                @if ($addon->photogallery->count() == 0)
                                    <div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show" role="alert">
                                        <strong>Uyarı!</strong> Yüklenmiş Görsel Bulunmuyor...
                                    </div>
                                @endif
                                <p> </p>
                            </div>
                        </div>

                        @foreach ($addon->photogallery as $img)
                            <div class="col-xl-4">
                                <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height" style="margin-bottom: 0;">
                                    <div class="m-portlet__head m-portlet__head--fit">
                                        <div class="m-portlet__head-tools">
                                            <ul class="m-portlet__nav">
                                                <li class="m-portlet__nav-item" aria-expanded="true">
                                                    <a href="{{ url('menu/content/'.Request::segment(3).'/addons/'.Request::segment(5).'/edit/'.Request::segment(7).'/photogallery_edit/'.$img->id) }}" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        <div class="m-widget19">
                                            <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">
                                                <img class="img-fluid" src="{{ url('upload/thumbnail3x/'.$img->url) }}" />
                                                <p class="m-widget19__title m--font-light" style="padding-bottom: 0;">
                                                    {{ $img->name }}
                                                </p>
                                                <div class="m-widget19__shadow" style="background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 1) 60%);"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>

            </div>
        @endif

	</div>
@endsection

@section('inline-scripts')
    <script type="text/javascript">
        
        @if (Request::segment(6) != 'add')

            @if ($addon->type == 'photo')
                Dropzone.options.dropzoneFileUpload = {
                    headers: { 'X-CSRF-TOKEN': $('#token').val() },
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 1,
                    maxFilesize: 5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        formData.append('uploadType', 'contentimage');
                        formData.append('contentid', {{ $addon->id }});
                        formData.append('lang_code', '{{ Request::segment(8) }}');
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgContainer").html('<img class="img-fluid" src="{{ url('') }}/upload/xlarge/' + data.location + '" />');
                        this.removeFile(file);
                    }
                };
            @endif

            @if ($addon->type == 'photogallery')
                
                Dropzone.options.dropzoneFileUpload = {
                    headers: { 'X-CSRF-TOKEN': $('#token').val() },
                    paramName: "file", // The name that will be used to transfer the file
                    maxFiles: 5,
                    maxFilesize: 5, // MB
                    addRemoveLinks: true,
                    accept: function(file, done) {
                        done();
                    },
                    init: function () {
                        //console.log('dropzone init');
                    },
                    sending: function(file, xhr, formData){
                        formData.append('uploadType', 'galleryimage');
                        formData.append('contentid', {{ $addon->id }});
                    },
                    success: function(file, xhr, event){
                        var data = jQuery.parseJSON(xhr);
                        $("#imgContainer").append('<div class="col-xl-4">'+
                        '    <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height" style="margin-bottom: 0;">'+
                        '        <div class="m-portlet__head m-portlet__head--fit">'+
                        '            <div class="m-portlet__head-tools">'+
                        '                <ul class="m-portlet__nav">'+
                        '                    <li class="m-portlet__nav-item" aria-expanded="true">'+
                        '                        <a href="{{ url('menu/content/'.Request::segment(3).'/edit/'.Request::segment(5).'/photogallery_edit/') }}/' + data.id + '" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill">'+
                        '                           <i class="fa fa-cog"></i>'+
                        '                        </a>'+
                        '                    </li>'+
                        '                </ul>'+
                        '            </div>'+
                        '        </div>'+
                        '        <div class="m-portlet__body">'+
                        '            <div class="m-widget19">'+
                        '                <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">'+
                        '                    <img class="img-fluid" src="{{ url('upload/thumbnail3x/') }}/' + data.location + '" />'+
                        '                    <p class="m-widget19__title m--font-light" style="padding-bottom: 0;">'+
                        '                    </p>'+
                        '                    <div class="m-widget19__shadow" style="background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 1) 60%);"></div>'+
                        '               </div>'+
                        '           </div>'+
                        '        </div>'+
                        '    </div>'+
                        '</div>');
                        this.removeFile(file);
                    }
                };
            @endif

        @endif

        $(document).ready(function(){

            $('#type').select2({
                placeholder: "Seçiniz..."
            });
            $('#row').select2({
                placeholder: "Seçiniz..."
            });
            $('#tag').select2({
                placeholder: "Seçiniz..."
            });
            $('#category').select2({
                placeholder: "Seçiniz..."
            });
            @if (Request::segment(6) != 'add')
                @if ($addon->type == 'photo')
                    $('#props').select2({
                        placeholder: "Seçiniz..."
                    });
                @endif

                @if ($addon->type == 'photogallery')
                    $('#props').select2({
                        placeholder: "Seçiniz..."
                    });
                @endif
            @endif

            $('#postBtn').click(function(e) {

                e.preventDefault();
                var btn = $(this);
                var form = $(this).closest('form');

                form.validate({
                    rules: {
                    @if (Request::segment(6) == 'delete' || (Request::segment(6) != 'add' && is_null($addon->variableLang(Request::segment(8)))))                    
                        title: {
                            required: true
                        },
                    @endif
                    @if (Request::segment(6) == 'add')
                        type: {
                            required: true
                        },
                    @endif
                    order: {
                        required: true
                    }
                    }
                });

                if (!form.valid()) {
                    return;
                }
                
                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                form.submit();
            });
        });
    </script>
@endsection
