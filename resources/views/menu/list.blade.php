@extends('layouts.webshell')

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            Menü Listesi
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Menü Listesi
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ url('menu/add') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="fa fa-plus"></i>
                                    <span>
                                        Yeni Menü Ekle
                                    </span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="menuListDataTable">
                    <thead>
                        <tr>
                            <th>
                                ID
                            </th>
                            <th width="30">
                                Sıra
                            </th>
                            <th width="50">
                                Durum
                            </th>
                            <th>
                                Adı
                            </th>
                            <th>
                                Slug
                            </th>
                            <th>
                                Üst Menü
                            </th>
                            <th>
                                Tip
                            </th>
                            <th>
                                İşlemler
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('inline-scripts')
    <script type="text/javascript">
        
        $(document).ready(function(){

            var table = $('#menuListDataTable').DataTable({
                responsive: true,
                dom: `<'row'<'col-sm-12'tr>> <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 50,
                order: [[1, 'asc']],
                language: {
                    "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                    "info": "Sayfa: _PAGE_/_PAGES_ ",
                    "infoEmpty": "Kayıt Yok.",
                    "zeroRecords": "Kayıt Yok.",
                    "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                    "processing": "Yükleniyor...",
                },
                searching: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "/getMenu", // ajax source
                    type: "POST",
                    headers: { 'X-CSRF-TOKEN': $('#token').val() },
                    data: {
                        // parameters for custom backend script demo
                        columnsDef: ['id', 'order', 'name', 'slug', 'top_id', 'type', 'setting', 'status', 'actions'],
                    },
                },
                columns: [
                    {name: 'id'},
                    {name: 'order'},
                    {name: 'status'},
                    {name: 'name'},
                    {name: 'slug'},
                    {name: 'top_id'},
                    {name: 'type'},
                    {name: 'actions'},
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        visible: false,
                    },
                    {
                        targets: 2,
                        render: function(data, type, full, meta) {
                            var status = {
                                'active': {'title': 'Aktif', 'class': 'm-badge--brand'},
                                'passive': {'title': 'Pasif', 'class': ' m-badge--metal'}
                            };
                            if (typeof status[data] === 'undefined') {
                                return data;
                            }
                            return '<span class="m-badge ' + status[data].class + ' m-badge--wide">' + status[data].title + '</span>';
                        },
                    },
                    {
                        targets: 6,
                        render: function(data, type, full, meta) {
                            var status = {
                                'menuitem': {'title': 'Menü Öğesi', 'class': 'm-badge--accent'},
                                'content': {'title': 'İçerik', 'class': 'm-badge--brand'},
                                'list': {'title': 'Liste', 'class': ' m-badge--success'},
                                'link': {'title': 'Link (Yönlendirme)', 'class': ' m-badge--info'},
                                'photogallery': {'title': 'Foto Galeri', 'class': ' m-badge--warning'},
                                'calendar': {'title': 'Takvim', 'class': ' m-badge--danger'}
                            };
                            if (typeof status[data] === 'undefined') {
                                return data;
                            }
                            return '<span class="m-badge ' + status[data].class + ' m-badge--wide">' + status[data].title + '</span>';
                        },
                    },
                    {
                        targets: -1,
                        title: 'İşlemler',
                        orderable: false,
                        render: function(data, type, full, meta) {
                            var slidertypebtn = {'icon':'', 'url':'', 'class':''};
                            if(data['slidertype'] == 'slider'){
                                slidertypebtn = {'icon':'images', 'url':'{{ url('menu/stslider') }}/'+full[0], 'class':'m-btn--hover-brand'};
                            }else if(data['slidertype'] == 'image'){
                                slidertypebtn = {'icon':'image', 'url':'{{ url('menu/stimage') }}/'+full[0], 'class':'m-btn--hover-brand'};
                            }else if(data['slidertype'] == 'no'){
                                slidertypebtn = {'icon':'image', 'url':'#', 'class':'m-btn--hover-metal'};
                            }

                            var editButtons = '';
                            @foreach ($langs as $lang)
                                editButtons += `<a href="{{ url('menu/edit') }}/`+full[0]+`/{{ $lang->code }}" class="m-portlet__nav-link btn btn-sm m-btn btn-outline-brand m-btn--pill" title="Düzenle {{ $lang->code }}">
                                    {{ $lang->code }}
                                </a> `;
                            @endforeach

                            var menuu = ``;
                            if(full[6] != 'menuitem' && full[6] != 'link'){
                                menuu = `<a href="{{ url('menu/content') }}/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="İçerikler">
                                  <i class="fa fa-indent"></i>
                                </a>
                                <a href="`+slidertypebtn['url']+`" class="m-portlet__nav-link btn m-btn `+slidertypebtn['class']+` m-btn--icon m-btn--icon-only m-btn--pill" title="Sayfa Görseli">
                                  <i class="fa fa-`+slidertypebtn['icon']+`" `+ ((slidertypebtn['url'] == '#') ? 'style="color:#D6D6D6;"' : '') +`></i>
                                </a>`;
                            }

                            return editButtons+`
                            <a href="{{ url('menu/edit') }}/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Ayarlar">
                              <i class="fa fa-cogs"></i>
                            </a>`+menuu;

                        },
                    },
                ],
            });

        });
    </script>
@endsection
