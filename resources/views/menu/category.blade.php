@extends('layouts.webshell')

@section('content')
    
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Kategori (Nerede)
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            Kategori (Nerede) Listesi
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Kategori (Nerede) Listesi
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ url('category') }}/add" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="fa fa-plus"></i>
                                    <span>
                                        Yeni Kategori (Nerede) Ekle
                                    </span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="categoryDataTable">
                    <thead>
                        <tr>
                            <th>
                                ID
                            </th>
                            <th width="30">
                                Sıra
                            </th>
                            <th>
                                Adı
                            </th>
                            <th width="50">
                                Durum
                            </th>
                            <th>
                                İşlemler
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('inline-scripts')
<script type="text/javascript">
    
    $(document).ready(function(){

        var table = $('#categoryDataTable').DataTable({
            responsive: true,
            dom: `<'row'<'col-sm-12'tr>> <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[1, 'asc']],
            language: {
                "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                "info": "Sayfa: _PAGE_/_PAGES_ ",
                "infoEmpty": "Kayıt Yok.",
                "zeroRecords": "Kayıt Yok.",
                "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                "processing": "Yükleniyor...",
            },
            searching: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "/getCategory", // ajax source
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('#token').val(), 'MENU-ID': $('#_menuid').val() },
                data: {
                    // parameters for custom backend script demo
                    menuId: $('#_menuid').val(),
                    columnsDef: ['id', 'order', 'title', 'status', 'actions'],
                },
            },
            columns: [
                {name: 'id'},
                {name: 'order'},
                {name: 'title'},
                {name: 'status'},
                {name: 'actions'},
            ],
            columnDefs: [
                {
                    targets: 0,
                    orderable: false,
                    visible: false,
                },
                {
                    targets: -2,
                    render: function(data, type, full, meta) {
                        var status = {
                            'active': {'title': 'Aktif', 'class': 'm-badge--brand'},
                            'passive': {'title': 'Pasif', 'class': ' m-badge--metal'}
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="m-badge ' + status[data].class + ' m-badge--wide">' + status[data].title + '</span>';
                    },
                },
                {
                    targets: -1,
                    title: 'İşlemler',
                    orderable: false,
                    render: function(data, type, full, meta) {

                        var editButtons = '';
                        @foreach ($langs as $lang)
                            editButtons += `<a href="{{ url('category/edit') }}/`+full[0]+`/{{ $lang->code }}" class="m-portlet__nav-link btn btn-sm m-btn btn-outline-brand m-btn--pill" title="Düzenle {{ $lang->code }}">
                                {{ $lang->code }}
                            </a> `;
                        @endforeach

                        return editButtons+`
                        <a href="{{ url('category/edit') }}/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Ayarlar">
                          <i class="fa fa-cogs"></i>
                        </a>`;
                    },
                }
            ],
        });

    });
</script>
@endsection
