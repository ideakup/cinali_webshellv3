@extends('layouts.webshell')

@section('content')

	<div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Kategori (Nerede)
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('category') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Kategori (Nerede) Listesi
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            @if (Request::segment(2) == 'add')
                                Kategori (Nerede) Ekle
                            @elseif (Request::segment(2) == 'edit')
                                @if (is_null(Request::segment(4)))
                                    Kategori (Nerede) Ayarları
                                @else
                                    Kategori (Nerede) Düzenle
                                @endif
                            @elseif (Request::segment(2) == 'delete')
                                Kategori (Nerede) Sil
                            @endif
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							@if (Request::segment(2) == 'add')
                                Kategori (Nerede) Ekle
                            @elseif (Request::segment(2) == 'edit')
                                @if (is_null(Request::segment(4)))
                                    Kategori (Nerede) Ayarları
                                @else
                                    Kategori (Nerede) Düzenle
                                @endif
                            @elseif (Request::segment(2) == 'delete')
                                Kategori (Nerede) Sil
                            @endif
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
                    @if (Request::segment(2) == 'edit' && is_null(Request::segment(4)))
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="{{ url('category/delete') }}/{{ $category->id }}" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" title="Kategori'yü Sil">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </li>
                        </ul>
                    @endif
				</div>
			</div>

			<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('category/save') }}" id="categoryForm">
                {{ csrf_field() }}
                <input type="hidden" name="crud" value="{{ Request::segment(2) }}">
                <input type="hidden" name="category_id" value="{{ Request::segment(3) }}">
                <input type="hidden" name="lang" value="{{ Request::segment(4) }}">
                <div class="m-portlet__body">

                    <div class="form-group m-form__group row">
                        <div class="col-10 ml-auto">
                            <h3 class="m-form__section">
                                @if (Request::segment(2) == 'add')
                                    Kategori (Nerede) Ekle
                                @elseif (Request::segment(2) == 'edit')
                                    @if (is_null(Request::segment(4)))
                                        Kategori (Nerede) Ayarları
                                    @else
                                        [{{ Request::segment(4) }}] Kategori (Nerede) Düzenle 
                                    @endif
                                @elseif (Request::segment(2) == 'delete')
                                    Kategori (Nerede) Sil
                                @endif

                            </h3>
                        </div>
                    </div>


                    <div class="form-group m-form__group row @if ($errors->has('title')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Başlık
                        </label>
                        <div class="col-7">
                            <input class="form-control m-input" type="text" id="title" name="title" 
                                @if (Request::segment(2) != 'add' && empty(old('title'))) 
                                    value="@if (is_null($category->variableLang(Request::segment(4)))){{ $category->variableLang($langs->first()->code)->title }}@else{{ $category->variableLang(Request::segment(4))->title }}@endif"
                                @else 
                                    value="{{ old('title') }}" 
                                @endif

                                @if (Request::segment(2) == 'delete' ||  (Request::segment(2) == 'edit' && is_null(Request::segment(4))))
                                    disabled="disabled" 
                                @endif
                            required>
                            @if ($errors->has('title'))
                                <div id="title-error" class="form-control-feedback">{{ $errors->first('title') }}</div>
                            @endif
                        </div>
                    </div>
                    
                    @if ( (Request::segment(2) == 'edit' && is_null(Request::segment(4))) || Request::segment(2) == 'add' )
                        <div class="form-group m-form__group row @if ($errors->has('order')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Sıralama
                            </label>
                            <div class="col-7">
                                <input class="form-control m-input" type="number" min="1" max="100000" id="order" name="order" 
                                    @if (Request::segment(2) != 'add' && empty(old('order'))) 
                                        value="{{ $category->order }}"
                                    @else 
                                        value="{{ old('order') }}" 
                                    @endif

                                    @if (Request::segment(2) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                required autofocus>
                                @if ($errors->has('order'))
                                    <div id="order-error" class="form-control-feedback">{{ $errors->first('order') }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="m-form__group form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Pasif / Aktif
                            </label>
                            <div class="col-3">
                                <span class="m-switch">
                                    <label>
                                        <input type="checkbox" @if ($category->status == 'active' || Request::segment(2) == 'add') {{ 'checked="checked"' }} @endif  id="status" name="status" value="active" 
                                            @if (Request::segment(2) == 'delete')
                                                disabled="disabled" 
                                            @endif
                                        />
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    @endif

                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                @if (Request::segment(2) == 'add' || Request::segment(2) == 'edit')
                                    <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydet
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                @elseif (Request::segment(2) == 'delete')
                                    <div class="alert alert-danger" role="alert">
                                        <strong> Siliyorsunuz... </strong>
                                        Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.
                                    </div>
                                    <button class="btn btn-danger m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydı Sil
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </form>

		</div>
	</div>
@endsection

@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $('#postBtn').click(function(e) {

                e.preventDefault();
                var btn = $(this);
                var form = $(this).closest('form');

                form.validate({
                    rules: {
                        title: {
                            required: true
                        },
                        @if ( (Request::segment(2) == 'edit' && is_null(Request::segment(4))) || Request::segment(2) == 'add' )
                            order: {
                                required: true
                            }
                        @endif
                    }
                });

                if (!form.valid()) {
                    return;
                }
                
                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                form.submit();
            });
        });
    </script>
@endsection
