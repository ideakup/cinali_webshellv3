@extends('layouts.webshell')

@section('content') 

	<div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('menu/list') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Menü Listesi
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        @if(is_null($menu) && Request::segment(3) == 0 && Request::segment(1) == 'form')
                            <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    Formlar
                                </span>
                            </a>
                        @else
                            <a href="{{ url('menu') }}/{{ Request::segment(2) }}/{{ Request::segment(3) }}" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    İçerik Listesi @if(!is_null($menu))({{ $menu->variableLang($langs->first()->code)->name }})@endif
                                </span>
                            </a>
                        @endif
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            @if(is_null($menu) && Request::segment(3) == 0 && Request::segment(1) == 'form')
                                Yeni Form Ekle
                            @else
                                @if (Request::segment(4) == 'add')
                                    İçerik Ekle
                                @elseif (Request::segment(4) == 'edit')
                                    @if (is_null(Request::segment(6)))
                                        İçerik Detayları
                                    @else
                                        İçerik Düzenle
                                    @endif
                                @elseif (Request::segment(4) == 'delete')
                                    İçerik Sil
                                @endif
                            @endif
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
	<div class="m-content">

		<div class="m-portlet m-portlet--mobile">
            
            <div class="m-portlet__head">

                <div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">

                            @if(is_null($menu) && Request::segment(3) == 0 && Request::segment(1) == 'form')
                                Yeni Form Ekle
                            @else
                                
                                @if (Request::segment(4) == 'add')
                                    İçerik Ekle
                                @elseif (Request::segment(4) == 'edit')
                                    @if (is_null(Request::segment(6)))
                                        İçerik Detayları
                                    @else
                                        İçerik Düzenle
                                    @endif
                                @elseif (Request::segment(4) == 'delete')
                                    İçerik Sil ({{ $menu->variableLang($langs->first()->code)->name }})
                                @elseif (Request::segment(4) == 'galup')
                                    Foto Galeri Düzenle ({{ $menu->variableLang($langs->first()->code)->name }})
                                @endif

                            @endif

						</h3>
					</div>
				</div>
                
				<div class="m-portlet__head-tools">
                    @if (Request::segment(4) == 'edit' && is_null(Request::segment(6)))
                        <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}/delete/{{Request::segment(5)}}" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" title="İçerik Sil">
                            <i class="fa fa-trash"></i>
                        </a>
                    @endif
				</div>

			</div>
            
			<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('menu/content/save') }}" id="contentForm">
                
                {{ csrf_field() }}
                <input type="hidden" name="crud" value="{{ Request::segment(4) }}">
                <input type="hidden" name="menu_id" value="{{ (!is_null($menu)) ? $menu->id : '' }}">
                <input type="hidden" name="content_id" value="{{ (!is_null($content)) ? $content->id : '' }}">

                <input type="hidden" name="lang" value="{{ Request::segment(6) }}">
                <input type="hidden" id="formdata" name="formdata"> 
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                <div class="m-portlet__body">
                    
                    <div class="form-group m-form__group row">

                        <div class="col-10 ml-auto">
                            <h3 class="m-form__section">

                                @if(is_null($menu) && Request::segment(3) == 0 && Request::segment(1) == 'form')
                                    Yeni Form Ekle
                                @else

                                    @if (!is_null(Request::segment(6))) 
                                        [{{ Request::segment(6) }}] 
                                    @endif

                                    @if (Request::segment(4) == 'add')
                                        İçerik Ekle
                                    @elseif (Request::segment(4) == 'edit')
                                        @if (is_null(Request::segment(6)))
                                            İçerik Detayları
                                        @else
                                            İçerik Düzenle
                                        @endif
                                    @elseif (Request::segment(4) == 'delete')
                                        İçerik Sil ({{ $menu->variableLang($langs->first()->code)->name }})
                                    @elseif (Request::segment(4) == 'galup')
                                        Galeri Düzenle
                                    @endif

                                    @if (Request::segment(4) != 'add')
                                        <small> (
                                            @if ($content->type == 'group')
                                                İçerik Grubu
                                            @elseif ($content->type == 'text')
                                                Metin & HTML
                                            @elseif ($content->type == 'photo')
                                                Fotoğraf
                                            @elseif ($content->type == 'photogallery')
                                                Foto Galeri
                                            @elseif ($content->type == 'link')
                                                Button & Link
                                            @elseif ($content->type == 'slide')
                                                Slide
                                            @elseif ($content->type == 'form')
                                                Form
                                            @elseif ($content->type == 'seperator')
                                                Seperatör
                                            @elseif ($content->type == 'file')
                                                Dosya
                                            @endif
                                        ) </small>
                                    @endif

                                @endif

                            </h3>
                        </div>

                    </div>

                    @include('crudpartials.title')

                    @include('crudpartials.slug')

                    @if(Request::segment(4) == 'edit' && is_null(Request::segment(6)) && ($content->type == 'photogallery' || $content->type == 'group'))
                        <div class="form-group m-form__group row @if ($errors->has('releaseDate')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Tarih
                            </label>
                            <div class="col-7">
                                <div class="input-group date">
                                    <input type="text" class="form-control m-input" name="releaseDate" value="@if(!empty($content->release_date)){{Carbon\Carbon::parse($content->release_date)->format('d/m/Y')}}@endif" readonly="" placeholder="Tarih Seçiniz..." id="release_datepicker" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    
                    @include('crudpartials.type')
                    
                    @include('crudpartials.order')
                        
                    @include('crudpartials.status')
                    
                    @include('crudpartials.tag')

                    @include('crudpartials.category')


                    @include('crudpartials.short_content')

                    @include('crudpartials.content')

                    @include('crudpartials.props')

                    @include('crudpartials.form')

                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                @if (Request::segment(4) == 'add' || Request::segment(4) == 'edit')

                                    <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydet
                                    </button>
                                    &nbsp;&nbsp;

                                    @if(is_null($menu) && Request::segment(3) == 0)
                                        <a href="{{ url('form/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                            Vazgeç
                                        </a>
                                    @else
                                        <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                            Vazgeç
                                        </a>
                                    @endif

                                @elseif (Request::segment(4) == 'delete')

                                    <div class="alert alert-danger" role="alert">
                                        <strong> Siliyorsunuz... </strong>
                                        Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.
                                    </div>
                                    <button class="btn btn-danger m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydı Sil
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/content') }}/{{ Request::segment(3) }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>

                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>

        @include('crudpartials.dropzone')
        @include('crudpartials.dropzonefile')

	</div>

@endsection


@section('inline-scripts')

    @include('crudpartials.contentjs')

    @include('crudpartials.formjs')

    @include('crudpartials.dropzonejs')
    
    @include('crudpartials.dropzonejsfile')


<script type="text/javascript">

    
    $(document).ready(function(){
       

        @if (Request::segment(4) == 'add')
            $('#type').select2({
                placeholder: "Seçiniz..."
            });
        @endif
        
        @if (Request::segment(4) == 'edit' && !is_null(Request::segment(6)))
            @if ($content->type == 'text')
                /*
                    $('#row').select2({
                        placeholder: "Seçiniz..."
                    });
                */
            @endif
            @if ($content->type == 'form')
                var formBuilder = $('#fb-editor').formBuilder(options);
            @endif
        @endif

        @if (Request::segment(4) == 'edit' && is_null(Request::segment(6)))
            @if ($content->type == 'group' || $content->type == 'text' || $content->type == 'photogallery')
                $('#tag').select2({
                    placeholder: "Seçiniz..."
                });
                $('#category').select2({
                    placeholder: "Seçiniz..."
                });
                $("#release_datepicker").datepicker({format: "dd/mm/yyyy", clearBtn: true, todayHighlight: true});
            @endif

            @if ($content->type == 'photo' || $content->type == 'photogallery' || $content->type == 'seperator')
                $('#props').select2({
                    placeholder: "Seçiniz..."
                });
            @endif
        @endif

        $('#postBtn').click(function(e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    @if (Request::segment(4) == 'delete' || (Request::segment(4) != 'add' && is_null($content->variableLang(Request::segment(6)))))
                        title: {
                            required: true
                        },
                    @endif
                    @if (Request::segment(4) == 'add')
                        type: {
                            required: true
                        },
                    @endif
                    order: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            @if (Request::segment(4) == 'edit' && !is_null(Request::segment(6)))
                @if ($content->type == 'form')
                    $('#formdata').val(formBuilder.actions.getData('json'));
                @endif
            @endif
            
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            form.submit();
        });
    });
</script>

<style type="text/css">
    .m-portlet__body > .form-group{
        display: flex;
    }
</style>
@endsection
