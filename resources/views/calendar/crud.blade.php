@extends('layouts.webshell')

@section('content')
    
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Takvim
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <a href="{{ url('calendar/list') }}" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Etkinlik Listesi
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            @if (Request::segment(2) == 'add')
                                Etkinlik Ekle
                            @elseif (Request::segment(2) == 'edit')
                                @if (is_null(Request::segment(4)))
                                    Etkinlik Ayarları
                                @else
                                    Etkinlik Düzenle
                                @endif
                            @elseif (Request::segment(2) == 'delete')
                                Etkinlik Sil
                            @endif
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							@if (Request::segment(2) == 'add')
                                Etkinlik Ekle
                            @elseif (Request::segment(2) == 'edit')
                                @if (is_null(Request::segment(4)))
                                    Etkinlik Ayarları
                                @else
                                    Etkinlik Düzenle
                                @endif
                            @elseif (Request::segment(2) == 'delete')
                                Etkinlik Sil
                            @endif
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
                    @if (Request::segment(2) == 'edit' && is_null(Request::segment(4)))
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="{{ url('calendar/delete') }}/{{ $calendar->id }}" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" title="Etkinliği Sil">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </li>
                        </ul>
                    @else
                        @if($calendar->capacity == -1)
                            <strong>Kapalı Grup Ulaşım Adresi:</strong> <a href="http://cinali_tanitim.local/tr/etkinlikler/{{ $calendar->id }}" target="_blank">http://cinali_tanitim.local/tr/etkinlikler/{{ $calendar->id }}</a>
                        @endif
                    @endif
                    
				</div>
			</div>
			<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('calendar/save') }}" id="calendarForm">
                {{ csrf_field() }}
                <input type="hidden" name="crud" value="{{ Request::segment(2) }}">
                <input type="hidden" name="calendar_id" value="{{ Request::segment(3) }}">
                <input type="hidden" name="lang" value="{{ Request::segment(4) }}">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="m-portlet__body">

                    <div class="form-group m-form__group row">
                        <div class="col-10 ml-auto">
                            <h3 class="m-form__section">
                                @if (Request::segment(2) == 'add')
                                    Etkinlik Ekle
                                @elseif (Request::segment(2) == 'edit')
                                    @if (is_null(Request::segment(4)))
                                        Etkinlik Ayarları
                                    @else
                                        Etkinlik Düzenle
                                    @endif
                                @elseif (Request::segment(2) == 'delete')
                                    Etkinlik Sil
                                @endif
                            </h3>
                        </div>
                    </div>
                    
                    <div class="form-group m-form__group row @if ($errors->has('owner')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Etkinliğin Sahibi 
                        </label>
                        <div class="col-7">
                            @if ($errors->has('owner'))
                                <div id="owner-error" class="form-control-feedback">{{ $errors->first('owner') }}</div>
                            @endif
                            <select class="form-control m-select2" id="owner" name="owner"
                                @if (Request::segment(2) == 'delete' || (Request::segment(2) == 'edit' && !is_null(Request::segment(4))))
                                    disabled="disabled" 
                                @endif
                            >
                                <option value="muze" @if ($calendar->owner == 'muze') selected @endif> Müze </option>
                                <option value="vakif" @if ($calendar->owner == 'vakif') selected @endif> Vakıf </option>
                                <option value="kutuphane" @if ($calendar->owner == 'kutuphane') selected @endif> Kütüphane </option>
                                <option value="dukkan" @if ($calendar->owner == 'dukkan') selected @endif> Dükkan </option>
                                <option value="fillibahce" @if ($calendar->owner == 'fillibahce') selected @endif> Filli Bahçe </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group m-form__group row @if ($errors->has('type')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Etkinlik Tipi 
                        </label>
                        <div class="col-7">
                            @if ($errors->has('type'))
                                <div id="type-error" class="form-control-feedback">{{ $errors->first('type') }}</div>
                            @endif
                            <select class="form-control m-select2" id="type" name="type"
                                @if (Request::segment(2) == 'delete' || (Request::segment(2) == 'edit' && !is_null(Request::segment(4))))
                                    disabled="disabled" 
                                @endif
                            >
                                <option value="other" @if ($calendar->type == 'other') selected @endif> Diğer </option>
                                <option value="kutuphaneetkinligi" @if ($calendar->type == 'kutuphaneetkinligi') selected @endif> Kütüphane Etkinliği </option>
                                <option value="muzeetkinligi" @if ($calendar->type == 'muzeetkinligi') selected @endif> Müze Etkinliği</option>
                                <option value="filmgosterimi" @if ($calendar->type == 'filmgosterimi') selected @endif> Film Gösterimi </option>
                                
                                <option value="soylesi" @if ($calendar->type == 'soylesi') selected @endif> Söyleşi </option>
                                <option value="konser" @if ($calendar->type == 'konser') selected @endif> Konser </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group m-form__group row @if ($errors->has('title')) has-danger @endif">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Başlık
                        </label>
                        <div class="col-7">
                            <input class="form-control m-input" type="text" id="title" name="title" 
                                @if (Request::segment(2) != 'add' && empty(old('title')))
                                    value="@if (is_null($calendar->variableLang(Request::segment(4)))){{ $calendar->variableLang($langs->first()->code)->title }}@else{{ $calendar->variableLang(Request::segment(4))->title }}@endif"
                                @else 
                                    value="{{ old('title') }}" 
                                @endif

                                @if (Request::segment(2) == 'delete' || (Request::segment(2) == 'edit' && is_null(Request::segment(4))))
                                    disabled="disabled" 
                                @endif
                            required autofocus>
                            @if ($errors->has('title'))
                                <div id="title-error" class="form-control-feedback">{{ $errors->first('title') }}</div>
                            @endif
                        </div>
                    </div>

                    @if (Request::segment(2) == 'edit' && is_null(Request::segment(4)))

                        <div class="form-group m-form__group row @if ($errors->has('tag')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Etiket (Ne)
                            </label>
                            <div class="col-7">
                                @if ($errors->has('tag'))
                                    <div id="tag-error" class="form-control-feedback">{{ $errors->first('tag') }}</div>
                                @endif
                                <select class="form-control m-select2" id="tag" name="tag[]" multiple="multiple"
                                    @if (Request::segment(4) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                >
                                    <option value="null">Seçiniz...</option>

                                    @foreach ($tags as $tag)
                                        <option value="{{ $tag->id }}" @if ($calendar->tags()->where('tag_id', $tag->id)->count()) {{ 'selected' }} @endif> {{ $tag->variableLang($langs->first()->code)->title }} </option>
                                    @endforeach
                                    
                                </select>
                            </div>
                        </div>

                        <div class="form-group m-form__group row @if ($errors->has('category')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Kategori (Nerede)
                            </label>
                            <div class="col-7">
                                @if ($errors->has('category'))
                                    <div id="category-error" class="form-control-feedback">{{ $errors->first('category') }}</div>
                                @endif
                                <select class="form-control m-select2" id="category" name="category[]" multiple="multiple"
                                    @if (Request::segment(4) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                >
                                    <option value="null">Seçiniz...</option>

                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" @if ($calendar->categories()->where('category_id', $category->id)->count()) {{ 'selected' }} @endif> {{ $category->variableLang($langs->first()->code)->title }} </option>
                                    @endforeach
                                    
                                </select>
                            </div>
                        </div>

                    @endif
                    
                    @if ( (Request::segment(2) == 'edit' && !is_null(Request::segment(4))) )
                        
                        <div class="form-group m-form__group @if ($errors->has('content')) has-danger @endif">
                            <label for="example-text-input">
                                İçerik
                            </label>
                            <textarea class="form-control m-input" id="content" name="content" rows="3">@if (!is_null($calendar->variableLang(Request::segment(4)))){{ $calendar->variableLang(Request::segment(4))->content }}@endif</textarea>
                            @if ($errors->has('content'))
                                <div id="content-error" class="form-control-feedback">{{ $errors->first('content') }}</div>
                            @endif
                        </div>

                        <div class="form-group m-form__group @if ($errors->has('address')) has-danger @endif">
                            <label for="example-text-input">
                                Yer/Mekan/Adres
                            </label>
                            <textarea class="form-control m-input" id="address" name="address" rows="3">@if (!is_null($calendar->variableLang(Request::segment(4)))){{ $calendar->variableLang(Request::segment(4))->address }}@endif</textarea>
                            @if ($errors->has('address'))
                                <div id="address-error" class="form-control-feedback">{{ $errors->first('address') }}</div>
                            @endif
                        </div>

                        <div class="form-group m-form__group @if ($errors->has('nocapmessage')) has-danger @endif">
                            <label for="example-text-input">
                                Kapasite Doldu Mesajı
                            </label>
                            <textarea class="form-control m-input" id="nocapmessage" name="nocapmessage" rows="3">@if (!is_null($calendar->variableLang(Request::segment(4)))){{ $calendar->variableLang(Request::segment(4))->nocapmessage }}@endif</textarea>
                            @if ($errors->has('nocapmessage'))
                                <div id="nocapmessage-error" class="form-control-feedback">{{ $errors->first('nocapmessage') }}</div>
                            @endif
                        </div>
                    
                        <div class="form-group m-form__group row">
                            <div class="col-6">
                                <div class="m-dropzone dropzone" action="{{ url('/uploadFile') }}" id="dropzoneFileUpload">
                                    <div class="m-dropzone__msg dz-message needsclick">
                                        <h3 class="m-dropzone__msg-title">
                                            Dosya yüklemek için dosyayı buraya sürükleyin yada bu alan tıklayın.
                                        </h3>
                                        <span class="m-dropzone__msg-desc">
                                            Bu alan <strong>jpg</strong> ve <strong>png</strong> formatlarını yüklemenize izin verir.
                                        </span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-6">
                                <div class="col-12" id="imgContainer">
                                    @if (empty($calendar->variableLang(Request::segment(4))->content))
                                        <div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show" role="alert">
                                            <strong>Uyarı!</strong> Yüklenmiş Görsel Bulunmuyor...
                                        </div>
                                    @else
                                        <img class="img-fluid" src="{{ url('upload/xlarge/'.$calendar->variableLang(Request::segment(4))->image_name) }}" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                    
                    @if ( (Request::segment(2) == 'edit' && is_null(Request::segment(4))) || Request::segment(2) == 'add' )
                        <div class="form-group m-form__group row @if ($errors->has('startDate')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Başlangıç Tarihi
                            </label>
                            <div class="col-7">
                                <div class="input-group date">
                                    <input type="text" class="form-control m-input" name="startDate" value="@if(!empty($calendar->start)){{Carbon\Carbon::parse($calendar->start)->format('d/m/Y')}}@endif" readonly="" placeholder="Tarih Seçiniz..." id="start_datepicker" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group m-form__group row @if ($errors->has('startTime')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Başlangıç Saati
                            </label>
                            <div class="col-7">
                                <div class="input-group date">
                                    <input type="text" class="form-control m-input" name="startTime" value="@if(!empty($calendar->start)){{Carbon\Carbon::parse($calendar->start)->format('H:i')}}@endif" readonly="" placeholder="Saat Seçiniz..." id="start_timepicker" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-clock"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group m-form__group row @if ($errors->has('endDate')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Bitiş Tarihi
                            </label>
                            <div class="col-7">
                                <div class="input-group date" id="enddate">
                                    <input type="text" class="form-control m-input" name="endDate" value="@if(!empty($calendar->end)){{Carbon\Carbon::parse($calendar->end)->format('d/m/Y')}}@endif" readonly="" placeholder="Tarih Seçiniz..." id="end_datepicker">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group m-form__group row @if ($errors->has('endTime')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Bitiş Saati
                            </label>
                            <div class="col-7">
                                <div class="input-group date">
                                    <input type="text" class="form-control m-input" name="endTime" value="@if(!empty($calendar->end)){{Carbon\Carbon::parse($calendar->end)->format('H:i')}}@endif" readonly="" placeholder="Saat Seçiniz..." id="end_timepicker">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-clock"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group m-form__group row @if ($errors->has('capacity')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">Kapasite</label>
                            
                            <div class="col-4">
                                @if ($errors->has('capacity_type'))
                                    <div id="capacity_type-error" class="form-control-feedback">{{ $errors->first('capacity_type') }}</div>
                                @endif
                                <select class="form-control m-select2" id="capacity_type" name="capacity_type"
                                    @if (Request::segment(2) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                >
                                    <option value="sinirli" @if (is_null($calendar->capacity) || $calendar->capacity > 0) selected @endif> Sınırlı </option>
                                    <option value="limitsiz" @if (!is_null($calendar->capacity) && $calendar->capacity == 0) selected @endif> Limitsiz </option>
                                    <option value="kapali" @if (!is_null($calendar->capacity) && $calendar->capacity == -1) selected @endif> Kapalı G. </option>
                                </select>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <input type="number" min="1" class="form-control" id="capacity" name="capacity" placeholder="" value="@if ($calendar->capacity > 0){{$calendar->capacity}}@endif" @if (!is_null($calendar->capacity) && $calendar->capacity <= 0) disabled @endif>
                                </div>
                            </div>
                        </div>

                        <div class="form-group m-form__group row @if ($errors->has('price')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">Ücret</label>
                            <div class="col-4">
                                @if ($errors->has('price_type'))
                                    <div id="price_type-error" class="form-control-feedback">{{ $errors->first('price_type') }}</div>
                                @endif
                                <select class="form-control m-select2" id="price_type" name="price_type"
                                    @if (Request::segment(2) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                >
                                    <option value="ucretli" @if (is_null($calendar->price) || $calendar->price > 0) selected @endif> Ücretli </option>
                                    <option value="ucretsiz" @if (is_null($calendar->price) || $calendar->price == 0) selected @endif> Ücretsiz </option>
                                </select>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    <input type="text" id="price" name="price" placeholder="" value="@if(is_null($calendar->price) || $calendar->price > 0){{str_replace('.', ',', $calendar->price)}}@endif" class="form-control price" data-inputmask="'alias': 'numeric', 'groupSeparator': '.', 'radixpoint': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'suffix': ' ₺', 'placeholder': '0'" @if(is_null($calendar->price) || $calendar->price == 0) disabled @endif>
                                </div>
                            </div>
                        </div>

                        <div class="form-group m-form__group row @if ($errors->has('reservationForm')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">Rezervasyon Formu</label>
                            
                            <div class="col-7">
                                @if ($errors->has('reservationForm'))
                                    <div id="reservationForm-error" class="form-control-feedback">{{ $errors->first('reservationForm') }}</div>
                                @endif
                                <select class="form-control m-select2" id="reservationForm" name="reservationForm"
                                    @if (Request::segment(2) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                >   
                                    <option value="null"> Seçiniz... </option>
                                    @foreach ($forms as $form)
                                        <option value="{{ $form->id }}" @if ($calendar->form_id == $form->id)
                                            selected
                                        @endif> {{ $form->variable->title }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group m-form__group row @if ($errors->has('order')) has-danger @endif">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Sıralama
                            </label>
                            <div class="col-7">
                                <input class="form-control m-input" type="number" min="1" max="100000" id="order" name="order" 
                                    @if (Request::segment(2) != 'add' && empty(old('order'))) 
                                        value="{{ $calendar->order }}"
                                    @else 
                                        value="{{ old('order') }}" 
                                    @endif

                                    @if (Request::segment(2) == 'delete')
                                        disabled="disabled" 
                                    @endif
                                required autofocus>
                                @if ($errors->has('order'))
                                    <div id="order-error" class="form-control-feedback">{{ $errors->first('order') }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="m-form__group form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                                Pasif / Aktif
                            </label>
                            <div class="col-3">
                                <span class="m-switch">
                                    <label>
                                        <input type="checkbox" @if ($calendar->status == 'active' || Request::segment(2) == 'add') {{ 'checked="checked"' }} @endif  id="status" name="status" value="active" 
                                            @if (Request::segment(2) == 'delete')
                                                disabled="disabled" 
                                            @endif
                                        />
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    @endif

                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-7">
                                @if (Request::segment(2) == 'add' || Request::segment(2) == 'edit')
                                    <button class="btn btn-accent m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydet
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                @elseif (Request::segment(2) == 'delete')
                                    <div class="alert alert-danger" role="alert">
                                        <strong> Siliyorsunuz... </strong>
                                        Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.
                                    </div>
                                    <button class="btn btn-danger m-btn m-btn--air m-btn--custom" id="postBtn">
                                        Kaydı Sil
                                    </button>
                                    &nbsp;&nbsp;
                                    <a href="{{ url('menu/list') }}" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                        Vazgeç
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </form>

		</div>
	</div>
@endsection

@section('inline-scripts')

    @php
        $uploadConfig = config('webshell.upload.imageType.calendar');
    @endphp

    <script type="text/javascript">

        Dropzone.options.dropzoneFileUpload = {
            headers: { 'X-CSRF-TOKEN': $('#token').val() },
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: {{ $uploadConfig['maxFiles'] }},
            maxFilesize: 5, // MB
            addRemoveLinks: true,
            accept: function(file, done) {
                done();
            },
            init: function () {
                //console.log('dropzone init');
            },
            sending: function(file, xhr, formData){
                formData.append('uploadType', '{{ $uploadConfig['uploadType'] }}');
                formData.append('calendarid', {{ $calendar->id }});
                formData.append('lang_code', '{{ Request::segment(4) }}');
            },
            success: function(file, xhr, event){
                var data = jQuery.parseJSON(xhr);
                $("#imgContainer").html('<img class="img-fluid" src="{{ url('') }}/upload/xlarge/' + data.location + '" />');
                this.removeFile(file);
            },
            transformFile: function(file, done) { 
                //console.log('transformFile');
                //console.log(file);
                var myDropZone = this;
                // Create the image editor overlay
                var editor = document.createElement('div');
                editor.style.position = 'fixed';
                editor.style.left = 0;
                editor.style.right = 0;
                editor.style.top = 0;
                editor.style.bottom = 0;
                editor.style.zIndex = 9999;
                editor.style.backgroundColor = '#000';
                document.body.appendChild(editor);
                // Create confirm button at the top left of the viewport
                var buttonConfirm = document.createElement('button');
                buttonConfirm.style.position = 'absolute';
                buttonConfirm.style.left = '10px';
                buttonConfirm.style.top = '10px';
                buttonConfirm.style.zIndex = 9999;
                buttonConfirm.textContent = 'Onayla';
                editor.appendChild(buttonConfirm);
                buttonConfirm.addEventListener('click', function() {
                    // Get the canvas with image data from Cropper.js
                    var canvas = cropper.getCroppedCanvas();
                    // Turn the canvas into a Blob (file object without a name)
                    canvas.toBlob(function(blob) {
                        // Create a new Dropzone file thumbnail

                        myDropZone.createThumbnail(
                            blob,
                            myDropZone.options.thumbnailWidth,
                            myDropZone.options.thumbnailHeight,
                            myDropZone.options.thumbnailMethod,
                            false, 
                            function(dataURL) {
                                // Update the Dropzone file thumbnail
                                myDropZone.emit('thumbnail', file, dataURL);
                                // Return the file to Dropzone
                                done(blob);
                            });
                        });
                    // Remove the editor from the view
                    document.body.removeChild(editor);
                });

                var buttonCancel = document.createElement('button');
                buttonCancel.style.position = 'absolute';
                buttonCancel.style.right = '10px';
                buttonCancel.style.top = '10px';
                buttonCancel.style.zIndex = 9999;
                buttonCancel.textContent = 'İptal';
                editor.appendChild(buttonCancel);

                buttonCancel.addEventListener('click', function() {
                    editor.remove();
                });

                var selectSize = document.createElement('select');
                selectSize.setAttribute("id", "selectSize");
                selectSize.style.position = 'absolute';
                selectSize.style.left = '40%';
                selectSize.style.top = '10px';
                selectSize.style.width = '20%';
                selectSize.style.zIndex = 9999;

                @foreach ($uploadConfig['type'] as $key => $value)
                    var opt = document.createElement('option');
                    opt.setAttribute("label", "{{ $value['title'] }}");
                    opt.setAttribute("value", {{ $value['aspectRatio'] }});
                    selectSize.appendChild(opt);
                @endforeach

                editor.appendChild(selectSize);

                selectSize.addEventListener('change', function() {
                    //console.log(this.value);
                    cropper.setAspectRatio(this.value);
                });

                // Create an image node for Cropper.js
                var image = new Image();
                image.src = URL.createObjectURL(file);
                editor.appendChild(image);

                // Create Cropper.js
                var cropper = new Cropper(image, { aspectRatio: {{ $uploadConfig['type']['default']['aspectRatio'] }}, zoomable: false, zoomOnTouch:false, zoomOnWheel:false, wheelZoomRatio:false, autoCropArea: 1});
                
            }
        };

        $(document).ready(function(){

            $('#reservationForm').select2({
                placeholder: "Seçiniz..."
            });

            $('#tag').select2({
                placeholder: "Seçiniz..."
            });

            $('#category').select2({
                placeholder: "Seçiniz..."
            });

            $('#owner').select2({
                placeholder: "Seçiniz..."
            });

            $('#type').select2({
                placeholder: "Seçiniz..."
            });

            $('#capacity_type').select2({
                placeholder: "Seçiniz..."
            }).on("change", function (e) { 
                if($('#capacity_type').val() == 'sinirli'){
                    $('#capacity').prop("disabled", false);
                }else{
                    $('#capacity').val('');
                    $('#capacity').prop("disabled", true);
                }
            });

             $('#price_type').select2({
                placeholder: "Seçiniz..."
            }).on("change", function (e) { 
                if($('#price_type').val() == 'ucretli'){
                    $('#price').prop("disabled", false);
                }else{
                    $('#price').val('');
                    $('#price').prop("disabled", true);
                } 
            });

            $("#start_datepicker").datepicker({format: "dd/mm/yyyy", clearBtn: true, todayHighlight: true});
            $("#start_timepicker").timepicker({showMeridian:!1, defaultTime:"", clearBtn: true});
            $("#end_datepicker").datepicker({format: "dd/mm/yyyy", clearBtn: true, todayHighlight: true});
            $("#end_timepicker").timepicker({showMeridian:!1, defaultTime:"", clearBtn: true});

            $("#price").inputmask();

            tinymce.init({
                selector: 'textarea#content',

                @if (Request::segment(4) == 'delete')
                    readonly : 1,
                @endif
                
                //plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
                //Color Picker, Context Menu, MoxieManager, noneditable, pageembed,  Text Color documentation
                //, linkchecker TEKRAR BAK
                //codesample, directionality, fullpage, help, pagebreak, paste, print, template, textpattern, toc, visualchars

                plugins: 'advlist lists anchor autolink autoresize charmap code fullscreen hr image imagetools insertdatetime link media nonbreaking preview searchreplace table visualblocks wordcount importcss paste',
                
                // table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol
                // formatselect styleselect fontselect fontsizeselect
                toolbar: 'undo redo | formatselect | bold italic underline strikethrough subscript superscript | removeformat | forecolor backcolor | alignleft aligncenter alignright alignjustify alignnone | numlist bullist outdent indent | link image media | anchor charmap hr insertdatetime nonbreaking | searchreplace | visualblocks code fullscreen',
                imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",

                branding: false,
                min_height: 400,

                relative_urls: false,
                convert_urls: false,


                /* without images_upload_url set, Upload tab won't show up*/
                images_upload_url: '{{ url('/uploadFile') }}',
                images_upload_base_path: '{{ url('/') }}',
                images_upload_credentials: true,
                images_upload_handler: function (blobInfo, success, failure) {
                   var xhr, formData;
                   xhr = new XMLHttpRequest();
                   xhr.withCredentials = false;
                   xhr.open('POST', '{{ url('/uploadFile') }}');
                   var token = '{{ csrf_token() }}';
                   xhr.setRequestHeader("X-CSRF-Token", token);
                   xhr.onload = function() {
                       var json;
                       if (xhr.status != 200) {
                           failure('HTTP Error: ' + xhr.status);
                           return;
                       }
                       json = JSON.parse(xhr.responseText);

                       if (!json || typeof json.location != 'string') {
                           failure('Invalid JSON: ' + xhr.responseText);
                           return;
                       }
                       success(json.location);
                   };
                   formData = new FormData();
                   formData.append('file', blobInfo.blob(), blobInfo.filename());
                   formData.append('uploadType', 'editor');
                   xhr.send(formData);
               }
            });

            tinymce.init({
                selector: 'textarea#address',

                @if (Request::segment(4) == 'delete')
                    readonly : 1,
                @endif
                
                //plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
                //Color Picker, Context Menu, MoxieManager, noneditable, pageembed,  Text Color documentation
                //, linkchecker TEKRAR BAK
                //codesample, directionality, fullpage, help, pagebreak, paste, print, template, textpattern, toc, visualchars

                plugins: 'advlist lists anchor autolink autoresize charmap code fullscreen hr image imagetools insertdatetime link media nonbreaking preview searchreplace table visualblocks wordcount importcss paste',
                
                // table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol
                // formatselect styleselect fontselect fontsizeselect
                toolbar: 'undo redo | formatselect | bold italic underline strikethrough subscript superscript | removeformat | forecolor backcolor | alignleft aligncenter alignright alignjustify alignnone | numlist bullist outdent indent | link image media | anchor charmap hr insertdatetime nonbreaking | searchreplace | visualblocks code fullscreen',
                imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",

                branding: false,
                min_height: 400,

                relative_urls: false,
                convert_urls: false,


                /* without images_upload_url set, Upload tab won't show up*/
                images_upload_url: '{{ url('/uploadFile') }}',
                images_upload_base_path: '{{ url('/') }}',
                images_upload_credentials: true,
                images_upload_handler: function (blobInfo, success, failure) {
                   var xhr, formData;
                   xhr = new XMLHttpRequest();
                   xhr.withCredentials = false;
                   xhr.open('POST', '{{ url('/uploadFile') }}');
                   var token = '{{ csrf_token() }}';
                   xhr.setRequestHeader("X-CSRF-Token", token);
                   xhr.onload = function() {
                       var json;
                       if (xhr.status != 200) {
                           failure('HTTP Error: ' + xhr.status);
                           return;
                       }
                       json = JSON.parse(xhr.responseText);

                       if (!json || typeof json.location != 'string') {
                           failure('Invalid JSON: ' + xhr.responseText);
                           return;
                       }
                       success(json.location);
                   };
                   formData = new FormData();
                   formData.append('file', blobInfo.blob(), blobInfo.filename());
                   formData.append('uploadType', 'editor');
                   xhr.send(formData);
               }
            });

            tinymce.init({
                selector: 'textarea#nocapmessage',

                @if (Request::segment(4) == 'delete')
                    readonly : 1,
                @endif
                
                //plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
                //Color Picker, Context Menu, MoxieManager, noneditable, pageembed,  Text Color documentation
                //, linkchecker TEKRAR BAK
                //codesample, directionality, fullpage, help, pagebreak, paste, print, template, textpattern, toc, visualchars

                plugins: 'advlist lists anchor autolink autoresize charmap code fullscreen hr image imagetools insertdatetime link media nonbreaking preview searchreplace table visualblocks wordcount importcss paste',
                
                // table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol
                // formatselect styleselect fontselect fontsizeselect
                toolbar: 'undo redo | formatselect | bold italic underline strikethrough subscript superscript | removeformat | forecolor backcolor | alignleft aligncenter alignright alignjustify alignnone | numlist bullist outdent indent | link image media | anchor charmap hr insertdatetime nonbreaking | searchreplace | visualblocks code fullscreen',
                imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",

                branding: false,
                min_height: 400,

                relative_urls: false,
                convert_urls: false,


                /* without images_upload_url set, Upload tab won't show up*/
                images_upload_url: '{{ url('/uploadFile') }}',
                images_upload_base_path: '{{ url('/') }}',
                images_upload_credentials: true,
                images_upload_handler: function (blobInfo, success, failure) {
                   var xhr, formData;
                   xhr = new XMLHttpRequest();
                   xhr.withCredentials = false;
                   xhr.open('POST', '{{ url('/uploadFile') }}');
                   var token = '{{ csrf_token() }}';
                   xhr.setRequestHeader("X-CSRF-Token", token);
                   xhr.onload = function() {
                       var json;
                       if (xhr.status != 200) {
                           failure('HTTP Error: ' + xhr.status);
                           return;
                       }
                       json = JSON.parse(xhr.responseText);

                       if (!json || typeof json.location != 'string') {
                           failure('Invalid JSON: ' + xhr.responseText);
                           return;
                       }
                       success(json.location);
                   };
                   formData = new FormData();
                   formData.append('file', blobInfo.blob(), blobInfo.filename());
                   formData.append('uploadType', 'editor');
                   xhr.send(formData);
               }
            });

            

            $('#postBtn').click(function(e) {

                e.preventDefault();
                var btn = $(this);
                var form = $(this).closest('form');

                form.validate({
                    rules: {
                        name: {
                            required: true
                        },
                        @if (Request::segment(2) == 'edit')
                        slug: {
                            required: true
                        },
                        @endif
                        order: {
                            required: true
                        }
                    }
                });

                if (!form.valid()) {
                    return;
                }
                
                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                form.submit();
            });
        });
    </script>
@endsection
