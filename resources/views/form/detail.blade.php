@extends('layouts.webshell')

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Formlar
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            Form Listesi
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="m-content">
        @php

            //dump($form);
            $formvariable = $form->variable()->first();
            $formvariableArr = json_decode($formvariable->content);
            //dump($formvariableArr);
            $formdata = App\FormData::where('form_id', $form->id)->get();
            //dump($formdata);
            //dump($formDist);

        @endphp
        <div class="m-portlet m-portlet--mobile">
            
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ $form->variable->title }} 
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    
                        @if($form->status == 'passive')
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="{{ url('/form/detail/'.$form->id.'/sendArchive') }}" class="btn btn-warning m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                        <span>
                                            <span>
                                                Arşive Gönder
                                            </span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        @else
                            <span class="m--font-danger">Form Aktif Olduğu İçin Arşive Gönderemezsiniz...</span>
                        @endif

                </div>
            </div>
            
            <div class="m-portlet__body">

                <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ url('formdata/export') }}" id="formDataForm">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <input type="hidden" name="formId" id="formId" value="{{ $form->id }}">
                    <input type="hidden" name="exportCont" id="exportCont" value="0">
                    @foreach ($formDist as $fff)
                        @php
                            if($fff->source_type == 'calendar'){
                                $formCont = App\Calendar::find($fff->source_id);
                            }else if($fff->source_type == 'menu'){
                                $formCont = App\Menu::find($fff->source_id);
                            }
                        @endphp

                    @endforeach

                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-2 col-form-label">
                            Veri Seç
                        </label>
                        <div class="col-7">
                            <select class="form-control m-select2" id="formDist" name="formDist">
                                <option value="null">Seçiniz...</option>

                                @foreach ($formDist as $fff)

                                    @if($fff->source_type == 'calendar')
                                        @php 
                                            $formCont = App\Calendar::find($fff->source_id);
                                        @endphp

                                        <option value="{{ $fff->source_type }}:{{ $fff->source_id }}"> 
                                        
                                            {{ $formCont->variable->title }} 
                                            @if($formCont->type == 'other')
                                                (Diğer)
                                            @elseif($formCont->type == 'kutuphaneetkinligi')
                                                (Kütüphane Etkinliği)
                                            @elseif($formCont->type == 'muzeetkinligi')
                                                (Müze Etkinliği)
                                            @elseif($formCont->type == 'filmgosterimi')
                                                (Film Gösterimi)
                                            @elseif($formCont->type == 'soylesi')
                                                (Söyleşi)
                                            @elseif($formCont->type == 'konser')
                                                (Konser)
                                            @endif
                                            
                                        </option>

                                    @elseif($fff->source_type == 'menu')
                                        @php 
                                            $formCont = App\Menu::find($fff->source_id);
                                        @endphp

                                        <option value="{{ $fff->source_type }}:{{ $fff->source_id }}"> 
                                        
                                            {{ $formCont->variable->title }} 
                                           
                                        </option>

                                    @endif
                                    
                                @endforeach
                                
                            </select>
                        </div>
                        <div class="col-3">
                            <button type="button" id="getirButton" class="btn btn-primary">Getir</button>
                            <button type="button" id="exportButton" class="btn btn-secondary">Dışarı Aktar</button>
                        </div>
                    </div>
                </form>

                <table class="table table-striped- table-bordered table-hover table-checkable" id="formListDataTable">
                    <thead>
                        <tr>
                            
                            <th>
                                ID
                            </th>

                            @foreach ($formvariableArr as $field)
                                @if(isset($field->name))
                                    <th>
                                        {{ (!empty($field->label) ? $field->label : '') }} - 
                                        {{ (!empty($field->placeholder) ? $field->placeholder : '') }}
                                    </th>
                                @endif
                            @endforeach

                            <th>
                                Kaynak
                            </th>
                            <th>
                                
                            </th>

                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('inline-scripts')
    <script type="text/javascript">
        
        $(document).ready(function(){

            $('#formDist').select2({
                placeholder: "Seçiniz..."
            });


            var table = $('#formListDataTable').DataTable({
                responsive: true,
                dom: `<'row'<'col-sm-12'tr>> <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                lengthMenu: [5, 10, 25, 50, 100],
                pageLength: 50,
                language: {
                    "lengthMenu": "Her sayfada _MENU_ kayıt göster",
                    "info": "Sayfa: _PAGE_/_PAGES_ ",
                    "infoEmpty": "Kayıt Yok.",
                    "zeroRecords": "Kayıt Yok.",
                    "infoFiltered": "(Toplam _MAX_ kayıttan filtrelendi)",
                    "processing": "Yükleniyor...",
                },
                searching: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                stateSave: false,
                ordering: false,
                ajax: {
                    url: "/getFormData", // ajax source
                    type: "POST",
                    headers: { 'X-CSRF-TOKEN': $('#token').val() },
                    data: {
                        // parameters for custom backend script demo
                        formId: function() { return $('#formId').val() },
                        exportCont: function() { return $('#exportCont').val() },
                        columnsDef: [
                            'id', 
                            @foreach ($formvariableArr as $field)
                                @if(isset($field->name))
                                    '{{ $field->name }}',
                                @endif
                            @endforeach
                            'source',
                            'actions'
                        ],
                    },
                },
                columns: [
                    {name: 'id'},
                    @foreach ($formvariableArr as $field)
                        @if(isset($field->name))
                            {name: '{{ $field->name }}'},
                        @endif
                    @endforeach
                    {name: 'source'},
                    {name: 'actions'}
                ],
                columnDefs: [
                /*
                    {
                        targets: 0,
                        orderable: false,
                        visible: false,
                    },
                    {
                        targets: 2,
                        render: function(data, type, full, meta) {
                            var status = {
                                'active': {'title': 'Aktif', 'class': 'm-badge--brand'},
                                'passive': {'title': 'Pasif', 'class': ' m-badge--metal'}
                            };
                            if (typeof status[data] === 'undefined') {
                                return data;
                            }
                            return '<span class="m-badge ' + status[data].class + ' m-badge--wide">' + status[data].title + '</span>';
                        },
                    },
                    {
                        targets: -1,
                        title: 'İşlemler',
                        orderable: false,
                        render: function(data, type, full, meta) {   

                            var editButtons = '';
                            @foreach ($langs as $lang)
                                editButtons += `<a href="{{ url('menu/content') }}/0/edit/`+full[0]+`/{{ $lang->code }}" class="m-portlet__nav-link btn btn-sm m-btn btn-outline-brand m-btn--pill" title="Düzenle {{ $lang->code }}">
                                    {{ $lang->code }}
                                </a> `;
                            @endforeach

                            return editButtons+`
                            <a href="detail/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Gelen Formlar">
                              <i class="fa fa-envelope-open"></i>
                            </a>
                            <a href="/`+full[0]+`" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Gelen Formlar">
                              <i class="fa fa-cog"></i>
                            </a>`;

                        },
                    },
                */
                ],
            });

            $('#getirButton').on('click', function(e) {
                e.preventDefault();
                //console.log($('#formDist').val());
                $('#exportCont').val(0);
                table.search($('#formDist').val()).draw();
                
            });

            /*
            $('#exportButton').on('click', function(e) {
                e.preventDefault();
                $('#exportCont').val(1);
                table.ajax.reload();
                
                //console.log($('#exportCont').val());
                //console.log($('#exportCont').val());
                //console.log(table.ajax.params());
                
            });
            */

            $('#exportButton').click(function(e) {
                e.preventDefault();
                var btn = $(this);
                var form = $(this).closest('form');
                $('#exportCont').val(1);
                form.submit();
            });


        });
    </script>
@endsection
