@extends('layouts.webshell') 
@php use Carbon\Carbon; @endphp

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü 
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            Paraşüt Genel Bilgiler
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Paraşüt Üyelik Bilgileri
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="m-section__content">
                    <table class="table m-table">
                        <tbody>
                            <tr>
                                <td><b>Firma Id</b></td>
                                <td>:</td>
                                <td>{{$me['id']}}</td>
                            </tr>
                            <tr>
                                <td><b>Kullanıcı Ad Soyad</b></td>
                                <td>:</td>
                                <td>{{$me['attributes']['name']}}</td>
                            </tr>
                            <tr>
                                <td><b>Kullanıcı E-Posta</b></td>
                                <td>:</td>
                                <td>{{$me['attributes']['email']}}</td>
                            </tr>
                            <tr>
                                <td><b>Üyelik Tarihi</b></td>
                                <td>:</td>
                                <td>{{Carbon::parse($me['attributes']['created_at'])->format('d.m.Y')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!--
            <div class="m-portlet m-portlet--mobile">

                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Kasa ve Banka Bilgileri
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                    </div>
                </div>

                <div class="m-portlet__body">
                    <div class="table-scrollable table-scrollable-borderless">
                        <table class="table table-hover table-light">
                            <thead>
                                <tr class="uppercase">
                                    <th>#</th>
                                    <th>Tip</th>
                                    <th>İsim</th>
                                    <th>Banka</th>
                                    <th>Şube</th>
                                    <th>Hesap No</th>
                                    <th>Iban</th>
                                    <th>Durum</th>
                                    <th>İşlem</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($accounts as $account)
                                    <tr class="{{(empty($accounts_relationship[$account['id']])) ? '' : 'success'}}">
                                        <td>{{$i++}}</td>
                                        <td>{{($account['attributes']['account_type'] == 'cash') ? 'Kasa' : 'Banka'}}</td>
                                        <td>{{$account['attributes']['name']}}</td> 
                                        <td>{{$account['attributes']['bank_name']}}</td>
                                        <td>{{$account['attributes']['bank_branch']}}</td>
                                        <td>{{$account['attributes']['bank_account_no']}}</td>
                                        <td>{{$account['attributes']['iban']}}</td>
                                        <td>
                                            @if (empty($accounts_relationship[$account['id']]))
                                                <span class="label label-sm label-default"> İlişkili Değil </span>
                                            @else
                                                <span class="label label-sm label-success"> İlişkilendirilmiş </span>
                                                
                                                @if ($accounts_relationship[$account['id']]->type == 'kredi-karti')
                                                    <span class="label label-sm label-warning"> Kredi Kartı </span>
                                                @elseif ($accounts_relationship[$account['id']]->type == 'havale')
                                                    <span class="label label-sm label-warning"> Havale </span>
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            @if (empty($accounts_relationship[$account['id']]))
                                                <a href="{{url('parasut/account_tools/take/'.$account['id'])}}" class="btn btn-icon-only blue btn-table" data-toggle="tooltip" title="İlişkilendir"><i class="fa fa-hand-o-right" aria-hidden="true"></i></a>
                                            @else
                                                <a href="{{url('parasut/account_tools/remove/'.$accounts_relationship[$account['id']]->id)}}" class="btn btn-icon-only btn-danger btn-table" data-toggle="tooltip" title="İlişkiyi Kes"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                <a href="{{url('parasut/account_tools/change/'.$accounts_relationship[$account['id']]->id)}}" class="btn btn-icon-only btn-success btn-table" data-toggle="tooltip" title="Tip Değiştir"><i class="fa fa-repeat" aria-hidden="true"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        -->
    </div>

@endsection

@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#postBtn').click(function(e) {
                e.preventDefault();
                var btn = $(this);
                var form = $(this).closest('form');

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                form.submit();
            });
        });
    </script>
@endsection
