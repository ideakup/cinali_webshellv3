@extends('layouts.app') @php use Carbon\Carbon; @endphp

@section('content')
<div class="page-content-wrapper">
    <div class="page-content"> 

        <div class="page-head">
            <div class="page-title">
                <h1>Paraşüt
                    <small>Boş Sayfa Şablonu</small>
                </h1>
            </div>
        </div>

        @include('partials.breadcrumbs')  
        
        <div class="row">
            <div class="col-md-12">
                <h2>Cin Ali Dükkan Müşterileri</h2>
            	<div class="portlet light portlet-fit bordered">
                    <div class="portlet-body">
                        <div class="table-scrollable table-scrollable-borderless">
                            <table class="table table-hover table-light">
                                <thead>
                                    <tr class="uppercase">
                                        <th> # </th>
                                        <th> Ad Soyad </th>
                                        <th> E-Posta </th>
                                        <th> İlişki </th>
                                        <!-- <th> İşlem </th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($users as $element)
                                    <tr>
                                        <td> {{$i++}} </td>
                                        <td> {{$element->name}} </td>
                                        <td> {{$element->email}} </td>
                                        <td>
                                            @if (is_null($element->account_id))
                                                <span class="label label-sm label-default"> İlişki Yok </span>
                                            @else 
                                                <span class="label label-sm label-success"> İlişkilendirilmiş </span>
                                            @endif
                                        </td>
                                        <!--
                                            <td>
                                                
                                                @if (!empty($relationship[$element->id]))
                                                    <button class="btn btn-icon-only blue btn-table modal_button" data-toggle="modal" id="{{$element->id}}"><i class="fa fa-object-group" aria-hidden="true"></i></button>    
                                                @else
                                                    <a href="{{url('parasut/send/'.$element->id)}}" class="btn btn-icon-only blue btn-table"><i class="fa fa-hand-o-right" aria-hidden="true"></i></a>
                                                @endif
                                                
                                            </td>
                                        -->
                                    </tr>
                                    @endforeach                                 
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
        @php
            $eksik = '<span class="label label-sm label-warning">EKSİK</span>';
        @endphp
        
        @foreach ($relationship as $key => $value)
        <div class="modal fade" id="modal_{{$key}}" tabindex="-1" role="modal_{{$key}}" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">{{$value[1]->email}}</h4>
                    </div>
                    <div class="modal-body"> 
                        <p>Paraşüt sisteminde aynı e-posta adresine ait kişi mevcut. Sistem eşitleme sırasında hangi bilgilerin esas alınmasını istiyorsanız lütfen onu seçin. </p>
                        <div class="row">
                            <div class="col-md-6 internal">       
                                {!!empty($value[1]->name) ? $eksik : $value[1]->name!!}  
                                <hr> 
                                {!!empty($value[1]->city) ? $eksik : $value[1]->city!!}                   
                                @php
                                    dump($value[1]);
                                @endphp
                                <hr>
                            </div>
                            <div class="col-md-6 external">    
                                {!!empty($value[2]['attributes']['name']) ? $eksik : $value[2]['attributes']['name']!!} 
                                <hr>              
                                {!!empty($value[2]['attributes']['city']) ? $eksik : $value[2]['attributes']['city']!!}         
                                @php
                                    dump($value[2]['attributes']);
                                @endphp
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default">Yeni Kayıt Oluştur</button>
                        <button type="button" class="btn btn-success">İlişkilendiranil</button>                        
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        @endforeach
        

    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('.modal_button').click(function(){
            $('#modal_'+$(this).attr('id')).modal('show');
        });
	});
</script>
@endsection
