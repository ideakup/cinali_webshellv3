@extends('layouts.webshell') 
@php use Carbon\Carbon; @endphp

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Menü 
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ url('dashboard') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator"> - </li>
                    <li class="m-nav__item">
                        <span class="m-nav__link-text">
                            Ürün Senkronizasyonu
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Ürün Senkronizasyonu
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="table-scrollable table-scrollable-borderless">

                    <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert">
                        <div class="m-alert__icon">
                            <i class="flaticon-exclamation-1"></i>
                            <span></span>
                        </div>
                        <div class="m-alert__text">
                            <strong>Önemli Hatırlatma!</strong> Paraşüt'e yeni eklenen ürünü Dükkan'a eklemek için ilk adımdır. Ürünleri ilişkilendirdikten sonra, düzenlemek ve etkinleştirmek için dükkan admin panelinden ilgili ürüne gidiniz...
                            </br></br>
                            Paraşüt'te stok takibi yapılmayan ve barkod numarası bulunmayan ürünler bu listede görünmez...
                        </div>
                    </div>

                    @if (!empty($products['cannot']))
                        <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger fade show" role="alert">
                            <div class="m-alert__icon">
                                <i class="flaticon-exclamation-1"></i>
                                <span></span>
                            </div>
                            <div class="m-alert__text">
                                <strong>Uyarı !</strong> Kırmızı ile işaretlenmiş satırlardaki ürünler, Dükkan'da mevcut ancak Paraşüt'te mevcut değildir. Ürün Silinmiş yada Arşiv yapılmış olabilir.
                            </div>
                        </div>
                    @endif

                    @if (!empty($products['relationship']))
                        <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger fade show" role="alert">
                            <div class="m-alert__icon">
                                <i class="flaticon-exclamation-1"></i>
                                <span></span>
                            </div>
                            <div class="m-alert__text">
                                <strong>Uyarı !</strong> Sarı ile işaretlenmiş ürünler, aynı ürün kodu ile Paraşüt'te yer almaktadır. Lütfen eşleştirin.
                            </div>
                        </div>
                    @endif

                    <table class="table m-table">

                        <thead>

                            <tr>
                                <th> Barkod </th>
                                <th> Ürün Adı </th>
                                <th> Stok </th>
                                <th> Durum </th>
                                <th> İlişkilendirme Tarihi </th>
                                <th> İşlem </th>
                            </tr>
                        
                        </thead>

                        <tbody>

                            @php
                                $i = 1;
                            @endphp
                            @if (isset($products['cannot']))
                                @foreach ($products['cannot'] as $cannot)
                                    <tr class="m-table__row--danger">
                                        <td> {{$cannot->code}} </td>
                                        <td> {{$cannot->label}} </td>
                                        <td>  </td>
                                        <td>
                                            @if ($cannot->status == '1')
                                                Etkinleştirildi
                                            @elseif ($cannot->status == '0')
                                                Devre Dışı Bırakıldı
                                            @elseif ($cannot->status == '-1')
                                                İncelemede
                                            @elseif ($cannot->status == '-2')
                                                Arşivlendi
                                            @endif
                                        </td>
                                        <td> <span class="m-badge m-badge--primary m-badge--wide">İlişki Yok</span> </td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            @endif

                            @foreach ($data as $element)
                                @if (stristr($element['attributes']['code'], 'kargo') === false)
                                    <tr class="@if(!empty($products['relationship'][$element['id']])) m-table__row--warning @endif">
                                        <td> {{$element['attributes']['code']}} </td>
                                        <td> {{$element['attributes']['name']}} </td>
                                        <td> {{intval($element['attributes']['stock_count'])}} adet</td>
                                        <td>
                                            @if (!empty($products['paired_pro'][$element['id']]))
                                                @if ($products['paired_pro'][$element['id']]->status == '1')
                                                Etkinleştirildi
                                                @elseif ($products['paired_pro'][$element['id']]->status == '0')
                                                Devre Dışı Bırakıldı
                                                @elseif ($products['paired_pro'][$element['id']]->status == '-1')
                                                İncelemede
                                                @elseif ($products['paired_pro'][$element['id']]->status == '-2')
                                                Arşivlendi
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            @if (!empty($products['paired_pro'][$element['id']]))
                                                {{Carbon::parse($products['paired_pro'][$element['id']]->relationship->created_at)->format('d.m.Y')}}
                                            @else
                                                <span class="m-badge m-badge--primary m-badge--wide">İlişki Yok</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if (!empty($products['paired_pro'][$element['id']]))
                                                <a href="{{url('')}}" target="_blank" class="btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--custom m-btn--pill" title="Ürüne git">
                                                    <i class="fas fa-external-link-alt"></i>
                                                </a>
                                            @else
                                                @if(!empty($products['relationship'][$element['id']]))
                                                    <a href="{{url('parasut/products/take/'.$element['id'])}}" class="btn btn-primary m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--custom m-btn--pill" title="Eşleştir">
                                                        <i class="fas fa-sync"></i>
                                                    </a>
                                                @else
                                                    <a href="{{url('parasut/products/take/'.$element['id'])}}" class="btn btn-primary m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--custom m-btn--pill" title="İlişkilendir">
                                                        <i class="fas fa-sync"></i>
                                                    </a>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endif  
                            @endforeach

                        </tbody>

                    </table>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
    	$(document).ready(function(){
    	});
    </script>
@endsection
