<table>
    <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                Başlık
            </th>
            <th>
                Başlangıç
            </th>
            <th>
                Bitiş
            </th>
            <th>
                Kapasite
            </th>
            <th>
                Ücret
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($calendar as $data)
            <tr>
                <td>{{ $data->id }}</td>
                <td>{{ $data->variable->title }}</td>
                <td>{{ Carbon\Carbon::parse($data->start)->format('d/m/Y H:i') }}</td>
                <td>
                    @if(!is_null($data->end))
                        {{ Carbon\Carbon::parse($data->end)->format('d/m/Y H:i') }}
                    @endif
                </td>
                <td>
                    @if($data->capacity > 0)
                        {{ $data->capacity }}
                    @elseif($data->capacity == 0)
                        Sınırsız
                    @elseif($data->capacity == -1)
                        Kapalı Grup
                    @endif
                </td>
                <td>
                    @if($data->price == 0)
                        Ücretsiz
                    @else
                        {{ $data->price }}
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
