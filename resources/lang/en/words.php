<?php

return [

    // General Word
    
    //Form Element Word
    'signin' => 'Sign In',
    'resetpassword' => 'Reset Password',

    'email' => 'E-mail',
    'password' => 'Password',
    'confirmpassword' => 'Confirm Password',
    'rememberme' => 'Remember Me',
    'forgetpassword' => 'Forget Password ?',
    'anotheraccount' => 'Another account ?',
    
    'request' => 'Request',
    'cancel' => 'Cancel',

    'emailaddress' => 'E-Mail Address',
    'emailtoreset' => 'Enter your email to reset your password:',
    
    'sendresetlink' => 'Send Password Reset Link',

    //Menu Word
    'dashboard' => 'Dashboard',
    'organizationscheme' => 'Organization Scheme',
    'fullscheme' => 'Org. Scheme',
    'placement' => 'Placement',

    'department' => 'Department',
    'departmentlist' => 'Department List',
    'departmentview' => 'View Department',
    'departmentadd' => 'Add Department',
    'departmentedit' => 'Edit Department',
    'departmentdelete' => 'Delete Department',
    'departmentdetails' => 'Department Details',

    'thisdepartmentdelete' => 'Delete This Department',
    'thisdepartmentdeleted' => 'This Department is deleted !!!',
    'topdepartment' => 'Top Department',
    'cannotselectedtopdepartment' => 'Cannot Selected Top Department.',
    'addnewdepartment' => 'Add New Department',
    
    'documentloader' => 'Document Loader',
    'documentloadershort' => 'Doc. Loader',

    'owndepartmentmanager' => 'Own Department Manager',
    'owndepartmentmanagershort' => 'Own Dep. Man.',
    'owndepartmentmanagerdescription' => ' ',

    'selectdepartmentorposition' => 'Select Department or Position',
    'selectdepartmentorpositiondescription' => ' ',

    'position' => 'Position',
    'positionlist' => 'Position List',
    'positionview' => 'View Position',
    'positionadd' => 'Add Position',
    'positionedit' => 'Edit Position',
    'positiondelete' => 'Delete Position',
    'positiondetails' => 'Position Details',

    'thispositiondelete' => 'Delete This Position',
    'thispositiondeleted' => 'This Position is deleted !!!',
    'topposition' => 'Top Position',
    'cannotselectedtopposition' => 'Cannot Selected Top Position.',
    'addnewposition' => 'Add New Position',
    
    'myprocesses' => 'My Processes',
    'addnewprocess' => 'Add New Process',
    'processtype' => 'Process Type',
    'follow-upprocess' => 'Follow-up Process',

    'processdetails' => 'Process Details',
    'processmanagement' => 'Process Management',
    'processmanagementlist' => 'Process Management List',
    'viewprocess' => 'View Process',
    'addprocess' => 'Add Process',
    'editprocess' => 'Edit Process',
    'deleteprocess' => 'Delete Process',
    'deletethisprocess' => 'Delete This Process',

    'adddeleteprocesssteps' => 'Add/Delete Process Steps',
    'addprocessstep' => 'Add Process Step',
    'processsteplist' => 'Process Step List',
    'editprocesssteps' => 'Edit Process Steps',

    'selectstepdetail' => 'Select Step Detail',
    'processowner' => 'Process Owner',


    'formgenerator' => 'Form Generator',


    'formgroup' => 'Form Group',
    'formgroups' => 'Form Groups',
    'formgrouplist' => 'Form Group List',
    'formgroupdetails' => 'Form Group Details',

    'viewformgroup' => 'View Form Group',
    'addformgroup' => 'Add Form Group',
    'editformgroup' => 'Edit Form Group',
    'deleteformgroup' => 'Delete Form Group',

    'addnewformgroup' => 'Add New Form Group',
    'deletethisformgroup' => 'Delete This Form Group',


    'formelement' => 'Form Element',
    'formelements' => 'Form Elements',
    'formelementlist' => 'Form Element List',
    'formelementdetails' => 'Form Element Details',

    'viewformelement' => 'View Form Element',
    'addformelement' => 'Add Form Element',
    'editformelement' => 'Edit Form Element',
    'deleteformelement' => 'Delete Form Element',
    
    'addnewformelement' => 'Add New Form Element',
    'deletethisformelement' => 'Delete This Form Element',

    'addformelements' => 'Add Form Elements',

    'addnewplacement' => 'Add New Placement',
    'deleteplacement' => 'Delete Placement',


    'user' => 'User',
    'userlist' => 'User List',
    'viewuser' => 'View User',
    'adduser' => 'Add User',
    'edituser' => 'Edit User',
    'deleteuser' => 'Delete User',
    'deletethisuser' => 'Delete This User',
    'userdetails' => 'User Details',
    'addnewuser' => 'Add New User',


    'actions' => 'Actions',
    'description' => 'Description',
    'status' => 'Status',
    'date' => 'Date',
    'name' => 'Name',
    'fullname' => 'Full Name',
    'createdate' => 'Create Date',
    'active' => 'Active',
    'passive' => 'Passive',
    'detail' => 'Detail',

    'detailsandconfirmation' => 'Details And Confirmation',
    'recentactivities' => 'Recent Activities',

    'reasonmessage' => 'Reason Message',
    'confirm' => 'Confirm',
    'reject' => 'Reject',
    'send' => 'Send',
    'reset' => 'Reset',
    'adddocument' => 'Add Document',
    'edit' => 'Edit',
    'view' => 'View',
    'type' => 'Type',
    'close' => 'Close',
    'back' => 'Back',

    'deleterecord' => 'Delete Record',
    'deleting' => 'Siliyorsunuz...',
    'deletingmessage' => 'Bu işlem geri alınamaz ve ilişkili kayıtlarda soruna sebep olabilir.',

    'addrecordsavechanges' => 'Add Record / Save changes',
    'selectavalue' => 'Select a value',
    'recordid' => 'ID',




    //Special Word

];
