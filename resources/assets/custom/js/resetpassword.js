//== Class Definition
var SnippetLogin = function() {

    var login = $('#m_login');

    //== Private Functions
    var handleResetPasswordFormSubmit = function() {

        $('#m_reset_password_submit').click(function(e) {

            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            var inputs = $('#' + form.attr('id') + ' input');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                    password_confirmation: {
                        equalTo: "#password"
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            inputs.attr('readonly', true);
            form.submit();

        });
    }

    //== Public Functions
    return {
        // public functions
        init: function() {
            handleResetPasswordFormSubmit();
        }
    };
}();

//== Class Initialization
jQuery(document).ready(function() {
    SnippetLogin.init();
});