<?php

Route::redirect('/', '/home', 301);
Route::get('getusername/{email}' , 'ToolsController@getusername');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

Auth::routes();

Route::get('/', 'AdminController@index');
Route::get('/dashboard', 'AdminController@index');

Route::get('/test', 'MenuController@test');

// AJAX ROUTES
Route::post('/getMenu', 'AdminController@getMenuAjax');
Route::post('/getContent', 'AdminController@getContentAjax');
Route::post('/getTag', 'AdminController@getTagAjax');
Route::post('/getCategory', 'AdminController@getCategoryAjax');
Route::post('/getContentAddons', 'AdminController@getContentAddonsAjax');
Route::post('/getSlider', 'AdminController@getSliderAjax');
Route::post('/getPhotoGallery', 'AdminController@getPhotoGalleryAjax');
Route::post('/getCalendar', 'AdminController@getCalendarAjax');
Route::post('/getForm', 'AdminController@getFormAjax');
Route::post('/getFormData', 'AdminController@getFormDataAjax');
Route::post('/uploadFile', 'AdminController@uploadFile');
Route::post('/upload2File', 'AdminController@upload2File');
Route::post('/cropThumbnail', 'AdminController@cropThumbnail');
// AJAX ROUTES

Route::get('/profile', 'ProfileController@index');
Route::post('/changePassword', 'ProfileController@change_password');

/**/Route::get('/sitesettings', 'SiteSettingsController@index');
/**/Route::post('/sitesettings_save', 'SiteSettingsController@save');

/**/Route::get('/language', 'LanguageController@index');
/**/Route::post('/language_save', 'LanguageController@language_save');

/**/Route::get('/photogallery', 'PhotoGalleryController@list');

Route::group(['prefix' => '/menu'], function () {

	Route::get('/list', 'MenuController@list');
	Route::get('/add', 'MenuController@add_edit');
	Route::get('/edit/{id}', 'MenuController@add_edit');
	Route::get('/edit/{id}/{lang}', 'MenuController@add_edit');
	Route::get('/delete/{id}', 'MenuController@delete');
	Route::post('/save', 'MenuController@save');

	Route::group(['prefix' => '/content'], function () {
		
		Route::get('/{id}', 'MenuController@content');
		Route::get('/{id}/add', 'MenuController@content_add');
		Route::get('/{id}/add/{cid}', 'MenuController@content_add');
		Route::get('/{id}/edit/{cid}', 'MenuController@content_edit');
		Route::get('/{id}/edit/{cid}/{lang}', 'MenuController@content_edit');
		//Route::get('/{id}/galup/{cid}', 'MenuController@content_add_edit');
		Route::get('/{id}/delete/{cid}', 'MenuController@content_delete');
		Route::post('/save', 'MenuController@content_save');

		Route::get('/{id}/addexist', 'MenuController@content_addexist');
		Route::post('/saveexist', 'MenuController@content_save_exist');

		Route::group(['prefix' => '/{id}/edit/{cid}'], function () {
			Route::get('/photogallery_edit/{fid}/{lang}', 'MenuController@photogalleryitem');
			Route::get('/photogallery_delete/{fid}/{lang}', 'MenuController@photogalleryitem_delete');
			Route::post('/photogallery_save', 'MenuController@photogalleryitem_save');
		});

		Route::group(['prefix' => '/{id}/edit/{cid}'], function () {
			Route::get('/slide_edit/{fid}/{lang}', 'MenuController@slideitem');
			Route::get('/slide_delete/{fid}/{lang}', 'MenuController@slideitem_delete');
			Route::post('/slide_save', 'MenuController@slideitem_save');
		});

		Route::group(['prefix' => '/{id}/subcontent/{cid}'], function () {
			Route::get('/', 'MenuController@content');
			/*
			Route::get('/{id}/addons/{cid}/add', 'MenuController@addon_add_edit');
			Route::get('/{id}/addons/{cid}/edit/{aid}', 'MenuController@addon_add_edit');
			Route::get('/{id}/addons/{cid}/edit/{aid}/{lang}', 'MenuController@addon_add_edit');
			//Route::get('/{id}/addons/{cid}/galup/{aid}', 'MenuController@addon_add_edit');
			Route::get('/{id}/addons/{cid}/delete/{aid}', 'MenuController@addon_delete');
			Route::post('/{id}/addons/{cid}/save', 'MenuController@addon_save');
			*/
		});



		Route::get('/{id}/addons/{cid}', 'MenuController@addons');
		Route::get('/{id}/addons/{cid}/add', 'MenuController@addon_add_edit');
		Route::get('/{id}/addons/{cid}/edit/{aid}', 'MenuController@addon_add_edit');
		Route::get('/{id}/addons/{cid}/edit/{aid}/{lang}', 'MenuController@addon_add_edit');
		//Route::get('/{id}/addons/{cid}/galup/{aid}', 'MenuController@addon_add_edit');
		Route::get('/{id}/addons/{cid}/delete/{aid}', 'MenuController@addon_delete');
		Route::post('/{id}/addons/{cid}/save', 'MenuController@addon_save');


		Route::group(['prefix' => '/{id}/addons/{cid}/edit/{aid}'], function () {
			Route::get('/photogallery_edit/{fid}', 'MenuController@addon_photogalleryitem');
			Route::get('/photogallery_delete/{fid}', 'MenuController@addon_photogalleryitem_delete');
			Route::post('/photogallery_save', 'MenuController@addon_photogalleryitem_save');
		});

	});

	Route::group(['prefix' => '/stslider'], function () {
		Route::get('/{id}', 'MenuController@stslider');
		Route::get('/{id}/add', 'MenuController@stslider_add_edit');
		Route::get('/{id}/edit/{sid}', 'MenuController@stslider_add_edit');
		Route::get('/{id}/edit/{sid}/{lang}', 'MenuController@stslider_add_edit');
		Route::get('/{id}/delete/{sid}', 'MenuController@stslider_delete');
		Route::post('/save', 'MenuController@stslider_save');
	});

	Route::get('/stimage/{id}', 'MenuController@stimage');
});

Route::group(['prefix' => '/tag'], function () {
	Route::get('/', 'MenuController@tag');
	Route::get('/add', 'MenuController@tag_add_edit');
	Route::get('/edit/{tid}', 'MenuController@tag_add_edit');
	Route::get('/edit/{tid}/{lang}', 'MenuController@tag_add_edit');
	Route::get('/delete/{tid}', 'MenuController@tag_delete');
	Route::post('/save', 'MenuController@tag_save');
});

Route::group(['prefix' => '/category'], function () {
	Route::get('/', 'MenuController@category');
	Route::get('/add', 'MenuController@category_add_edit');
	Route::get('/edit/{cid}', 'MenuController@category_add_edit');
	Route::get('/edit/{cid}/{lang}', 'MenuController@category_add_edit');
	Route::get('/delete/{cid}', 'MenuController@category_delete');
	Route::post('/save', 'MenuController@category_save');
});

Route::group(['prefix' => '/calendar'], function () {
	
	Route::get('/', 'CalendarController@index');
	Route::get('/list', 'CalendarController@list');
	Route::get('/add', 'CalendarController@add_edit');
	Route::get('/edit/{id}', 'CalendarController@add_edit');
	Route::get('/edit/{id}/{lang}', 'CalendarController@add_edit');
	Route::get('/delete/{id}', 'CalendarController@delete');
	Route::post('/save', 'CalendarController@save');
	
	Route::get('/reservations', 'CalendarController@reservations');

});

Route::group(['prefix' => '/form'], function () {

	Route::get('/list', 'MenuController@form_list');
	Route::get('/detail/{formid}', 'MenuController@form_detail');
	Route::get('/detail/{formid}/sendArchive', 'MenuController@form_sendArchive');

	Route::get('content/0/add', 'MenuController@content_add');
	//Route::get('/{id}/edit/{cid}', 'MenuController@content_add_edit');
	//Route::get('/{id}/edit/{cid}/{lang}', 'MenuController@content_add_edit');

});

Route::post('/calendar/export', 'ExportController@calendarExport');
Route::post('/formdata/export', 'ExportController@formDataExport');

Route::group(['prefix' => '/parasut'], function () {
	Route::get('/info', 'ParasutController@info');
	Route::group(['prefix' => '/products'], function () {
		Route::get('/', 'ParasutController@products');
		Route::get('/take/{id}', 'ParasutController@product_take');
	});
	
	//Route::get('/account_tools/{tool_id}/{id}', 'ParasutController@account_tools');
	//Route::get('/customers', 'ParasutController@customers');
	//Route::get('/create-invoice/{id}', 'ParasutController@create_invoice');

});


Route::group(['prefix' => '/stock'], function() {

	Route::get('/barcodes', 'StockController@barcodes');
	Route::get('/cafe_barcodes', 'StockController@cafe_barcodes');
	
	Route::post('/make_barcodes', 'StockController@makeBarcodes');

	Route::get('/stocktaking', 'StockController@stocktaking');
	Route::get('/stocktakingdetail/{id}', 'StockController@stocktakingdetail');

	Route::get('/newstocktaking/{id}', 'StockController@newstocktaking');
	Route::get('/newstocktaking', 'StockController@newstocktaking');

	Route::get('/check_listen', 'StockController@checkListen');
	Route::post('/taking_save', 'StockController@takingSave');
	Route::post('/taking_ok', 'StockController@takingOk');
	Route::post('/taking_cancel', 'StockController@takingCancel');
	/* ----- */
	Route::get('/products', 'StockController@products');

	Route::get('/first_stock_count', 'StockController@first_stock_count');

	Route::get('/barcode', 'StockController@barcode');
	Route::post('/make_first_barcode', 'StockController@makeFirstBarcode');
	Route::post('/make_barcode', 'StockController@makeBarcode');

});
