<?php

return [

	'upload' => [

		'path' => '/upload',

        'imageType' => [

            'photo' => [
                'maxFiles' => 1,
                'uploadType' => 'contentimage',
                'thumbnail' => true,
                'type' => [
                    'default' => [
                        'title' => 'Seçiniz',
                        'aspectRatio' => 1,
                    ],
                    'afis' => [
                        'title' => 'Haber-Duyuru-Afiş',
                        'aspectRatio' => 0.71,
                    ],
                    'tamgenislik' => [
                        'title' => 'Tam Genişlik',
                        'aspectRatio' => 2.5,
                    ],
                    'serbest' => [
                        'title' => 'Serbest Seçim',
                        'aspectRatio' => 0,
                    ],
                ],
            ],

            'slide' => [
                'maxFiles' => 5,
                'uploadType' => 'slideimage',
                'thumbnail' => false,
                'type' => [
                    'default' => [
                        'title' => 'Seçiniz',
                        'aspectRatio' => 1,
                    ],
                    'tamgenislik' => [
                        'title' => 'Tam Genişlik',
                        'aspectRatio' => 2.5,
                    ],
                ],
            ],

            'photogallery' => [
                'maxFiles' => 5,
                'uploadType' => 'galleryimage',
                'thumbnail' => false,
                'type' => [
                    'default' => [
                        'title' => 'Seçiniz',
                        'aspectRatio' => 1,
                    ],
                    'serbest' => [
                        'title' => 'Serbest Seçim',
                        'aspectRatio' => 0,
                    ],
                ],
            ],

            'stimage' => [
                'maxFiles' => 1,
                'uploadType' => 'stimage',
                'thumbnail' => false,
                'type' => [
                    'default' => [
                        'title' => 'Seçiniz',
                        'aspectRatio' => 1,
                    ],
                    'ustfoto' => [
                        'title' => 'Sayfa Üstü Fotoğrafı',
                        'aspectRatio' => 7.85,
                    ],
                    'serbest' => [
                        'title' => 'Serbest Seçim',
                        'aspectRatio' => 0,
                    ],
                ],
            ],

            'stslider' => [
                'maxFiles' => 1,
                'uploadType' => 'stslider',
                'thumbnail' => false,
                'type' => [
                    'default' => [
                        'title' => 'Seçiniz',
                        'aspectRatio' => 1,
                    ],
                    'tamgenislik' => [
                        'title' => 'Tam Genişlik',
                        'aspectRatio' => 2.5,
                    ],
                    'serbest' => [
                        'title' => 'Serbest Seçim',
                        'aspectRatio' => 0,
                    ],
                ],
            ],

            'calendar' => [
                'maxFiles' => 1,
                'uploadType' => 'calendarimage',
                'thumbnail' => true,
                'type' => [
                    'default' => [
                        'title' => 'Seçiniz',
                        'aspectRatio' => 1,
                    ],
                    'afis' => [
                        'title' => 'Haber-Duyuru-Afiş',
                        'aspectRatio' => 0.71,
                    ],
                    'tamgenislik' => [
                        'title' => 'Tam Genişlik',
                        'aspectRatio' => 2.5,
                    ],
                    'serbest' => [
                        'title' => 'Serbest Seçim',
                        'aspectRatio' => 0,
                    ],
                ],
            ],
            
        ],

        
        'imageThumbnail' => [

            'thumbnail' => [
                'width' => 600,
                'height' => 0,
                'aspectRatio' => 1.5,
                'widen' => true,
                'heighten' => false,
                'crop' => true,
            ],

        ],

        'imageStandart' => [

            'xlarge' => [
                'width' => 2000,
                'height' => 0,
                'aspectRatio' => 0,
                'widen' => true,
                'heighten' => false,
                'crop' => false,
            ],

            'large' => [
                'width' => 1200,
                'height' => 0,
                'aspectRatio' => 0,
                'widen' => true,
                'heighten' => false,
                'crop' => false,
            ],

            'medium' => [
                'width' => 600,
                'height' => 0,
                'aspectRatio' => 0,
                'widen' => true,
                'heighten' => false,
                'crop' => false,
            ],
            
            'small' => [
                'width' => 300,
                'height' => 0,
                'aspectRatio' => 0,
                'widen' => true,
                'heighten' => false,
                'crop' => false,
            ],

            'thumbnail' => [
                'width' => 600,
                'height' => 0,
                'aspectRatio' => 0,
                'widen' => true,
                'heighten' => false,
                'crop' => true,
            ],
            
        ],

    ],

    'seperator' => [

        'type1' => [
            'name' => 'Sarı',
            'color' => 'f5eec2'
        ],

        'type2' => [
            'name' => 'Mavi',
            'color' => 'c1cff4'
        ],

        'type3' => [
            'name' => 'Yeşil',
            'color' => 'd2e9b5'
        ],
    
    ],

    'content' => [

        'photo' => [

            'props' => [

                /* Gösterim Türü */
                'fullwidth' => [
                    'value' => 'fullwidth',
                    'label' => 'Full Width',
                    'description' => 'Her şartta içinde bulunduğu alana tam sığar.',
                ],
                'responsive' => [
                    'value' => 'responsive',
                    'label' => 'Responsive',
                    'description' => 'Responsive davranır.',
                ],
                'fixed' => [
                    'value' => 'fixed',
                    'label' => 'Gerçek Boyut',
                    'description' => 'Photo nun gerçek boyutlarına her şartta uyum sağlar.',
                ],

            ],

        ],
    
    ],

    'menu_content' => [

        'menuitem' => [
            'group' => false,
            'text' => false,
            'photo' => false,
            'photogallery' => false,
            'link' => false,
            'slide' => false,
            'seperator' => false,
            'form' => false,
        ],  
        'content' => [
            'group' => false,
            'text' => true,
            'photo' => true,
            'photogallery' => true,
            'link' => false,
            'slide' => true,
            'seperator' => true,
            'form' => true,
        ],
        'list' => [
            'group' => true,
            'text' => false,
            'photo' => false,
            'photogallery' => false,
            'link' => false,
            'slide' => false,
            'seperator' => false,
            'form' => false,
        ],
        'photogallery' => [
            'group' => false,
            'text' => false,
            'photo' => false,
            'photogallery' => true,
            'link' => false,
            'slide' => false,
            'seperator' => false,
            'form' => false,
        ],
        'link' => [
            'group' => false,
            'text' => false,
            'photo' => false,
            'photogallery' => false,
            'link' => false,
            'slide' => false,
            'seperator' => false,
            'form' => false,
        ],
        'calendar' => [
            'group' => false,
            'text' => true,
            'photo' => true,
            'photogallery' => true,
            'link' => false,
            'slide' => false,
            'seperator' => false,
            'form' => true,
        ],
    
    ],

    'content_subcontent' => [

        'menuitem' => [
            'group' => false,
            'text' => false,
            'photo' => false,
            'photogallery' => false,
            'link' => false,
            'slide' => false,
            'seperator' => false,
            'form' => false,
        ],  
        'content' => [
            'group' => false,
            'text' => true,
            'photo' => true,
            'photogallery' => true,
            'link' => false,
            'slide' => true,
            'seperator' => true,
            'form' => true,
        ],
        'list' => [
            'group' => true,
            'text' => false,
            'photo' => false,
            'photogallery' => false,
            'link' => false,
            'slide' => false,
            'seperator' => false,
            'form' => false,
        ],
        'photogallery' => [
            'group' => false,
            'text' => false,
            'photo' => false,
            'photogallery' => true,
            'link' => false,
            'slide' => false,
            'seperator' => false,
            'form' => false,
        ],
        'link' => [
            'group' => false,
            'text' => false,
            'photo' => false,
            'photogallery' => false,
            'link' => false,
            'slide' => false,
            'seperator' => false,
            'form' => false,
        ],
        'calendar' => [
            'group' => false,
            'text' => true,
            'photo' => true,
            'photogallery' => true,
            'link' => false,
            'slide' => false,
            'seperator' => false,
            'form' => true,
        ],
    
    ],

];

/*
'xlarge' => 2400,
'large' => 1200,
'medium' => 600,
'small' => 300,

'slide' => 1200,
'thumbnail2x' => 600,
'thumbnail3x' => 400,
'thumbnail4x' => 300,
'thumbnail169' => 656,
'thumbnail32' => 600,
'calendar' => 731,
'stimage' => array('w' => 1100, 'h' => 140 ),
'stslider' => array('w' => 1920, 'h' => 850 ),
*/