<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarVariable extends Model
{
    protected $table = 'calendarvariable';
}
