<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'mshop_product';

    public function relationship(){
    	return $this->hasOne('App\Account_Products', 'proid', 'id');
    }
}
