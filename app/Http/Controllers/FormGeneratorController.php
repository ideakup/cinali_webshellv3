<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use App\Language;
use App\User;
use App\Menu;
use App\MenuVariable;
use App\Content;
use App\ContentVariable;
use App\Tag;
use App\TagVariable;
use App\Category;
use App\CategoryVariable;
use App\Form;
use App\FormVariable;

use App\MenuHasContent;
use App\ContentHasTag;
use App\ContentHasCategory;

use App\ContentPhotoGalleryVariable;
use App\Slider;
use App\SliderVariable;

use Carbon\Carbon;
use App;

class FormGeneratorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list()
    {
    	$langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        return view('formgenerator.list', array('langs' => $langs));
    }

    public function add_edit($id = null, $lang = null)
    {
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
    	if(is_null($id)){
    		$form = new Content();
    	}else{
    		$form = Content::find($id);
    	}
    	return view('formgenerator.crud', array('langs' => $langs, 'form' => $form));
    }

    public function save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if($request->crud == 'add'){
            $rules = array(
                'title' => 'required|max:255',
                'order' => 'numeric|min:1|max:100000'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }
            $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
            
            $form = new Content();
            $form->type = 'form';
            $form->order = $request->order;
            $form->status = ($request->status == 'active') ? 'active' : 'passive';
            $form->save();

            foreach ($languages  as $language) {
                $formVariable = new ContentVariable();
                $formVariable->content_id = $form->id;
                $formVariable->lang_code = $language->code;
                $formVariable->title = $request->title;
                $formVariable->save();
            }
            $text = 'Başarıyla Eklendi...';

        }else if($request->crud == 'edit'){

            if (is_null($request->lang)) {
                
                $rules = array(
                    'order' => 'numeric|min:1|max:100000'
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
                }

                $form = Content::find($request->form_id);
                $form->order = $request->order;
                $form->status = ($request->status == 'active') ? 'active' : 'passive';
                $form->save();

                $text = 'Başarıyla Düzenlendi...';

            }else{

                $rules = array(
                    'title' => 'required|max:255',
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
                }

                $formVariable = Content::find($request->form_id)->variableLang($request->lang);
                if (!isset($formVariable)) {
                    $formVariable = new ContentVariable();
                    $formVariable->content_id = $request->form_id;
                    $formVariable->lang_code = $request->lang;
                }
                $formVariable->title = $request->title;
                $formVariable->content = $request->formdata;
                $formVariable->save();

                $text = 'Başarıyla Düzenlendi...';
            }

        }else if($request->crud == 'delete'){

        }

        return redirect('formgenerator/list')->with('message', array('text' => $text, 'status' => 'success'));
    }

}
