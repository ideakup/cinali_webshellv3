<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exports\FormDataExport;
use App\Exports\CalendarExport;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{

    public function formDataExport(Request $request) 
    {
    	//dd($request->input());
        return Excel::download(new FormDataExport($request->input()), 'formData.xlsx');
    }

    public function calendarExport(Request $request) 
    {
    	//dd($request->input());
        return Excel::download(new CalendarExport($request->input()), 'takvim.xlsx');
    }

}


