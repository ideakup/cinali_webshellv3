<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use App\Language;
use App\User;
use App\Menu;
use App\MenuVariable;
use App\Content;
use App\ContentVariable;
use App\Tag;
use App\TagVariable;
use App\Category;
use App\CategoryVariable;

use App\MenuHasContent;
use App\ContentHasTag;
use App\ContentHasCategory;

use App\ContentPhotoGalleryVariable;
use App\Slider;
use App\SliderVariable;

use App\Calendar;
use App\CalendarVariable;
use App\CalendarHasTag;
use App\CalendarHasCategory;
//use App\Reservations;
use Carbon\Carbon;
use App;

class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $calendar = Calendar::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        return view('calendar.calendar', array('calendar' => $calendar));
    }

    public function list()
    {
    	$langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        return view('calendar.list', array('langs' => $langs));
    }

    public function add_edit($id = null, $lang = null)
    {
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $categories = Category::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $forms = Content::where('type', 'form')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

    	if(is_null($id)){
    		$calendar = new Calendar;
    	}else{
    		$calendar = Calendar::find($id);
    	}
    	return view('calendar.crud', array('langs' => $langs, 'calendar' => $calendar, 'forms' => $forms, 'tags' => $tags, 'categories' => $categories));
    }

    public function delete($id)
    {
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $tags = Tag::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $categories = Category::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $forms = Content::where('type', 'form')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

        if(is_null($id)){
            $calendar = new Calendar;
        }else{
            $calendar = Calendar::find($id);
        }
        return view('calendar.crud', array('langs' => $langs, 'calendar' => $calendar, 'forms' => $forms, 'tags' => $tags, 'categories' => $categories));
    }

    public function save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if($request->crud == 'add'){
            
            $rules = array(
                'title' => 'required'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }
            $languages = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

            $calendar = new Calendar();
            $calendar->owner = $request->owner;
            $calendar->type = $request->type;

            $calendar->start = Carbon::createFromFormat('d/m/Y H:i', $request->startDate.' '.$request->startTime)->toDateTimeString();

            if (is_null($request->endDate)) {
                $calendar->end = null;
            }else{
                $calendar->end = Carbon::createFromFormat('d/m/Y H:i', $request->endDate.' '.$request->endTime)->toDateTimeString();
            }
            
            if($request->capacity_type == 'sinirli'){
                $calendar->capacity = $request->capacity;
            }elseif($request->capacity_type == 'limitsiz'){
                $calendar->capacity = 0;
            }elseif($request->capacity_type == 'kapali'){
                $calendar->capacity = -1;
            }

            if($request->price_type == 'ucretli'){
                $price = str_replace(' ', '', $request->price);
                $price = str_replace('₺', '', $price);
                $price = str_replace('.', '', $price);
                $price = str_replace(',', '.', $price);
                $calendar->price = $price;
            }elseif($request->price_type == 'ucretsiz'){
                $calendar->price = 0;
            }

            if ($request->reservationForm != 'null') {
                $calendar->form_id = $request->reservationForm;
            }
            
            $calendar->order = $request->order;
            $calendar->status = ($request->status == 'active') ? 'active' : 'passive';
            $calendar->save();

            foreach ($languages as $language) {
                $calendarVariable = new CalendarVariable();
                $calendarVariable->calendar_id = $calendar->id;
                $calendarVariable->lang_code = $language->code;
                $calendarVariable->title = $request->title;
                $calendarVariable->save();
            }

            $text = 'Başarıyla Eklendi...';

        }else if($request->crud == 'edit'){

            if (is_null($request->lang)) {
                //dd($request->input());
                $calendar = Calendar::where('id', $request->calendar_id)->first();
                $calendar->owner = $request->owner;
                $calendar->type = $request->type;
                $calendar->start = Carbon::createFromFormat('d/m/Y H:i', $request->startDate.' '.$request->startTime)->toDateTimeString();
                if (is_null($request->endDate)) {
                    $calendar->end = null;
                }else{
                    $calendar->end = Carbon::createFromFormat('d/m/Y H:i', $request->endDate.' '.$request->endTime)->toDateTimeString();
                }

                if($request->capacity_type == 'sinirli'){
                    $calendar->capacity = $request->capacity;
                }elseif($request->capacity_type == 'limitsiz'){
                    $calendar->capacity = 0;
                }elseif($request->capacity_type == 'kapali'){
                    $calendar->capacity = -1;
                }

                if($request->price_type == 'ucretli'){
                    $price = str_replace(' ', '', $request->price);
                    $price = str_replace('₺', '', $price);
                    $price = str_replace('.', '', $price);
                    $price = str_replace(',', '.', $price);
                    $calendar->price = $price;
                }elseif($request->price_type == 'ucretsiz'){
                    $calendar->price = 0;
                }

                if ($request->reservationForm != 'null') {
                    $calendar->form_id = $request->reservationForm;
                }
               
                $calendar->order = $request->order;
                $calendar->status = ($request->status == 'active') ? 'active' : 'passive';
                $calendar->save();


                $tagItems = CalendarHasTag::where('calendar_id', $request->calendar_id)->delete();
                if (!is_null($request->tag)) {
                    foreach ($request->tag as $tt) {
                        $tagItem = new CalendarHasTag();
                        $tagItem->calendar_id = $request->calendar_id;
                        $tagItem->tag_id = $tt;
                        $tagItem->save();
                    }
                }

                $categoryItems = CalendarHasCategory::where('calendar_id', $request->calendar_id)->delete();
                if (!is_null($request->category)) {
                    foreach ($request->category as $cc) {
                        $categoryItem = new CalendarHasCategory();
                        $categoryItem->calendar_id = $request->calendar_id;
                        $categoryItem->category_id = $cc;
                        $categoryItem->save();
                    }
                }

                $text = 'Başarıyla Düzenlendi...';

            }else{
                
                $rules = array(
                    'title' => 'required'
                );

                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                   return \Redirect::back()->withErrors($validator)->withInput();
                }

                $calendarVariable = Calendar::find($request->calendar_id)->variableLang($request->lang);
                $calendarVariable->title = $request->title;
                $calendarVariable->content = $request->content;
                $calendarVariable->address = $request->address;
                $calendarVariable->nocapmessage = $request->nocapmessage;
                $calendarVariable->save();

            }

        }else if($request->crud == 'delete'){
                $calendar = Calendar::find($request->calendar_id);
                $calendar->deleted = 'yes';
                $calendar->status = 'passive';
                $calendar->save();
                $text = 'Başarıyla Silindi...';
        }

        return redirect('calendar/list')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function reservations(){
    	$data = Reservations::all();
    	return view('events.reservations', array('data' => $data));
    }
}
