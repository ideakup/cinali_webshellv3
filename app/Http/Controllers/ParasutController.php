<?php

namespace App\Http\Controllers;

use Auth;
use Redirect;
use Validator;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\User;
use App\Products;
use App\Account_Products;
use App\Account_Orders;
use App\Account_Info;

use App\CustomLibrary\Parasut\Client;
use Carbon\Carbon;

class ParasutController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
		//PARAŞÜT ENTEGRASYON
		//Client ID: 68df3988fe806516b10f23e932547f4bb8514b746d63e0f0c5c33b17b4c0ce4f
		//Client Secret: 5bc141dcaace32f2be74da4ae15b5418eaafc384f2fe8910468d059e8becc7c7
		//Callback urls: urn:ietf:wg:oauth:2.0:oob
        $this->config = [
		    'client_id'     => '68df3988fe806516b10f23e932547f4bb8514b746d63e0f0c5c33b17b4c0ce4f',
		    'client_secret' => '5bc141dcaace32f2be74da4ae15b5418eaafc384f2fe8910468d059e8becc7c7',
		    'username'      => 'parasutapi@cinaliyonetim.com',
		    'password'      => 'cin.1234.ali',
		    'company_id'    => '201695',
		    'grant_type'    => 'password',
		    'redirect_uri'  => 'urn:ietf:wg:oauth:2.0:oob'
		];
    }

	public function create_invoice($id)
	{
		$parasut = new Client($this->config);
		$parasut->authorize();

		//dd($parasut->call('sales_invoices/7463445')); //Fatura görüntüleme
		//$parasut->call('sales_invoices/7462704', null, 'DELETE');dd('silindi'); //fatura silme

		$invoice_series = Account_Orders::select('invoice_series')->orderBy('invoice_id', 'desc')->first()->invoice_series;
		$invoice_id = Account_Orders::select('invoice_id')->orderBy('invoice_id', 'desc')->first()->invoice_id + 1;

		$order_base = DB::table('mshop_order_base')->find($id);
		$order_pro = DB::table('mshop_order_base_product')->where('baseid', $id)->get();
		$order_address = DB::table('mshop_order_base_address')->where('baseid', $id)->first();
		$order_service_delivery = DB::table('mshop_order_base_service')->where('baseid', $id)->where('type', 'delivery')->first();
		$order_service_payment = DB::table('mshop_order_base_service')->where('baseid', $id)->where('type', 'payment')->first();

		$account_id = Account_Info::where('category', 'cash_bank')->where('type', $order_service_payment->code)->where('status', '1')->select('account_id')->first();
		if (empty($account_id)) {
			return Redirect::to('parasut/info')->with('message', array('text' => 'Satışa uygun ilişkili bir banka hesabı bulunmamaktadır.', 'status' => 'danger'));
		}else{
			$account_id = $account_id->account_id;
		}				

		$user = User::find($order_base->customerid);

		$data_arr = array();
		foreach ($order_pro as $value) { // ürün veya ürünler ayrıntıları
			if(count(Account_Products::where('proid', $value->prodid)->get()) > 0){
				$data_arr[] = [
					"type" => "sales_invoice_details",
					"attributes" => [
						"quantity" => $value->quantity,
						"unit_price" => $value->price / (1 + ($value->taxrate / 100)),
						"vat_rate" => $value->taxrate,
					],
					"relationships" => [
						"product" => [
							"data" => [
								"id" => Account_Products::where('proid', $value->prodid)->get()->first()->account_pro_id,
								"type" => "products"
							]
						]
					]
				];
			}else{
				return Redirect::to('parasut/products')->with('message', array('text' => 'Ürün paraşüt sisteminde ilişkili değil. Lütfen ürünü, ilişkilendirin.', 'status' => 'danger'));
			}
		}

		if($order_service_delivery->price > 0){ // kargo ücreti
			$data_arr[] = [
				"type" => "sales_invoice_details",
				"attributes" => [
					"quantity" => 1,
					"unit_price" => $order_service_delivery->price / (1 + ($order_service_delivery->taxrate / 100)),
					"vat_rate" => $order_service_delivery->taxrate,
				],
				"relationships" => [
					"product" => [
						"data" => [
							"id" => 6754815, //Kargo ücreti olan ürünün idsi
							"type" => "products"
						]
					]
				]
			];
		}

		if(is_null($user->account_id)){ // paraşüte kayıtlı değilse yeni user kaydetme
			$newcontact = [ 
				"data" => [
					"type" => "contacts",
					"attributes" => [
						"email" => $user->email,
						"name" => $user->firstname.' '.$user->lastname,
						"short_name" => $user->name,
						"contact_type" => empty($user->company) ? 'person' : 'company',	
				        "tax_number" => null,
				        "tax_office" => null,
				        "archived" => false,
				        "iban" => null,
				        "city" => $user->city,
				        "district" => null,
				        "address" => $user->address1.' '.$user->address2,
				        "phone" => $user->telephone,
				        "fax" => $user->telefax,
						"is_abroad" => false,
						"account_type" => "customer"			        
					],
				]
			];
			$parasut_contact = $parasut->call('contacts', $newcontact, 'POST');
			$user->account_id = $parasut_contact['data']['id'];
			$user->save();
		}

		$newsales_invoices = [ // fatura ayrıntıları
			"data" => [
    			"type" => "sales_invoices",
    			"attributes" => [
			        "item_type" => "invoice",
					"description" => $user->name,
					"issue_date" => Carbon::now()->format("Y-m-d"),
			        "invoice_id" => $invoice_id,
			        "invoice_series" => $invoice_series,
					"due_date" => Carbon::parse($order_base->ctime)->format("Y-m-d"),
			        "invoice_discount_type" => "percentage",
			        "invoice_discount" => 0,
			        "order_date" => null,
			        "billing_address" => $order_address->address1.' '.$order_address->address2,
					"billing_phone" => $order_address->telephone,
					"billing_fax" => $order_address->telefax,
					"tax_office" => "string",
					"tax_number" => $order_address->vatid,
					"city" => $order_address->city,
					"is_abroad" => false,
					"order_no" => $id,
					"order_date" => Carbon::parse($order_base->ctime)->format("Y-m-d"),
					"shipment_addres" =>  $order_address->address1.' '.$order_address->address2
    			],
    			"relationships" => [
    				"details" => [
    					"data" => $data_arr
    				],    				
					"contact" => [
						"data" => [
							"id" => $user->account_id,
							"type" => "contacts"
						]
					]
				]
    		]
		];

		$parasut_order = $parasut->call('sales_invoices', $newsales_invoices, 'POST'); // fatura post

		$invoices_pay = [ // ödeme ayrıntıları
			"data" => [
    			"type" => "payments",
    			"attributes" => [
    				"description" => $user->name.' ödemesi',
					"account_id" => $account_id, // cin ali dükkan kasası
					"date" => Carbon::parse($order_base->ctime)->format("Y-m-d"),
					"amount" => $parasut_order['data']['attributes']['net_total']
    			]
    		]
		];

		$parasut->call('sales_invoices/'.$parasut_order['data']['id'].'/payments', $invoices_pay, 'POST'); // ödeme post

		$order = new Account_Orders();
		$order->order_base_id = $id;
		$order->customerid = $order_base->customerid;
		$order->account_order_id = $parasut_order['data']['id'];
		$order->invoice_series = $parasut_order['data']['attributes']['invoice_series'];
		$order->invoice_id = $parasut_order['data']['attributes']['invoice_id'];
		$order->save();

		$data = 
		'
			<html>
				<head>
					<script>
						function loaded(){window.setTimeout(CloseMe, 4000);}
						function CloseMe() {window.close();}
					</script>
				</head>
				<body onLoad="loaded()">
					<h3>Bu siparişe ait fatura, başarıyla paraşüte gönderildi</h3>
					<small>Bu sayfa, 4 saniye sonra ototmatik olarak kapanacaktır.</small>
				</body>
			</html>
		';

		return $data;
	}

    public function info()
    {
    	
    	$parasut = new Client($this->config);
		$parasut->authorize();

    	$me = $parasut->me()['data']; //hesap bilgileri
    	$accounts = $parasut->call('accounts')['data']; //kasa ve banka bilgileri
    	$accounts_relationship = array();
    	foreach (Account_Info::where('category', 'cash_bank')->get() as $value) {
    		foreach ($accounts as $account) {
    			if($value->account_id == $account['id']){
    				$accounts_relationship[$account['id']] = $value;
    			}
    		}
    	}

    	return view('parasut.info', array('me' => $me, 'accounts' => $accounts, 'accounts_relationship' => $accounts_relationship));
    }

    public function account_tools($tool_id, $id)
    {

    	$parasut = new Client($this->config);
		$parasut->authorize();

		if($tool_id == 'take'){
			$account = $parasut->call('accounts/'.$id)['data'];

			$account_info = new Account_Info();
			$account_info->category = 'cash_bank';
			$account_info->type = 'havale';
			$account_info->account_id = $account['id'];
			$account_info->name = $account['attributes']['name'];
			$account_info->status = '1';
			$account_info->save();
			$text = 'Kasa veya banka hesabı ilişkilendirildi.';
		}else if($tool_id == 'change'){
			$account_info = Account_Info::find($id);
			$account_info->type = ($account_info->type == 'kredi-karti') ? 'havale' : 'kredi-karti';
			$account_info->save();
			$text = 'Kasa veya banka hesabı tipi değiştirildi.';
		}else if($tool_id == 'remove'){
			$account_info = Account_Info::find($id);
			$account_info->delete();			
			$text = 'Kasa veya banka hesabı ilişkisi silindi.';
		}
    	return Redirect::to('parasut/info')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function customers()
    {

    	$parasut = new Client($this->config);
		$parasut->authorize();

		$users = User::orderBy('account_id', 'desc')->get();
		$relationship = array();

    	$data = $parasut->call('contacts')['data']; // müşteri liste ve bilgileri

    	//eşitlenmiş olmayan müşterilerin arasından e-posta adresilerine göre ilişkili olanları bulma
		foreach (User::where('account_id', null)->get() as $internal) {
			foreach ($data as $external) {
				if($external['attributes']['email'] == $internal->email){
					$relationship[$internal->id][1] = $internal;
					$relationship[$internal->id][2] = $external;
				}
			}
		}

    	return view('parasut.customers', array('users' => $users, 'data' => $data, 'relationship' => $relationship));
    }

    public function products(){
    	
    	$parasut = new Client($this->config);
		$parasut->authorize();

		$parasut_products = array();
		$total_pages = 1;

    	$data = $parasut->call('products', [
            'page[number]' => 1,
            'page[size]' => 25,
        ]);
        
    	foreach ($data['data'] as $pprod) {
    		if ($pprod['attributes']['inventory_tracking'] && !is_null($pprod['attributes']['barcode'])) {
    			$parasut_products[] = $pprod;
    		}
    	}

    	$total_pages = $data['meta']['total_pages'];

    	if($total_pages > 1){
	    	for ($i=2; $i <= $total_pages; $i++) { 

	    		$data = $parasut->call('products', [
		            'page[number]' => $i,
		            'page[size]' => 25,
		        ]);
	    		foreach ($data['data'] as $pprod) {
	    			if ($pprod['attributes']['inventory_tracking'] && !is_null($pprod['attributes']['barcode'])) {
			    		$parasut_products[] = $pprod;
			    	}
		    	}
		    	
	    	}
    	}

    	/*
    	foreach ($parasut_products as $key) {
    		dump($key);
    	}
    	die;
    	*/

    	$products = array();
    	foreach (Products::where("typeid", 1)->get() as $product) {
    		$cnt = 0;
    		foreach ($parasut_products as $value) {

    			if (!empty($product->relationship->account_pro_id) && $value['id'] == $product->relationship->account_pro_id) {
    				$products['paired'][] = $value;
    				$products['paired_pro'][$value['id']] = $product;
    				$cnt = 1;
    			}else if($value['attributes']['code'] == $product->code){
    				$products['relationship'][$value['id']] = true;
    				$cnt = 1;
    			}
    		}

    		if($cnt == 0){
    			$products['cannot'][] = $product;
    		}

    	}

    	return view('parasut.products', array('products' => $products, 'data' => $parasut_products));
    }

    public function product_take($id)
    {
    	$parasut = new Client($this->config);
		$parasut->authorize();

    	$data = $parasut->call('products/'.$id)['data'];
    	//dd($data);

    	$has_pro = Products::where('code', $data['attributes']['code'])->first();

    	if(is_null($has_pro)){
	    	$new_pro = new Products();
	    	$new_pro->typeid = 1;
	    	$new_pro->siteid = 1;
	    	$new_pro->code = $data['attributes']['code'];
	    	$new_pro->label = $data['attributes']['name'];
	    	$new_pro->config = '[]';
	    	$new_pro->status = 0;
	    	$new_pro->mtime = Carbon::parse($data['attributes']['updated_at']);
	    	$new_pro->ctime = Carbon::parse($data['attributes']['created_at']);
	    	$new_pro->editor = 'test@test.com';
	    	$new_pro->target = '';
	        $new_pro->timestamps = false;
	    	$new_pro->save();
    		$text = 'Paraşüt ürünü, dükkan ürünlerine eklendi.';
    	}else{
    		$new_pro = $has_pro;
    		$new_pro->label = $data['attributes']['name'];
	    	$new_pro->status = 0;
	    	$new_pro->mtime = Carbon::parse($data['attributes']['updated_at']);
	        $new_pro->timestamps = false;
	    	$new_pro->save();
    		$text = 'Paraşüt ürünü, dükkan eşitlendi.';
		}

    	if(is_null(DB::table('mshop_stock')->where('productcode', $new_pro->code)->first())){
    		DB::table('mshop_stock')->insert([
	    		'typeid' => 1,
	    		'siteid' => 1,
	    		'productcode' => $new_pro->code,
	    		'stocklevel' => intval($data['attributes']['stock_count']),
	    		'mtime' => $new_pro->mtime,
				'ctime' => $new_pro->ctime,
				'editor' => $new_pro->editor
	    	]);
    	}else{
    		DB::table('mshop_stock')->where('productcode', $new_pro->code)->update([
	    		'stocklevel' => intval($data['attributes']['stock_count']),
	    		'mtime' => $new_pro->mtime,
	    	]);
    	}

    	$list_price = $data['attributes']['list_price'] * (1 + ($data['attributes']['vat_rate'] / 100));  

    	$indexprice = DB::table('mshop_index_price')->where('prodid', $new_pro->id)->select('priceid')->first();
    	if(is_null($indexprice)){
    		$priceid = DB::table('mshop_price')->insertGetId([    		
	    		'typeid' => 3,
	    		'siteid' => 1,
	    		'domain' => 'product',
	    		'label' => $new_pro->label.' :: 1 ~ '.$list_price.' TRY',
	    		'currencyid' => 'TRY',
	    		'quantity' => 1,
	    		'value' => $list_price,
	    		'costs' => '0.00',
	    		'rebate' => '0.00',
	    		'taxrate' => $data['attributes']['vat_rate'],
	    		'status' => 1,
	    		'mtime' => $new_pro->mtime,
				'ctime' => $new_pro->ctime,
				'editor' => $new_pro->editor    		
	     	]);

	    	DB::table('mshop_index_price')->insert([
	    		'prodid' => $new_pro->id,
	    		'siteid' => 1,
	    		'priceid' => $priceid,
	    		'listtype' => 'default',
	    		'type' => 'default',
	    		'currencyid' => 'TRY',
	    		'value' => $list_price,
	    		'costs' => '0.00',
	    		'rebate' => '0.00',
	    		'taxrate' => $data['attributes']['vat_rate'],
	    		'quantity' => 1,
	    		'mtime' => $new_pro->mtime,
				'ctime' => $new_pro->ctime,
				'editor' => $new_pro->editor
	    	]);

			DB::table('mshop_product_list')->insert([
			    'parentid' => $new_pro->id,
			    'typeid' => 10,
			    'siteid' => 1,
			    'domain' => 'price',
			    'refid' => $priceid,
			    'config' => '[]',
			    'pos' => 0,
			    'status' => 1,
	    		'mtime' => $new_pro->mtime,
				'ctime' => $new_pro->ctime,
				'editor' => $new_pro->editor 
			]);
    	}else{ 	
    		DB::table('mshop_index_price')->where('prodid', $new_pro->id)->update([
	    		'value' => $list_price,
	    		'taxrate' => $data['attributes']['vat_rate'],
	    		'mtime' => $new_pro->mtime,
	    	]);

	    	DB::table('mshop_price')->where('id', $indexprice->priceid)->update([
	    		'label' => $new_pro->label.' :: 1 ~ '.$list_price.' TRY',
	    		'value' => $list_price,
	    		'taxrate' => $data['attributes']['vat_rate'],
	    		'mtime' => $new_pro->mtime,
	    	]);
    	}   

    	if(count(Account_Products::where('proid', $new_pro->id)->get()) == 0){
    		$account_pro = new Account_Products();
			$account_pro->proid = $new_pro->id;
			$account_pro->procode = $new_pro->code;
			$account_pro->account_pro_id = $data['id'];
			$account_pro->save();
    	}			

    	return Redirect::to('parasut/products')->with('message', array('text' => $text, 'status' => 'success'));
    }

}
