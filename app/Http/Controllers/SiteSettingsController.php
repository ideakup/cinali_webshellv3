<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SiteSettings;

class SiteSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$sitesettings = SiteSettings::orderBy('order', 'asc')->get();
        return view('sitesettings', array('sitesettings' => $sitesettings));
    }

	public function save(Request $request)
    {	
    	foreach ($request->input() as $key => $value) {
    		$sitesettings = SiteSettings::where('slug', $key)->first();
    		if (!is_null($sitesettings)) {
    			$sitesettings->value = $value;
    			$sitesettings->save();
    		}
    	}
    	$text = 'Başarıyla Kaydedildi...';
		return redirect('sitesettings')->with('message', array('text' => $text, 'status' => 'success'));
    }
    
}
