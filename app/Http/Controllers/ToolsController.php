<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class ToolsController extends Controller
{
    
    public function getusername($email = null){
        if(!is_null($email)){
            $user = User::where('email', $email)->select('name', 'avatar')->first();
        }
        if(empty($user)){
            return $user;  
        }
        return array('name' => $user->name, 'avatar' => $user->avatar);        
    }

}