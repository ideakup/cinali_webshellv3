<?php

namespace App\Http\Controllers;

use App;
use Config;
use Auth;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\UploadRequest;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Settings;
use App\Stock_Tracking;
use App\Stock_Taking;
use App\Products;

use App\KafeProduct;
use App\KafeOrder;
use App\KafeOrderDetails;

use DNS1D;
use DNS2D;

use App\CustomLibrary\Parasut\Client;
use Carbon\Carbon;

class StockController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->barcode_size = Settings::where('department', 'stock')->where('name', 'barcode_size')->first();
		//PARAŞÜT ENTEGRASYON
        $this->config = [
            'client_id'     => '68df3988fe806516b10f23e932547f4bb8514b746d63e0f0c5c33b17b4c0ce4f',
            'client_secret' => '5bc141dcaace32f2be74da4ae15b5418eaafc384f2fe8910468d059e8becc7c7',
            'username'      => 'parasutapi@cinaliyonetim.com',
            'password'      => 'cin.1234.ali',
            'company_id'    => '201695',
            'grant_type'    => 'password',
            'redirect_uri'  => 'urn:ietf:wg:oauth:2.0:oob'
        ];
    }

    public function getParasutProductsAll()
    {
        $parasut = new Client($this->config);
        $parasut->authorize();

        $parasut_products = array();
        $total_pages = 1;

        $data = $parasut->call('products', [
            'page[number]' => 1,
            'page[size]' => 25,
        ]);

        foreach ($data['data'] as $pprod) {
            if($pprod['attributes']['inventory_tracking']){
                $parasut_products[] = $pprod;
            }
        }

        $total_pages = $data['meta']['total_pages'];

        if($total_pages > 1){
            for ($i=2; $i <= $total_pages; $i++) { 

                $data = $parasut->call('products', [
                    'page[number]' => $i,
                    'page[size]' => 25,
                ]);
                foreach ($data['data'] as $pprod) {
                    if($pprod['attributes']['inventory_tracking']){
                        $parasut_products[] = $pprod;
                    }
                }
            }
        }

        return $parasut_products;
    }

    public function barcodes()
    {
        $parasut_products = $this->getParasutProductsAll();

        for ($i=0; $i < count($parasut_products); $i++) {
            
            $parasut_products[$i]['barcodes'] = null;
            $prod = Products::where('code', $parasut_products[$i]['attributes']['barcode'])->first();

            if ($parasut_products[$i]['attributes']['stock_count'] < 0){
                // Paraşüt kayıtlarında stok 0'dan küçük...
                $parasut_products[$i]['status'] = 'error';
                $parasut_products[$i]['status_text'] = "Paraşüt kayıtlarında stok 0'dan küçük...";
                $parasut_products[$i]['barcode'] = null;
                $parasut_products[$i]['barcodeimg'] = null;
            }else if(empty($prod)){
                // Ürün Dükkanda mevcut değil...
                $parasut_products[$i]['status'] = 'error';
                $parasut_products[$i]['status_text'] = "Ürün Dükkanda mevcut değil...";
                $parasut_products[$i]['barcode'] = null;
                $parasut_products[$i]['barcodeimg'] = null;
            }else{
                // Ürün Dükkanda mevcut...
                if (is_null($prod->relationship)) {
                    // Ürün ilişkilendirilmemiş...
                    $parasut_products[$i]['status'] = 'relationship_null';
                    $parasut_products[$i]['status_text'] = "Ürünü İlişkilendirin...";
                    $parasut_products[$i]['barcode'] = null;
                    $parasut_products[$i]['barcodeimg'] = null;
                }else{
                    // Ürün ilişkilendirilmiş...
                    $parasut_products[$i]['status'] = 'relationship_success';
                    $parasut_products[$i]['status_text'] = "Barkod Üretimi için hazır...";
                    $parasut_products[$i]['barcode'] = $parasut_products[$i]['attributes']['barcode'];

                    $parasut_products[$i]['barcodeimg'] = is_null($parasut_products[$i]['attributes']['barcode']) ? null : '<img src="data:image/png;base64,'.DNS1D::getBarcodePNG($parasut_products[$i]['attributes']['barcode'], "C128", $this->barcode_size->val_str_1*2, $this->barcode_size->val_str_2*2).'" alt="barcode" />';

                    
                    $parasut_products[$i]['relationship'] = $prod->relationship;
                    $stock_tracks = array();
                    if (Stock_Tracking::where('stock_id', $parasut_products[$i]['id'])->where('disband', '0')->count() > 0) { 
                        foreach (Stock_Tracking::where('stock_id', $parasut_products[$i]['id'])->where('disband', '0')->get() as $key => $value) {
                            $stock_tracks[] = [
                                'procode' => $value->procode,
                                'group_count' => $value->group_count,
                                'barcode' => $value->barcode,
                                'img' => '<img src="data:image/png;base64,'.DNS1D::getBarcodePNG($value->barcode, "C128", $this->barcode_size->val_str_1*2, $this->barcode_size->val_str_2*2).'" alt="barcode" />',
                            ];                          
                        }
                        $parasut_products[$i]['barcodes'] = $stock_tracks;
                        //dump($stock_tracks);
                    }
                }
            }
        }

        foreach ($parasut_products as $key => $row) {
            $barcode[$key]  = $row['barcode'];
        }

        array_multisort($barcode, SORT_ASC, $parasut_products);

        return view('stock.barcodes', array('parasut_products' => $parasut_products));
    }

    public function cafe_barcodes()
    {
        $kafe_product = KafeProduct::orderBy('barcode')->get();
        //dd($kafe_product);

        return view('stock.cafe_barcodes', array('kafe_product' => $kafe_product));
        //$parasut_products = $this->getParasutProductsAll();

        /*
        for ($i=0; $i < count($parasut_products); $i++) {
            
            $parasut_products[$i]['barcodes'] = null;
            $prod = Products::where('code', $parasut_products[$i]['attributes']['barcode'])->first();

            if ($parasut_products[$i]['attributes']['stock_count'] < 0){
                // Paraşüt kayıtlarında stok 0'dan küçük...
                $parasut_products[$i]['status'] = 'error';
                $parasut_products[$i]['status_text'] = "Paraşüt kayıtlarında stok 0'dan küçük...";
                $parasut_products[$i]['barcode'] = null;
                $parasut_products[$i]['barcodeimg'] = null;
            }else if(empty($prod)){
                // Ürün Dükkanda mevcut değil...
                $parasut_products[$i]['status'] = 'error';
                $parasut_products[$i]['status_text'] = "Ürün Dükkanda mevcut değil...";
                $parasut_products[$i]['barcode'] = null;
                $parasut_products[$i]['barcodeimg'] = null;
            }else{
                // Ürün Dükkanda mevcut...
                if (is_null($prod->relationship)) {
                    // Ürün ilişkilendirilmemiş...
                    $parasut_products[$i]['status'] = 'relationship_null';
                    $parasut_products[$i]['status_text'] = "Ürünü İlişkilendirin...";
                    $parasut_products[$i]['barcode'] = null;
                    $parasut_products[$i]['barcodeimg'] = null;
                }else{
                    // Ürün ilişkilendirilmiş...
                    $parasut_products[$i]['status'] = 'relationship_success';
                    $parasut_products[$i]['status_text'] = "Barkod Üretimi için hazır...";
                    $parasut_products[$i]['barcode'] = $parasut_products[$i]['attributes']['barcode'];

                    $parasut_products[$i]['barcodeimg'] = is_null($parasut_products[$i]['attributes']['barcode']) ? null : '<img src="data:image/png;base64,'.DNS1D::getBarcodePNG($parasut_products[$i]['attributes']['barcode'], "C128", $this->barcode_size->val_str_1*2, $this->barcode_size->val_str_2*2).'" alt="barcode" />';

                    
                    $parasut_products[$i]['relationship'] = $prod->relationship;
                    $stock_tracks = array();
                    if (Stock_Tracking::where('stock_id', $parasut_products[$i]['id'])->where('disband', '0')->count() > 0) { 
                        foreach (Stock_Tracking::where('stock_id', $parasut_products[$i]['id'])->where('disband', '0')->get() as $key => $value) {
                            $stock_tracks[] = [
                                'procode' => $value->procode,
                                'group_count' => $value->group_count,
                                'barcode' => $value->barcode,
                                'img' => '<img src="data:image/png;base64,'.DNS1D::getBarcodePNG($value->barcode, "C128", $this->barcode_size->val_str_1*2, $this->barcode_size->val_str_2*2).'" alt="barcode" />',
                            ];                          
                        }
                        $parasut_products[$i]['barcodes'] = $stock_tracks;
                        //dump($stock_tracks);
                    }
                }
            }
        }

        foreach ($parasut_products as $key => $row) {
            $barcode[$key]  = $row['barcode'];
        }

        array_multisort($barcode, SORT_ASC, $parasut_products);
        
        return view('stock.cafe_barcodes', array('parasut_products' => $parasut_products));
        */
    }

    public function makeBarcodes(Request $request)
    {
        $product_id = $request->input('product_id');

        $kutusayisi = intval($request->input('pctc'));
        $kutudakiurunsayisi = intval($request->input('pct'));
        $tekurun = 0;

        $bolen = intval($request->input('pct'));
        $stoksayisi = intval($request->input('pct'));

        $parasut = new Client($this->config);
        $parasut->authorize();

        $product_parasut = $parasut->call('products/'.$product_id)['data'];
        
        $product_db = Products::where('code', $product_parasut['attributes']['barcode'])->first();

        if(empty($product_db)){
            //bu ürün dükkana eşlenmemiş, dükkanda yok
            return \Redirect::back()->with('message', array('text' => 'Bu ürün eşleştirilmemiş durumda. Lütfen önce "Ürün Senkronizasyonu" sayfasından bu ürünü "<i class="fa fa-hand-o-right" aria-hidden="true"></i> - İlişkilendirin" ve sonra tekrar deneyin.', 'status' => 'error'));
        }

        if (is_null($kutusayisi) || $kutusayisi <= 0 || is_null($kutudakiurunsayisi) || $kutudakiurunsayisi <= 0) {
            $kutusayisi = 0;
            $kutudakiurunsayisi = 0;
            $tekurun = $product_parasut['attributes']['stock_count'];
        }else{
            $tekurun = $product_parasut['attributes']['stock_count'] - ($kutusayisi * $kutudakiurunsayisi);
        }

        for ($i=0; $i < intval($kutusayisi); $i++) { 
            $value = $this->generate_barcode($product_parasut['id'], 'multi', $product_parasut['attributes']['barcode'], $kutudakiurunsayisi, $product_db);
        }
        for ($i=0; $i < $tekurun; $i++) { 
            $value = $this->generate_barcode($product_parasut['id'], 'single', $product_parasut['attributes']['barcode'], 1, $product_db);
        }
        
        return redirect('stock/barcodes');
    }

    public function stocktaking()
    {
        $arr = array();
        session(['barcode_arr' => $arr]);

        $datas = Stock_Taking::orderBy('date', 'desc')->get();
        $parasut_products = $this->getParasutProductsAll();

        return view('stock.stocktaking', array('datas' => $datas, 'parasut_products' => $parasut_products));
    }

    public function stocktakingdetail($id = null)
    {
        $data = Stock_Taking::find($id);
        $stockTracking = Stock_Tracking::all();
        $parasut_products = $this->getParasutProductsAll();

        return view('stock.stocktakingdetail', array('data' => $data, 'stockTracking' => $stockTracking, 'parasut_products' => $parasut_products));
    }

    public function newstocktaking($id = null)
    {
        if ($id == null) {
            $resumeStockTracking = null;
        }else{
            $resumeStockTracking = Stock_Taking::find($id);
        }
        $arr = array();
        session(['barcode_arr' => $arr]);

        $parasut_products = $this->getParasutProductsAll();

        return view('stock.newstocktaking', array('parasut_products' => $parasut_products, 'resumeStockTracking' => $resumeStockTracking));
    }

    public function checkListen()
    {

        $barcode = $_REQUEST['barcode'];

        if(empty($barcode)){
            //boş ise
            return response()->json('ERROR 03', 406, [], JSON_PRETTY_PRINT);
        }

        $check = Stock_Tracking::select('group_count', 'proid', 'barcode')->where('barcode', $barcode)->get();
       
        $barcode_arr = session('barcode_arr');

        if(count($check) > 0){
            if($check->first()->group_count > 1 && in_array($check->first()->barcode, $barcode_arr)){
                // bu kutu barkodu daha önceden okutuldu
                return response()->json('ERROR 01', 406, [], JSON_PRETTY_PRINT);
            }else{
                $barcode_arr[] = $barcode;
            }
        }else{
            // böyle bir barkod yok
            return response()->json('ERROR 02', 406, [], JSON_PRETTY_PRINT);
        }
        
        session(['barcode_arr' => $barcode_arr]);
        $product_name = Products::find($check->first()->proid);

        return response()->json(['status' => 'OK', 'title' => $product_name->label], 200, [], JSON_PRETTY_PRINT);
    }

    public function takingSave(Request $request)
    {

        $report = array();

        $get_before = Stock_Tracking::where('disband', '0')->get()->groupBy('procode');
        $before = array();
        foreach($get_before as $code => $elements){
            $sub_before = null;
            foreach ($elements as $element) {
                $sub_before = $sub_before + $element->group_count;
            }
            $before[$code] = $sub_before;
        }              

        //check we db
        //tekli ürünün kaç adet oldunu hesaplayan bölüm
        $num_arr = array();
        foreach ($request->input() as $key => $value) {
            if(explode("_", $key)[0] == 'num'){
                if(!is_null($value)){
                    $check_now = Stock_Tracking::
                        where('barcode', $request->
                        input('take_'.explode("_", $key)[1]))->
                        where('disband', '0')->
                        where('group_count', '1')->
                        first();
                    if(!empty($check_now)){
                        $num_arr[explode("_", $key)[1]] = $value;                    
                    }
                }
            }
        }

        //sayımı ürün bazında toplamlarını arraya atan bölüm
        $sub_take = array();
        foreach ($request->input() as $key => $value) {
            if(explode("_", $key)[0] == 'take'){       
                $check_now = Stock_Tracking::
                    where('barcode', $value)->
                    where('disband', '0')->
                    first();
                if(!empty($check_now)){
                    if(!empty($num_arr[explode("_", $key)[1]])){
                        $sub_x = $check_now->group_count * $num_arr[explode("_", $key)[1]];
                        if(empty($sub_take[$check_now->procode])){
                            $sub_take[$check_now->procode] = $sub_x;
                        }else{
                            $sub_take[$check_now->procode] = $sub_take[$check_now->procode] + $sub_x;   
                        }
                    }else{
                        if(empty($sub_take[$check_now->procode])){
                            $sub_take[$check_now->procode] = $check_now->group_count;
                        }else{
                            $sub_take[$check_now->procode] = $sub_take[$check_now->procode] + $check_now->group_count;   
                        }  
                    }                                             
                }          
            }            
        }

        $readBarcode = array();
        foreach ($request->input() as $key => $value) {
            if(explode("_", $key)[0] == 'take' && $value != null){
                //dump($key." - ".$value." - ".explode("_", $key)[1]);
                $readBarcode[intval(explode("_", $key)[1])]['code'] = explode("G", $value)[0];
                $readBarcode[intval(explode("_", $key)[1])]['number'] = explode("_", $key)[1];
                $readBarcode[intval(explode("_", $key)[1])]['take'] = $value;
            }

            if(explode("_", $key)[0] == 'num' && $value != null){
                //dump($key." - ".$value." - ".explode("_", $key)[1]);
                $readBarcode[intval(explode("_", $key)[1])]['num'] = $value;
            }
        }

        $report['readBarcode'] = $readBarcode;
        $parasut_products = $this->getParasutProductsAll();

        $products = array();
        foreach ($parasut_products as $key => $value) {
            if ($value['attributes']['code'] != "") {
                $products[$value['attributes']['code']] = intval($value['attributes']['stock_count']);
            }
        }
        
        foreach ($sub_take as $key => $value) {

            $report[$key]['localCount'] = $before[$key];
            $report[$key]['parasutCount'] = $products[$key];
            $report[$key]['nowCount'] = $value;
            
            /*
                if($value > $before[$key]){
                    $report['local'][$key]['class'] = 'warning';
                    $report['local'][$key]['text'] = 'Stoktakinden daha fazla sayıda sayıldı.';
                }else if($value < $before[$key]){
                    $report['local'][$key]['class'] = 'danger';
                    $report['local'][$key]['text'] = 'Stoktakinden daha az sayıda sayıldı.';
                }else if($value == $before[$key]){
                    $report['local'][$key]['class'] = 'success';
                    $report['local'][$key]['text'] = 'Stoktakla eşit sayıda sayıldı.';
                }
                $report['local'][$key]['before'] = $before[$key];
                $report['local'][$key]['now'] = $value;

                if($value > $products[$key]){
                    $report['parasut'][$key]['class'] = 'warning';
                    $report['parasut'][$key]['text'] = 'Stoktakinden daha fazla sayıda sayıldı.';
                }else if($value < $products[$key]){
                    $report['parasut'][$key]['class'] = 'danger';
                    $report['parasut'][$key]['text'] = 'Stoktakinden daha az sayıda sayıldı.';
                }else if($value == $products[$key]){
                    $report['parasut'][$key]['class'] = 'success';
                    $report['parasut'][$key]['text'] = 'Stoktakla eşit sayıda sayıldı.';
                }
                $report['parasut'][$key]['before'] = $products[$key];
                $report['parasut'][$key]['now'] = $value;
            */
        }

        if (isset($request->idd)) {
            $stock_taking = Stock_Taking::find($request->idd);
        }else{
            $stock_taking = new Stock_Taking();
        }

        $stock_taking->date = Carbon::now();
        $stock_taking->staff = $request->input('staff');
        $stock_taking->controller_staff = $request->input('controller_staff');
        $stock_taking->notes = $request->input('notes');
        $stock_taking->report = json_encode($report);
        $stock_taking->save();

        return redirect('stock/stocktaking');
    }

    public function takingOk(Request $request)
    {
    
        $data = Stock_Taking::find($request->idd);
        $report = json_decode($data->report, true);

        $countData = array();
        foreach ($report['readBarcode'] as $key => $value) {
            if (isset($value['num'])) {
                $countData[$value['code']]['single'][] = intval($value['num']);
            }else{
                $countData[$value['code']]['boxes'][] = array($value['take'], intval(explode("N", explode("G", $value['take'])[1])[0]));
            }
        }

        foreach ($countData as $key => $value) {
            //dump($key);
            //dump($value['boxes']);

            $activeBoxBarcodes = Stock_Tracking::where('procode', $key)->whereNotNull('barcode_order')->where('disband', '0')->get();
            ////dump($activeBoxBarcodes);
            foreach ($activeBoxBarcodes as $boxBarcode) {
                foreach ($value['boxes'] as $barcode) {
                    if($boxBarcode->barcode == $barcode[0]){
                        $boxBarcode->sayildi = true;
                    }
                }
            }

            foreach ($activeBoxBarcodes as $boxBarcode) {
                if (isset($boxBarcode->sayildi)) {
                    //dump($boxBarcode->barcode.' - '.$boxBarcode->group_count.' - ok');
                }else{
                    //dump($boxBarcode->barcode.' - '.$boxBarcode->group_count.' - disband = 1');
                    //dump($boxBarcode);
                    $boxBarcode->disband = '1';
                    $boxBarcode->save();
                }
            }
            
            $singleBarcodeCount = 0;
            foreach ($value['single'] as $val) {
                $singleBarcodeCount += $val;
            }
            //dump('$singleBarcodeCount: '.$singleBarcodeCount);

            $activeSingleBarcodesCount = Stock_Tracking::where('procode', $key)->whereNull('barcode_order')->where('disband', '0')->count();
            //dump('$activeSingleBarcodesCount: '.$activeSingleBarcodesCount);

            if ($activeSingleBarcodesCount > $singleBarcodeCount) {
                $activeSingleBarcodes = Stock_Tracking::where('procode', $key)->whereNull('barcode_order')->where('disband', '0')->get();
                $i = 0;
                foreach ($activeSingleBarcodes as $singleBarcode) {
                    if ($i >= $singleBarcodeCount) {
                        //dump($singleBarcode->barcode.' - '.$singleBarcode->group_count.' - disband = 1');
                        //dump($singleBarcode);
                        $singleBarcode->disband = '1';
                        $singleBarcode->save();
                    }
                    $i++;
                }
            }else if ($activeSingleBarcodesCount < $singleBarcodeCount) {
                
                $refBarcode = Stock_Tracking::where('procode', $key)->whereNull('barcode_order')->where('disband', '0')->first();
                $yenikayitsayisi = $singleBarcodeCount - $activeSingleBarcodesCount;

                for ($i=0; $i < $yenikayitsayisi; $i++) { 
                    ////dump('add new');
                    
                    $new_stock = new Stock_Tracking();
                    $new_stock->stock_id = $refBarcode->stock_id;
                    $new_stock->proid = $refBarcode->proid;
                    $new_stock->procode = $refBarcode->procode;
                    $new_stock->group_count = $refBarcode->group_count;
                    $new_stock->barcode_order = $refBarcode->barcode_order;
                    $new_stock->barcode = $refBarcode->barcode;
                    $new_stock->save();

                }

            }
            //dump('------------------------------------------');
        }

        $data->cheked = '1';
        $data->save();
        
        //dd($report['readBarcode']);

        return redirect('stock/stocktaking');
    }

    public function takingCancel(Request $request)
    {
        Stock_Taking::find($request->idd)->delete();
        return redirect('stock/stocktaking');
    }


    /*********//*********//*********//*********//*********//*********/


    public function products(){

        $parasut = new Client($this->config);
        $parasut->authorize();

        $parasut_products = array();
        $total_pages = 1;

        $data = $parasut->call('products', [
            'page[number]' => 1,
            'page[size]' => 25,
        ]);

        foreach ($data['data'] as $pprod) {
            if($pprod['attributes']['inventory_tracking']){
                $parasut_products[] = $pprod;
            }
        }

        $total_pages = $data['meta']['total_pages'];

        if($total_pages > 1){
            for ($i=2; $i <= $total_pages; $i++) { 

                $data = $parasut->call('products', [
                    'page[number]' => $i,
                    'page[size]' => 25,
                ]);
                foreach ($data['data'] as $pprod) {
                    if($pprod['attributes']['inventory_tracking']){
                        $parasut_products[] = $pprod;
                    }
                }
            }
        }

        $pro_list = array();

        foreach ($parasut_products as $product) {
            if(empty(Products::where('code', $product['attributes']['code'])->first())){
                $relation = 0;
            }else{
                $relation = 1;
            }

            $img = is_null($product['attributes']['barcode']) ? null : '<img src="data:image/png;base64,'.DNS1D::getBarcodePNG($product['attributes']['barcode'], "C128",$this->barcode_size->val_str_1,$this->barcode_size->val_str_2).'" alt="barcode" />';

            $pro_list[] = [
                'id' => $product['id'],
                'name' => $product['attributes']['name'],
                'relation' => $relation,
                'code' => $product['attributes']['code'],
                'stock' => $product['attributes']['stock_count'],
                'barcode' => $product['attributes']['barcode'],
                'img' => $img,
            ];
        }

        return view('stock.products', array('pro_list' => $pro_list));
    }

    public function first_stock_count(){

        $parasut = new Client($this->config);
        $parasut->authorize();

        $parasut_products = array();
        $total_pages = 1;

        $data = $parasut->call('products', [
            'page[number]' => 1,
            'page[size]' => 25,
        ]);

        foreach ($data['data'] as $pprod) {
            if($pprod['attributes']['inventory_tracking']){
                $parasut_products[] = $pprod;
            }
        }

        $total_pages = $data['meta']['total_pages'];

        if($total_pages > 1){
            for ($i=2; $i <= $total_pages; $i++) { 

                $data = $parasut->call('products', [
                    'page[number]' => $i,
                    'page[size]' => 25,
                ]);
                foreach ($data['data'] as $pprod) {
                    if($pprod['attributes']['inventory_tracking']){
                        $parasut_products[] = $pprod;
                    }
                }
            }
        }


        for ($i=0; $i < count($parasut_products); $i++) { 
            $parasut_products[$i]['barcodes'] = null;

            //dump(Products::where('code', $parasut_products[$i]['attributes']['code'])->first());
            $prod = Products::where('code', $parasut_products[$i]['attributes']['code'])->first();

            if ($parasut_products[$i]['attributes']['stock_count'] < 0){
                // Paraşüt kayıtlarında stok 0'dan küçük...
                $parasut_products[$i]['status'] = 'error';
                $parasut_products[$i]['status_text'] = "Paraşüt kayıtlarında stok 0'dan küçük...";
            }else if(empty($prod)){
                // Ürün Dükkanda mevcut değil...
                $parasut_products[$i]['status'] = 'error';
                $parasut_products[$i]['status_text'] = "Ürün Dükkanda mevcut değil...";
            }else{
                // Ürün Dükkanda mevcut...
                if (is_null($prod->relationship)) {
                    // Ürün ilişkilendirilmemiş...
                    $parasut_products[$i]['status'] = 'relationship_null';
                    $parasut_products[$i]['status_text'] = "Ürünü İlişkilendirin...";
                }else{
                    // Ürün ilişkilendirilmiş...
                    $parasut_products[$i]['status'] = 'relationship_success';
                    $parasut_products[$i]['status_text'] = "Barkod Üretimi için hazır...";
                    $parasut_products[$i]['relationship'] = $prod->relationship;
                    $stock_tracks = array();
                    if (Stock_Tracking::where('stock_id', $parasut_products[$i]['id'])->count() > 0) { 
                        foreach (Stock_Tracking::where('stock_id', $parasut_products[$i]['id'])->get() as $key => $value) {
                            $stock_tracks[] = [
                                'procode' => $value->procode,
                                'group_count' => $value->group_count,
                                'barcode' => $value->barcode,
                                'img' => '<img src="data:image/png;base64,'.DNS1D::getBarcodePNG($value->barcode, "C128",$this->barcode_size->val_str_1,$this->barcode_size->val_str_2).'" alt="barcode" />',
                            ];                          
                        }
                        $parasut_products[$i]['barcodes'] = $stock_tracks;
                        //dump($stock_tracks);
                    }
                }
            }
        }
    

        return view('stock.first_stock_count', array('parasut_products' => $parasut_products));
    }

    public function barcode(){


        $parasut = new Client($this->config);
        $parasut->authorize();

        $parasut_products = array();
        $total_pages = 1;

        $data = $parasut->call('products', [
            'page[number]' => 1,
            'page[size]' => 25,
        ]);

        foreach ($data['data'] as $pprod) {
            if($pprod['attributes']['inventory_tracking']){
                $parasut_products[] = $pprod;
            }
        }

        /*
        $total_pages = $data['meta']['total_pages'];

        if($total_pages > 1){
            for ($i=2; $i <= $total_pages; $i++) { 

                $data = $parasut->call('products', [
                    'page[number]' => $i,
                    'page[size]' => 25,
                ]);
                foreach ($data['data'] as $pprod) {
                    if($pprod['attributes']['inventory_tracking']){
                        $parasut_products[] = $pprod;
                    }
                }
            }
        }
        */

        for ($i=0; $i < count($parasut_products); $i++) { 
            $parasut_products[$i]['barcodes'] = null;
            $parasut_products[$i]['barcode_stock_count'] = null;
            $parasut_products[$i]['stock_box_details'] = null;


            //dump(Products::where('code', $parasut_products[$i]['attributes']['code'])->first());
            $prod = Products::where('code', $parasut_products[$i]['attributes']['code'])->first();

            if ($parasut_products[$i]['attributes']['stock_count'] < 0){
                // Paraşüt kayıtlarında stok 0'dan küçük...
                $parasut_products[$i]['status'] = 'error';
                $parasut_products[$i]['status_text'] = "Paraşüt kayıtlarında stok 0'dan küçük...";
            }else if(empty($prod)){
                // Ürün Dükkanda mevcut değil...
                $parasut_products[$i]['status'] = 'error';
                $parasut_products[$i]['status_text'] = "Ürün Dükkanda mevcut değil...";
            }else{
                // Ürün Dükkanda mevcut...
                if (is_null($prod->relationship)) {
                    // Ürün ilişkilendirilmemiş...
                    $parasut_products[$i]['status'] = 'relationship_null';
                    $parasut_products[$i]['status_text'] = "Ürünü İlişkilendirin...";
                }else{
                    // Ürün ilişkilendirilmiş...
                    $parasut_products[$i]['status'] = 'relationship_success';
                    $parasut_products[$i]['status_text'] = "Barkod Üretimi için hazır...";
                    $parasut_products[$i]['relationship'] = $prod->relationship;
                    $stock_tracks = array();
                    if (Stock_Tracking::where('stock_id', $parasut_products[$i]['id'])->count() > 0) {
                        $barcode_stock_count = 0;
                        foreach (Stock_Tracking::where('stock_id', $parasut_products[$i]['id'])->get() as $key => $value) {
                            $barcode_stock_count += $value->group_count;

                            


                            $stock_tracks[] = [
                                'procode' => $value->procode,
                                'group_count' => $value->group_count,
                                'barcode' => $value->barcode,
                                'disband' => $value->disband,
                                //'img' => '<img src="data:image/png;base64,'.DNS1D::getBarcodePNG($value->barcode, "C128",$this->barcode_size->val_str_1,$this->barcode_size->val_str_2).'" alt="barcode" />',
                            ];
                            

                        }




                        $parasut_products[$i]['stock_box_details'] = null;


                        $parasut_products[$i]['barcodes'] = $stock_tracks;
                        $parasut_products[$i]['barcode_stock_count'] = $barcode_stock_count;
                        //dump($parasut_products[$i]);
                    }
                }
            }
        }
        //die;
        return view('stock.stockbarcode', array('parasut_products' => $parasut_products));




        /*
        	$parasut = new Client($this->config);
    		$parasut->authorize();

            $parasut_products = array();
            $total_pages = 1;

            $data = $parasut->call('products', [
                'page[number]' => 1,
                'page[size]' => 25,
            ]);

            foreach ($data['data'] as $pprod) {
                if($pprod['attributes']['inventory_tracking']){
                    $parasut_products[] = $pprod;
                }
            }

            $total_pages = $data['meta']['total_pages'];

            if($total_pages > 1){
                for ($i=2; $i <= $total_pages; $i++) { 

                    $data = $parasut->call('products', [
                        'page[number]' => $i,
                        'page[size]' => 25,
                    ]);
                    foreach ($data['data'] as $pprod) {
                        if($pprod['attributes']['inventory_tracking']){
                            $parasut_products[] = $pprod;
                        }
                    }
                }
            }



            $stock_bills = array();
            $total_pages_purchase_bills = 1;

            $purchase_bills = $parasut->call('purchase_bills/', [
                'sort' => '-issue_date',
                'page[number]' => 1,
                'page[size]' => 25,
                'include' => 'details.product'
            ]);

            foreach ($purchase_bills['data'] as $purchase_bill) {
                if($purchase_bill['attributes']['is_detailed']){
                    $stock_bills[] = $purchase_bill;
                }
            }
      
            $total_pages_purchase_bills = $purchase_bills['meta']['total_pages'];

            if($total_pages_purchase_bills > 4){
                $total_pages_purchase_bills = 4;
            }

            if($total_pages_purchase_bills > 1){
                for ($i=2; $i <= $total_pages_purchase_bills; $i++) { 

                    $purchase_bills = $parasut->call('purchase_bills/', [
                        'sort' => '-issue_date',
                        'page[number]' => $i,
                        'page[size]' => 25,
                        'include' => 'details.product'
                    ]);
                    foreach ($purchase_bills['data'] as $purchase_bill) {
                        if($purchase_bill['attributes']['is_detailed']){
                            $stock_bills[] = $purchase_bill;
                        }
                    }
                }
            }
           
            dump($stock_bills);
            return view('stock.barcode', array('stock_bills' => $stock_bills));
        */
            /*
            $products = $parasut->call('products/8213476');

            $shipment_documents = $parasut->call('shipment_documents/');
            echo "---";
            dd($shipment_documents);
            //$purchase_bills = $parasut->call('purchase_bills/6215069')['data'];
            //dd($purchase_bills);

        */


        die;
    	$products = $parasut->call('products')['data'];
    	$shipment_documents = $parasut->call('shipment_documents')['data'];
    	//$purchase_bills = $parasut->call('purchase_bills/6215069')['data'];
        $purchase_bills = $parasut->call('purchase_bills')['data'];


    	$stock_activities = array();
    	foreach ($shipment_documents as $key => $value) {
    		$data = null;
    		$datas = null;
    		if($value['attributes']['inflow'] == true){ 
    			$details = $parasut->call('shipment_documents/'.$value['id'].'/details');   
    			foreach ($details['included'] as $key3 => $value3) {
    				if (Stock_Tracking::where('stock_id', $value['id'])->count() > 0) { 
	    				foreach (Stock_Tracking::where('stock_id', $value['id'])->get() as $key2 => $value2) {
    						if($value3['attributes']['code'] == $value2->procode){
    							$datas[] = [
			    					'procode' => $value2->procode,
			    					'group_count' => $value2->group_count,
			    					'barcode' => $value2->barcode,
			    					'img' => '<img src="data:image/png;base64,'.DNS1D::getBarcodePNG($value2->barcode, "C128",$this->barcode_size->val_str_1,$this->barcode_size->val_str_2).'" alt="barcode" />',
			    				];
    						}		    				
		    			}
		    			$data[$key3] = $datas;
	    			}
    			}	
                $relation = array();
                foreach ($details['included'] as $key5 => $value5) {  
                    if(empty(Products::where('code', $value5['attributes']['code'])->first())){
                        $relation[$key5] = 0;
                    }else{
                        $relation[$key5] = 1;
                    }
                }
    			$stock_activities[] = [
    				'id' => $value['id'],
                    'relation' => $relation,
    				'date' => $value['attributes']['created_at'],
    				'datas' => $details['data'],
    				'products' => $details['included'],
    				'stock_tracking' => $data
    			];
    		}    		
    	}

    	//dd($shipment_documents);
    	
    	return view('stock.barcode', array('stock_activities' => $stock_activities));
    }

    public function makeBarcode(Request $request){
		$stock_id = $request->input('activity_id');
    	$detail_id = $request->input('id');
        $bolen = $request->input('pct');

    	$parasut = new Client($this->config);
		$parasut->authorize();
		$shipment_documents = $parasut->call('shipment_documents/'.$stock_id.'/details');

		for ($i=0; $i < count($shipment_documents['data']) ; $i++) { 
			if($shipment_documents['data'][$i]['id'] == $detail_id){
				$product_parasut = $shipment_documents['included'][$i]['attributes'];
				$bolunen = intval($shipment_documents['data'][$i]['attributes']['quantity']);
			}
		}
        if(is_null($product_parasut['barcode'])){
            //paraşütteki bu ürünün barkod bilgisi boş
            return \Redirect::back()->with('message', array('text' => 'Bu ürünün "Paraşüt" sisteminde "Barkod" bilgisi yoktur. Lütfen önce paraşüt üzerinden bu ürüne barkod bilgisi girin ve sonra tekrar deneyin.', 'status' => 'error'));
        }

		if(Stock_Tracking::where('stock_id', $stock_id)->where('procode', $product_parasut['code'])->count() > 0){
            //bu harekette bulunan, bu ürünün barkodu daha önceden hazırlanmış
            //bu if'e yapı gereği girmesi imkansızdır. Ama yinede control olarak durur.
            return \Redirect::back();
		}

		$product_db = Products::where('code', $product_parasut['code'])->first();

		if(empty($product_db)){
			//bu ürün dükkana eşlenmemiş, dükkanda yok
            return \Redirect::back()->with('message', array('text' => 'Bu ürün eşleştirilmemiş durumda. Lütfen önce "Ürün Senkronizasyonu" sayfasından bu ürünü "<i class="fa fa-hand-o-right" aria-hidden="true"></i> - İlişkilendirin" ve sonra tekrar deneyin.', 'status' => 'error'));
		}
    	
        if(is_null($bolen)){
            $bolum = 0;
            $kalan = $bolunen;
        }else{
            $bolum = $bolunen / $bolen;
            $kalan = $bolunen - (intval($bolum) * $bolen);
        }        	

    	for ($i=0; $i < intval($bolum); $i++) { 
    		$value = $this->generate_barcode($stock_id, 'multi', $product_parasut['barcode'], $bolen, $product_db);
    	}
    	for ($i=0; $i < $kalan; $i++) { 
    		$value = $this->generate_barcode($stock_id, 'single', $product_parasut['barcode'], 1, $product_db);
    	}
        return redirect('stock/barcode');
    }

    public function makeFirstBarcode(Request $request){

        $product_id = $request->input('product_id');
        $bolen = intval($request->input('pct'));
        $stoksayisi = intval($request->input('pct'));

        $parasut = new Client($this->config);
        $parasut->authorize();

        $product_parasut = $parasut->call('products/'.$product_id)['data'];
        
        $product_db = Products::where('code', $product_parasut['attributes']['code'])->first();

        if(empty($product_db)){
            //bu ürün dükkana eşlenmemiş, dükkanda yok
            return \Redirect::back()->with('message', array('text' => 'Bu ürün eşleştirilmemiş durumda. Lütfen önce "Ürün Senkronizasyonu" sayfasından bu ürünü "<i class="fa fa-hand-o-right" aria-hidden="true"></i> - İlişkilendirin" ve sonra tekrar deneyin.', 'status' => 'error'));
        }

        if(is_null($bolen) || $bolen == 0){
            $bolum = 0;
            $kalan = $product_parasut['attributes']['stock_count'];
        }else{
            $bolum = floor($product_parasut['attributes']['stock_count'] / $bolen);
            $kalan = $product_parasut['attributes']['stock_count'] % $bolen;
        }

        for ($i=0; $i < intval($bolum); $i++) { 
            $value = $this->generate_barcode($product_parasut['id'], 'multi', $product_parasut['attributes']['barcode'], $bolen, $product_db);
        }
        for ($i=0; $i < $kalan; $i++) { 
            $value = $this->generate_barcode($product_parasut['id'], 'single', $product_parasut['attributes']['barcode'], 1, $product_db);
        }
        
        return redirect('stock/first_stock_count');
    }

    public function generate_barcode($stock_id, $type, $barcode, $group, $product){

    	$val = null;
    	if($type == 'multi'){
    		$last_order = Stock_Tracking::where('procode', $product->code)->orderBy('barcode_order', 'desc')->select('barcode_order')->first();
	    	if(is_null($last_order)){
	    		$val = 1;
	    	}else{
	    		$val = $last_order->barcode_order + 1;
	    	}
    		$generate_barcode = $barcode.'G'.$group.'N'.$val;
    	}else{
    		$generate_barcode = $barcode;
    	}

    	$stock = new Stock_Tracking();
    	$stock->stock_id = $stock_id;
    	$stock->proid = $product->id;
    	$stock->procode = $product->code;
    	$stock->group_count = $group;
    	$stock->barcode_order = $val;
    	$stock->barcode = $generate_barcode;
    	$stock->save();

    	return $stock;
    }


}
