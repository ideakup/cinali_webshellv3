<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Language;

class Menu extends Model
{
    protected $table = 'menu';
    
    public function content()
    {
        return $this->belongsToMany('App\Content', 'menu_has_content');
    }
    
    public function variable()
    {
        $currentLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        return $this->hasOne('App\MenuVariable', 'menu_id', 'id')->where('lang_code', $currentLang->code);
    }

    public function variableLang($langcode)
    {
        return $this->hasOne('App\MenuVariable', 'menu_id', 'id')->where('lang_code', $langcode)->first();
    }

    public function variables()
    {
        return $this->hasMany('App\MenuVariable', 'menu_id', 'id');
    }

    public function topMenu()
    {
        return $this->hasOne('App\Menu', 'id', 'top_id')->where('deleted', 'no');
    }

    public function subMenu()
    {
        return $this->hasMany('App\Menu', 'top_id', 'id')->where('deleted', 'no');
    }
    
    public function slider()
    {
        return $this->hasOne('App\Slider', 'menu_id', 'id');
    }

    
    
}
