<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KafeOrder extends Model
{
    protected $table = 'kafe_order';
}
