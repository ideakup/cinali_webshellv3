<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KafeOrderDetails extends Model
{
    protected $table = 'kafe_order_details';
}
