<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account_Orders extends Model
{
    protected $table = 'account_has_orders';

    public function users(){
    	return $this->hasOne('App\User', 'id', 'customerid');
    }
}
