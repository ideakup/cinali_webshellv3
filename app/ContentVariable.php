<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentVariable extends Model
{
    protected $table = 'contentvariable';

    public function content()
    {
        return $this->belongsTo('App\Content', 'content_id', 'id');
    }
  
}
