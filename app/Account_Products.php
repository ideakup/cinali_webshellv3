<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account_Products extends Model
{
    protected $table = 'account_has_products';

    public function products(){
    	return $this->hasOne('App\Products', 'id', 'proid');
    }
}
