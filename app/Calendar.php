<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $table = 'calendar';

    public function variable()
    {
        $currentLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        return $this->hasOne('App\CalendarVariable', 'calendar_id', 'id')->where('lang_code', $currentLang->code);
    }

    public function variableLang($langcode)
    {
        return $this->hasOne('App\CalendarVariable', 'calendar_id', 'id')->where('lang_code', $langcode)->first();
    }

    public function variables()
    {
        return $this->hasMany('App\CalendarVariable', 'calendar_id', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'calendar_has_tag');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'calendar_has_category');
    }

}
