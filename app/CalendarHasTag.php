<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarHasTag extends Model
{
    protected $table = 'calendar_has_tag';
}
