<?php

namespace App\Exports;

use App\Content;
use App\FormData;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class FormDataExport implements FromView
{
    
    protected $arr;
    
    public function __construct(array $arr)
    {
        $this->arr = $arr;
    }

    public function view(): View
    {
    	//dd($this->arr['formDist']);
    	$form = Content::where('id', $this->arr['formId'])->where('type', 'form')->where('deleted', 'no')->first();
    	$formData = FormData::where('form_id', $this->arr['formId']);
    	
    	if($this->arr['formDist']!= 'null'){
    		$formdist = explode(':', $this->arr['formDist']);
    		$formData = $formData->where('source_type', $formdist[0])->where('source_id', $formdist[1]);
    	}
    	
    	$formData = $formData->get();
    	return view('exports.formData', array('form' => $form, 'formData' => $formData));
    }
}
