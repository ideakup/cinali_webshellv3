<?php

namespace App\Exports;

use App\Content;
use App\Calendar;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CalendarExport implements FromView
{
    
    protected $arr;
    
    public function __construct(array $arr)
    {
        $this->arr = $arr;
    }

    public function view(): View
    {
    	
    	$calendar = Calendar::where('deleted', 'no')->orderBy('start', 'desc')->get();
    	return view('exports.calendar', array('calendar' => $calendar));
    }
}
