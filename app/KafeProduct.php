<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KafeProduct extends Model
{
    protected $table = 'kafe_products';
}
