<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarHasCategory extends Model
{
    protected $table = 'calendar_has_category';
}
